# Tinkerman.cat

This is the repository for the tech blog [http://tinkerman.cat](http://tinkerman.cat).

[![Netlify Status](https://api.netlify.com/api/v1/badges/51724850-fb93-4313-971a-6794731b24c4/deploy-status)](https://app.netlify.com/sites/tinkerman/deploys)
[![twitter](https://img.shields.io/twitter/follow/xoseperez.svg?style=social)](https://twitter.com/intent/follow?screen_name=xoseperez)

This blog site uses:

* [Hugo](https://gohugo.io) Static Content Manager
* [HTMLTest](https://github.com/wjdp/htmltest) as HTML linter
* [GIT](https://git-scm.com) as version control system
* [GitLab](https://gitlab.com) as repository hosting
* [Netlify](https://netlify.com) as site deployer and hosting

The theme is based on [Hestia Pure](https://themes.gohugo.io/hestia-pure/) by [diwao](http://diwao.com/).

## License

The contents of the following folders and files as copyrighted by Xose Pérez and Licensed under the [Creative Commons Attribution-ShareAlike 4.0 International Public
License](https://creativecommons.org/licenses/by-sa/4.0/):

* Everything under `/content`
* Everything under `/static/uploads`
* `/static/image/header.jpg`
* `/static/image/xose.png`

#!/bin/sh
docker run --rm -it -v $(pwd):/src klakegg/hugo:0.107.0 --minify
find public -type f \( -name '*.html' -o -name '*.js' -o -name '*.css' \) -exec gzip -v -f --best {} \;
find public -name "*.gz" -exec rename 's/.gz$//' {} \;

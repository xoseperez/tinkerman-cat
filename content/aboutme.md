---
title: "About me"
aside: false
---

# About me

My name is Xose Pérez and I live in Sant Pol de Mar, a small town near Barcelona. I studied Astrophysics but computers have always been a passion.

For years I worked for different companies in different sectors of the web development world, from geographic information systems (even before Google Maps crashed in) to home theater media players, from financial sites and e-commerce to social networks.

In 2014 I quit and we, as a family, started a new life. We were traveling for 5 months in South America with our 2 and 5 year old daughters and wrote about the experience here: [Sin Piedras en los Bolsillos](http://sinpiedrasenlosbolsillos.com) (in Spanish). Back in Barcelona we decided to move to a small town north of the city, 1 minute away from the beach. The kids attend to a school in the forest and my wife works there too. Now I'm writing you from my study here, hearing the sound of the waves hitting the sand.

{{< image "/uploads/2019/05/santpol.jpg" "\"Les Barques\" beach in Sant Pol de Mar" >}}

Of course all these changes meant I should rework myself professionally. I recalled things from my days at the college and I'm trying to turn a hobby of mine (DIY electronic projects) into a way of life. I'm still doing some web development (hey! just learned Hugo :)) but most of the time I'm dealing with embedded development in C, also some python and node. On the hardware side I'm a fan of laser cutters but they are way too expensive yet. I design my own PCBs and I have my good share of welding and burning LEDs.

## Projects

For the past few years I've been (and I'm still) involved in several projects related to technological empowerment and technological sovereignty. Let me tell you about some of them.

### ESPurna

{{< image "/uploads/2019/05/github-xoseperez-espurna.png" >}}

When I'm in front of the computer and not working, chances are I'm "working" on ESPurna, although this is not happening very often lately. ESPurna is an libre source firmware for ESP8266 based smart-devices. It currently supports more than 125 different devices, lots of sensors and some third party integrations. But you should better visit the project repository on Github: [ESPurna](https://github.com/xoseperez/espurna).

### The Things Network Catalunya 

{{< image "/uploads/2019/05/ttncat.logo.png" >}}

I'm sure you already know what is [The Things Network](http://thethingsnetwork.org), but if you don't it's a community-based network for the Internet of Things based on LoRaWAN. There are literally hundreds of different local communities all around the globe. I've been involved in the [The Things Network Catalunya](http://ttn.cat) community for almost 3 years now and so far we have deployed 7 gateways of our own plus 7 more gateways from partners of the community. In 2018 we did more than 40 different workshops in the city of Barcelona and this year we are already working with other municipalities around Barcelona to develop projects around specific use cases like energy poverty.

### Tarpuna

{{< image "/uploads/2019/05/etarpunometre.jpg" "e-Tarpunòmetre by Tarpuna" >}}

[Tarpuna](http://tarpunacoop.org) is a workers' cooperative that develops projects related to social innovation in fields such as agriculture, energy and digital fabrication. I worked there for 2 and a half years in projects related to the public FabLabs in Barcelona ([Ateneus de Fabricació](https://twitter.com/BCN_AteneusFab)). We also developed an energy usage awareness project that included a DIY kit ([e-Tarpunòmetre](http://bancdenergia.org/que-es-e-tarpunometre/)).

### AllWize

{{< image "/uploads/2019/05/20190504_231200s.jpg" "AllWize K2" >}}

LoRa and LoRaWAN are great since you can deploy your own infrastructure, so you can build a network of your own or a community one like The Things Network. [Wize](http://wize-alliance.com) is also an IoT protocol with no network lock-in, so you can also deploy your won network. You might have not hear about Wize but you should know it's an evolution of the Wireless MBus protocol that's been around for longer than any other LPWAN network nowadays (like Sigfox or LoRaWAN itself). Recently I had the chance to join [AllWize](http://allwize.io) as an external consultant and developer. AllWize goal is to ease the access to this technology for PoC developers or makers with affordable hardware and easy to use libraries for the Arduino world.
---
author: Xose
comments: true
date: 2018-04-09 22:22:04+00:00
layout: post
slug: geekworm-power-pack-hat-hack
title: "GeekWorm Power Pack Hat Hack"
post_id: 2245
image: "images/20180315_141353s-1200x800.jpg"
categories:
- Hacking
- Projects
tags:
- attiny85
- ds3231
- geekworm
- hat
- lipo
- mosfet
- oshpark
- raspberry pi
- rtc
- sensortag
- ups
- usbasp
---

Sorry for the tongue twister ;)

I've been somewhat busy lately and it's been a long time since my last post. I have a few projects on the go but not much time to sit down and write about them... Let's see if this one goes through...

I've been lately looking for a reliable UPS system for Raspberry Pi 3. I moved my home server to a RPi a few months ago and even thou its behind an ACS UPS a couple of other projects involving RPis required mobility (one of them) and unassisted power backup (the other). So I started browsing several marketplaces looking for a solution.

<!-- more -->

## Update: heads up on Joachim's work

Joachim Baumann's is working on an Raspberry Pi UPS based on the same Geekworm UPS hat that offers very interesting features, including:

  * configuration sync between the ATTiny and the Raspberry
  * temperature measurement for monitoring the battery temperature
  * communication using I2C, thus not blocking any pins of the Raspberry Pi
  * minimal modification of existing hardware
  * automatic shutdown and restart
  * different thresholds for warning, hard shutdown and restart
  * external button to execute configurable functionality
  * measurement of an additional external voltage

I strongly recommend you to check his work: https://github.com/jbaumann/attiny_daemon


## Some "available" solutions

There are some (not many) solutions out there. I had a few requirements and some goods-to-have. Initially price was not an issue and that was good since that allowed me to focus on the features:

  * Hat form factor (I wasn't looking for a USB power bank)
  * Capable to deliver power to the Raspberry Pi while charging
  * Capable to deliver enough power for a Raspberry Pi 3 running several services (but headless)
  * Battery monitoring from the OS
  * RTC (nice to have)
  * Compatible with LiPo batteries, other chemistries as a bonus

I don't want to favor one solution over another. But the [UPS PIco HV 3.0](https://github.com/modmypi/PiModules/wiki/UPS-PIco-HV3.0-A-Stack-450) might be the best option "available", and I quote "available" because it's out of stock everywhere I have checked. It sells for about USB 34, supports LiPo and LiFePO4 batteries, delivers up to 3A, has battery monitoring, RTC and a zillion of other goodies. There is also the [LiFePO4wered/Pi3](https://www.tindie.com/products/xorbit/lifepo4weredpi3). It is not a RPi hat but it's small enough. Supports only LiFePO4 batteries and lacks RTC. Sells for USD 42.

Of course these two products are not backed by big companies but by good professionals that have probably designed them to solve a need they had. And at some point they thought there was a market niche here. Actually both of them are on Tindie.

## Perfect is the enemy of good

The UPS PIco was almost perfect but I just couldn't find a way to buy it. So when I found the UPS Power Pack by GeekWorm that was less than USD 16 (without battery) I thought it was not too risky to give it a try.

{{< lazyimg "images/20180315_141429s.jpg" "Forget about the purple board on top for the moment ;)" >}}

It supports LiPo batteries, you can use the Raspberry Pi while charging and it can deliver up to 2A (1.4A while charging). It provides an I2C interface to check the battery charge status from the OS and support hot unplug. There is little information provided by the manufacturer but the people at [raspberrypiwiki.com](http://www.raspberrypiwiki.com/index.php/Power_Pack_Pro) have gathered some info about it.

You can buy a [GeekWorm Power Pack Pro v1.1 without battery](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338044841&mpre=https%3A%2F%2Fwww.ebay.com%2Fitm%2FGeekworm-Power-Pack-Pro-V1-1-UPS-HAT-Lithium-Battery-Expansion-Board-For-Raspber%2F302677513779) [Ebay] for less than 15€ or [with a 2500mA LiPo battery](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338044841&mpre=https%3A%2F%2Fwww.ebay.com%2Fitm%2FGEEKWORM-POWER-PACK-PRO-V11-LITHIUM-BATTERY-POWER-SOURCE-UPS-HAT-EXPANSION-BOARD%2F273057103494) [Ebay] for 22.5€.

## With a little help from my friends

The Geekworm Power Pack Pro has a main issue. It has two power output modes: via the secondary micro USB connector or via the GPIO and, incomprehensibly, the first one is the default output. That means that even thou the board "boots" when power is applied (for instance after a power blackout) it won't power the Raspberry Pi until you long click the side button.

What were they thinking about? This ruins the whole purpose of the hat.

Fine. This required some help. I needed a way to set the hat into GPIO output power mode automatically and, since we are "improving" the hat, why not adding an RTC and maybe a way to power a secondary device.

This last point was a requirement in one of the projects I was working on: I needed a way to power cycle a Texas Instruments SensorTag because I needed it in "discoverable" mode so the Raspberry Pi could connect to it. The SensorTag enters "discoverable" mode on boot or when you click one of the side buttons.

{{< lazyimg "images/geekworm-monitor-0.1-schematic.png" >}}

Let me introduce you the GeekWorm monitor. It sports an ATTiny85 that monitors the hat power supply and the RPi 3V3 pin and changes the power output mode if the Raspberry Pi is not being powered.

{{< lazyimg "images/20180315_141857s.jpg" >}}

But that's not everything. It also monitors a specific GPIO of the RPi and expects a HIGH there. If it's not it allows 3 minutes in case the Raspberry Pi is booting before power cycling it. This is necessary because in the event that a powerdown is executed by the OS the Raspberry Pi won't boot by itself unless it's power supply is reset.

{{< lazyimg "images/20180315_141751s.jpg" >}}

Imagine you are running on batteries (no external power) and the OS detects the battery is running low and it executes a preventive power down to avoid data corruption. But then power comes back and the battery is charging again and we would like to resume operations. The ATTiny85 will notice the Raspberry Pi is not running and will want to reset it. The only way to reboot the RPi is to disconnect it and reconnect it. Well, changing the power output mode to USB and back to GPIO does just that.

{{< lazyimg "images/20180315_141819s.jpg" >}}

Finally the board also features a DS3231 RTC backed by a coin cell and a secondary connector to power an external device via a MOSFET driven by the Raspberry Pi.

You might have notice the purple. Yes, you can [order the GeekWorm Monitor from OSH Park](https://oshpark.com/shared_projects/qaalNXjU).

### Normal operation

{{< lazyimg "images/20180315_141957s.jpg" >}}

Let me summarize the operation:

  1. Under normal circumstances, the USB cable will be connected to the hat power input at all times, powering and charging the LiPo unless there is a power outage.
  2. If that happens the battery will start powering the Raspberry Pi immediately so it won't reset. And the battery will start discharging...
  3. At this point the Raspberry Pi is monitoring the battery charge level via I2C and might decide to shutdown to prevent data corruption
  4. Eventually power will be back and the battery will start charging but the Power Pack is not powering the RPi
  5. The ATTiny85 gets its power from the 5V input line of the hat, so it wakes and checks the Raspberry Pi 3V3 pin and it reads low.
  6. The ATTiny85 pushed the button line to ground and changes the power output mode
  7. It will wait up to 3 minutes for a certain GPIO to be asserted true before changing the power output mode (and go back to 5).
  8. If the pin is asserted HIGH then everything is OK. The ATTiny85 will keep on monitoring the pin in case it goes low for whatever reason.

### The GeekWorm Monitor

#### The ATTiny85

The ATTiny85 is powered by the input 5V. This is very convenient since it sets the operation in a well known state: "I'm awake, so the battery is charging, so the Raspberry Pi should be running".

{{< lazyimg "images/20180315_141507s.jpg" "The red wire powers the ATTiny85, the black one is used to tie the button to ground to change the power output mode" >}}

Changing the power output mode is done pushing down the button line via a transistor for 5s effectively emulating a button push. The ATTiny85 will do that if it detects the Raspberry Pi is not being powered or when it needs to power cycle it.

Flashing the ATTiny85 is done using a USB like the one below (you can buy it here [USBASP Programmer](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338044841&mpre=https%3A%2F%2Fwww.ebay.com%2Fitm%2F1PCS-USBASP-USBISP-AVR-Programmer-Adapter-10-Pin-Cable-USB-ATMEGA8-ATMEGA128-CA%2F142129678652) [Ebay]). You might remember I used the same "edge connector" with my [Solr project](/post/solr-digital-wrist-watch/).

{{< lazyimg "images/20180315_142356s.jpg" >}}

The code is very straight forward.

```
#include <Arduino.h>

#define MODE_GPIO           0       // Connected to the Geekworm mode button
#define POWER_GPIO          1       // Connected to the RPi 3V3
#define RUNNING_GPIO        2       // Connected to the RPi GPIO set at local.rc

#define CHECK_DELAY         5000    // Run check every 5 seconds
#define CHECK_RUNNING       36      // Check RPi GPIO every 36 delays (3 minutes)
#define CHANGE_MODE_DELAY   5000    // Hold mode button down for 5 seconds

unsigned int count = 0;

void pressButton() {
    digitalWrite(MODE_GPIO, HIGH);
    delay(CHANGE_MODE_DELAY);
    digitalWrite(MODE_GPIO, LOW);
    count = 0;
}

void setup() {
    pinMode(MODE_GPIO, OUTPUT);
    pinMode(POWER_GPIO, INPUT);
    pinMode(RUNNING_GPIO, INPUT);
    digitalWrite(MODE_GPIO, LOW);
}

void loop() {

    // Every X seconds
    delay(CHECK_DELAY);

    // Check if RPi3 is being powered
    bool powered = digitalRead(POWER_GPIO) == HIGH;

    // Change mode if RPi3 is not being powered
    if (!powered) pressButton();

    // Every X delays
    count = (count + 1) % CHECK_RUNNING;
    if (0 == count) {

        // Check GPIO
        bool idle = digitalRead(RUNNING_GPIO) == LOW;

        // Press button if GPIO is not HIGH
        // If switching off,
        // loop will switch it on again after CHECK_DELAY seconds
        // efectively resetting the RPi
        if (idle) pressButton();

    }

}
```

### Saying I'm alive

I said the ATTiny85 is monitoring the 3V3 pin to know if the Raspberry Pi is being powered but it's also monitoring a GPIO to know if it's actually running. It expects a HIGH value there and will wait 3 minutes to see it before rebooting the RPi.

Forcing the Raspberry Pi to output a HIGH in a certain GPIO is very easy and does not require any special language or library, you just have to use the /sys/class/gpio path. Edit the /etc/rc.local file and add this contents before the exit line:

```
echo 14 > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio14/direction
echo 1 > /sys/class/gpio/gpio14/value
```

### Monitoring the battery

I used a python script supervised with supervisor to monitor the battery level and power down the Raspberry PI if it goes below a certain threshold for a certain time. Since the code is too long (and includes several files) to copy it here I have created a Gist with it here: [Geekworm battery monitor script](https://gist.github.com/xoseperez/c29072f4ce548f7bf7ce5129642fcffb). The only dependency is ruamel.yaml to read the YAML configuration file.

Note that the code is conservative and requires two different conditions to perform a power down:

  * The battery level must be below a certain threshold
  * The battery must have been continuously discharging for a certain amount of time

If either of this two conditions becomes false the power down is not executed or cancelled.

### Prototyping zones?

Just a side note so you don't do the same mistake I did. Do you see those two prototyping zones in the board?

{{< lazyimg "images/20180315_141632s.jpg" >}}Well, they are not. They are all connected to ground... maybe a cooling mechanism?

		

---
author: Xose
comments: true
date: 2017-02-13 22:14:16+00:00
layout: post
slug: the-mysterious-ic
title: "The mysterious IC"
post_id: 1541
image: "images/20170213_221200x.jpg"
categories:
- Learning
- Projects
tags:
- esp8266
- espurna
- itead
- itead studio
- pogo pin
- psa-b
- pulse
- rocketbook
- songle
---

Sometimes Chinese manufacturers throw a mysterious, unlabelled, IC into their designs so we can spend a few hours trying to figure out what they are and what they do. It's such fun! I've been playing with one of those this afternoon, trying to answer those questions but also trying to understand why! Why is that chip there? Why did someone decided she needed that chip there?

{{< lazyimg "images/20170210_142531x.jpg" >}}

Some weeks ago a user of [ESPurna](https://github.com/xoseperez/espurna) asked me if the firmware supported [Itead's 1CH self-lock/inching board](https://www.itead.cc/smart-home/inching-self-locking-wifi-wireless-switch.html). My answer was "why not" since all Itead's products are very much alike. Wrong. This one is different. Let me summarise why:

  * There is no entry in the Itead's wiki for the device
  * There are no schematics, drawings, in the store
  * It uses (and brings out) a Songle SRD-05VDC-SL-C SPDT relay (there is only one other product using this relay in Itead Studio store)
  * It uses [Itead Studio PSA-B module](https://www.itead.cc/smart-home/psa-01.html)
  * It does not have an AC/DC transformer
  * My module supports 12VDC connection
  * It features a second button exclusively for the pulsing feature
  * There is no header to program the PSA-B module

All these things together kept me wondering... is this an Itead's product?

<!-- more -->

I have no idea but I guess Itead sell products from other manufacturers in their web page (like everybody else). Actually you can find the same module or very similar **under different brands** on Aliexpress (like this one [here](http://s.click.aliexpress.com/e/UnAaAqb) [Aliexpress]). In this case the selling point is its "inching" feature. I call it **"pulse" feature**. There is a hardware button you can use to set the module in self-locking mode (basically like any smart device out there) or in inching mode, where the relay will change back after 1 second. Then you can connect your load to the NO or the NC terminals to suit your needs: always ON and turn OFF for 1 second or the other way around.

{{< lazyimg "images/20170210_142515x.jpg" "The button closer to the board edge is the inching button. It is next to an LED that show the current inching status: self-locking (or normal) when lit, inching mode when off." >}}

## Discovering

By discovery I mean to know what GPIOs are connected to what. The initial discovery was done why the same user that was asking for support. Using the typical Itead configuration (relay on GPIO12, LED on GPIO13 and button on GPIO0) worked fine. But we could not find where the inching button or LED were connected to.

{{< lazyimg "images/IM150310008-PSA-PinMapx.png" >}}

The "schematic" of the PSA-B module in Itead's page suggests that all those NC (non-connected) terminals are actually connected to GPIO14, GPIO15, TXD, RXD and RESET. But 14 and 15 did not seem to work with the second button.

So I asked Itead if I could have a sample of the board and they sent me one. Now: there are two versions of the board in Itead's store: the 5V one and the 5V/12V one. Both have a PSA-B module, a Songle relay with COM, NC and NO screw terminals, two buttons and a microUSB port for power. But the 5V/12V one has also a screw terminal and a L7805 linear regulator.

I got the second, but not exactly the same model they have pictures of. Instead mine had a second 6 holes unpopulated header by the relay and an LED next to the inching button. Also, in my model the 4 hole header is not labelled. Because it is connected to the mysterious chip! I guess that in the 5V model the header is connected to the WiFi module.

## The mysterious IC

{{< lazyimg "images/20170210_142545x.jpg" "Here it is!! On the center top of the bottom of the board. Unlabelled. Unknown. Unnecessary. Mysterious." >}}

Grabbed my tester and my RocketBook and started sketching some notes. The mysterious IC is (obviously) managing the inching button and functionality. Contrary to some board by Itead (like the Dual), the main button is attached to the PSA-B module but since the mysterious IC needs to know about the relay being triggered it sits in the middle between the PSA-B module and the relay.

The 4 hole header looks like a programming header for the mysterious chip. Whilst the 6 hole header is only using 3: 5V, GND and pin to trigger the main button that could be useful if you use a case for the board (you should somehot, high voltage there).

{{< lazyimg "images/Rocketbook-2017-02-13-205025-Page002.jpg" "I never said I was good at drawing" >}}

There are two missing resistors (R4 and R5) that were designed to get rid of the unkown chip and drive the relay directly from the KEY pipn of the PSA-B module.

But the reality is that all the inching functionality is out of the hands of the ESP8266 based module. So bad luck here. But, why did they decide to add this chip? There is no need for it, the PSA-B module has 2 free GPIO that could be used for the inching button and the LED...

## Using it with ESPurna

### Flashing the PSA-B module

Since the header is not connected to the module flashing it is a bit harder. But not much. The main button is tied to GPIO0 so you can easily enter flash mode powering the board while pressing the button (the one that's closer to the electrolytic capacitors).

Pins 7, 8 and 9 of the PSA-B module are RX, TX and GND (check my drawing above). I guess a 3-pin header might work but I happened to have some really tiny pogo pins that fit inside a female header just tight, so that's what I did. **An easy pogo-programmer**.

{{< lazyimg "images/20170213_221200x.jpg" >}}

### OK, be quite and do not disturb

Since I don't have access to the mysterious chip, I just let it there. Set it to self-locking mode (LED on) and forget. Be quite and do not disturb. I cannot use the hardware inching button but I have that same functionality from the web interface of ESPurna. Actually not the same: better. **In ESPurna I can define the toggle back timeout** (it's 1 second fixed in the stock version).

## Actually, there is something good about it

Actually, there is one good thing about the unkown IC. It has some kind of memory and stores the last status of the relay and the inching mode. That means that a reset in the PSA-B module (either user-driven or caused by the WDT) does not make the relay flicker.

But I still don't know why...		

---
author: Xose
comments: true
date: 2014-04-02 22:50:24+00:00
layout: post
slug: rentalito-goes-spark-core
title: "Rentalito goes Spark Core"
post_id: 619
image: "images/rentalito.sparkcore.small_.jpg"
categories:
- Code
- Projects
tags:
- arduino
- dht22
- ir remote control
- mqtt
- rentalito
- spark core
- wsn
---

[The Rentalito](http://tinkerman.eldiariblau.net/the-rentalito/) is a never ending project. It began as a funny project at work using an Arduino UNO and an Ethernet Shield, then it got rid of some cables by using a Roving Networks RN-XV WIFI module, after that it supported MQTT by implementing Nick O’Leary’s [PubSubClient](https://github.com/knolleary/pubsubclient) library and now it leaves the well known Arduino hardware to embrace the powerful [Spark Core](http://www.spark.io) board.

{{< lazyimg "images/rentalito.sparkcore.small_.jpg" "Spark Core powered Rentalito - prototype" >}}

## Spark Core

The Spark Core is a development board based on the STM32F103CB, an ARM 32-bit Cortex M3 microcontroller by ST Microelectronics, that integrates Texas Instruments CC3000 WIFI module. It makes creating WIFI-enabled devices extremely easy.

The main benefits of migrating from the Arduino+RN-XV bundle to Spark Core are:

  * Powerful 32 bit microcontroller
  * Reliable WIFI connection (auto-reset on failure)
  * Smaller foot print
  * OTA programming (even over the Internet)

And of course it's a good opportunity to add a couple of features: temperature and humidity sensor and IR remote control to switch the display on and off or fast forward messages.

## MQTT support

Spark forum user Kittard [ported Nick's MQTT library to the Spark Core](https://community.spark.io/t/mqtt-library-and-sample/2111). Since the Spark team implemented the Wiring library for the Spark Core it normally takes very little effort to port Arduino code to the Core.

The library supports both subscribing and publishing. You can subscribe to as many topic as you wish and you get the messages in a callback function with the following prototype:

```
void (*callback)(char*,uint8_t*,unsigned int);
```

From here it's very easy to just store the last value for every topic we are subscribed to, along with some metadata like the precision or the units.

Publishing is even easier. A simple call to publish method is all it takes:

```
bool PubSubClient::publish(char* topic, char* payload);
```

## DHT22 support

DHT22 support is provided by another port, in this case from Adafruit's DHT library for Arduino. Forum user wgbartley (this guy is from the Spark Elite, people that basically live on the Spark forums) published the ported [DHT library for the Spark Core](https://gist.github.com/wgbartley/8301123) in github.

Recently another user (peekay123, also from the Elite) has published a [non-blocking version of the DHT22 library](https://github.com/pkourany/idDHT22Spark). It uses interrupts to trap transitions on the data line and calculate timings and a state machine to track message structure. The previous one performs all the calculations in a single method call and disables interrupts to keep control over the timing.

## HT1632C dot matrix display support

This one I ported it myself from my previous fork of the original [HT1632C library for Arduino](https://code.google.com/p/ht1632c/) by an anonymous user. You can checkout the code at bitbucket ([Holtek's HT1632C library for the Spark Core](https://bitbucket.org/xoseperez/sparkcore-ht1632)). The library supports:

  * 32x16 displays
  * drawing graphic primitives (points, lines, circles,...)
  * drawing single colour bitmaps
  * printing chars and texts in fixed positions or aligned to the display boundaries
  * red, green and orange colours
  * 23 different fonts
  * 16 levels of brightness
  * horizontal and vertical scroll

It's still a work in progress but it's almost in beta stage.

## IR remote support

I had an old-verison Sparkfun IR Control Kit (check it [here](http://garagelab.com/profiles/blogs/tutorial-how-to-use-the-ir-remote-control-with-arduino)) laying around and I thought it was a good idea to have a way to switch the LED display on and off. I struggled for a couple of days with the IRRemote library for Arduino ([like some others](https://community.spark.io/t/ir-transmitter-recorder/1004)) but finally I quit and I decided to implement my own simpler version.

The approach is very much the same as for the DHT22 non-blocking library before: an interrupt-driven routine that calculates and stores pulse lengths and a state machine to know where in the decoding process we are.

```
void ir_int() {

    if (ir_status == STATUS_READY) return;

    unsigned long now = micros();
    unsigned long width = now - ir_previous;

    if (width > BIT_1) {
        ir_pulses = 0;
        ir_status = STATUS_IDLE;
    } else {
        ir_data[ir_pulses++] = width;
        ir_status = (ir_pulses == 16) ? STATUS_READY : STATUS_DECODING;
    }

    ir_previous = now;

}
```

Then in the main loop we check if the message is ready, perform the corresponding action and reset the state:

```
if (ir_status == STATUS_READY) {

    if (millis() > ir_timer) {

        int key = ir_decode();

        switch(key)  {
            case 10: next(); break;
            case 18: previous(); break;
            case 34: brightness(1); break;
            case 42: brightness(-1); break;
            case 2: toggle(); break;
            default: break;
        }

    }

    ir_status = STATUS_IDLE;
    ir_timer = millis() + IR_DEBOUNCE;

}
```

The decoding is a matter of translating pulse lengths to bits.

```
int ir_decode() {
    unsigned int result = 0;
    for (byte i = 0 ; i < 16 ; i++)
        if (ir_data[i] > BIT_0) result |= (1<<i);
    if (REMOTE_CHECK != (result & REMOTE_CHECK)) return 0;
    return result >> 8;
}
```

It's very important to add some noise reduction components around the IR receiver, otherwise you will only get streams of semi-random numbers every time you press a key in the remote. You can check the datasheet for the specific model you are using (for instance, check the "application circuit" in the first page of the [TSOP382 IR receiver](https://www.sparkfun.com/datasheets/Sensors/Infrared/tsop382.pdf) Sparkfun sells) or check the schematic in the next section.

## Schematic and layout

The project is mostly a software Frankenstein (well, not quite so, you can check the code in [bitbucket](https://bitbucket.org/xoseperez/sparkcore-rentalito)). The hardware part is pretty simple. You can get all the information you need from tutorials and datasheets. My only advice is to add noise suppression circuitry around the IR receiver.

{{< lazyimg "images/schema.png" "Schematic" >}}

## Next steps

I'm ready to try to do my first home etching experiment and this project looks like a good candidate. The first step was to create a board layout using Eagle. The board should be one-layered and the form factor the same as the Arduino, so it could use the same holes the Arduino did in the display frame.

And this is the result:

{{< lazyimg "images/board.png" "Board layout" >}}

As you can see it's mostly one-layered, I will have to use one wire to connect the DHT22 sensor VCC pin. The layout looks promising and I'm eager to see the final result. Hopefully I will post it here soon.

Thanks for reading!		

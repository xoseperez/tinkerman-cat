---
author: Xose
comments: true
date: 2017-09-03 16:08:52+00:00
layout: post
slug: using-google-assistant-control-your-esp8266-devices
title: "Using Google Assistant to control your ESP8266 devices"
post_id: 1972
image: "images/header.jpg"
categories:
- Learning
- Tutorial
tags:
- allo
- alexa
- amazon dot
- amazon echo
- avs
- belkin wemo
- curl
- esp8266
- espurna
- fauxmo
- fauxmoesp
- google assistant
- http api
- hue
- ifttt
- node-red
- philips hue
- raspberry pi
- rest
- reverb
- webhooks
- wemo
- yonomi
---

In my last post I tried to explain how to [access your IoT devices at home from the Internet in a secure way using a reverse proxy](/post/secure-remote-access-to-your-iot-devices/). Truth is that I had it running since maybe 6 months ago without giving it too much use until recently. Reason? My Nexus 5 had been having serious problems (battery not charging, screen broken, earpiece not working,...) and I decided to replace it with a new mobile phone and the new one has **Google Assistant** built in.

So one of the first things I have tried is to make Google Assistant toggle my smart switches flashed with [**ESPurna**](https://github.com/xoseperez/espurna). Nad as it turns out it's not hard to do but -at the moment- you have to relay on a cloud service like [**IFTTT**](https://ifttt.com) (IF This Then That). This is a key difference with **Amazon Alexa** services you have to be aware. It has some benefits but also some drawbacks.

<!-- more -->

## Amazon Alexa vs. Google Assistant

The "vs." might sound a bit like a fight but I'll be focusing on few aspects on the way the two work when managing IoT devices like smart switches.

First let me name things correctly. Amazon Alexa is the service behind devices like [**Amazon Echo**](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338044841&mpre=http%3A%2F%2Fwww.ebay.com%2Fitm%2FAmazon-Echo-w-Alexa-Voice-Control-Personal-Assistant-Bluetooth-Speaker-Black-%2F182735756949) [Ebay] or **[Amazon Dot](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338044841&mpre=http%3A%2F%2Fwww.ebay.com%2Fitm%2FNew-Sealed-Amazon-Echo-Dot-Streamer-Black-alexa-2nd-generation-%2F322494480912) **[Ebay]. The same way Google Assistant is the intelligence behind **[Google Home](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338044841&mpre=http%3A%2F%2Fwww.ebay.com%2Fitm%2FGoogle-Home-%2F222571438996)** [Ebay]. On Android or iOS you have, for instance, [**Reverb**](https://reverb.ai), which is an app that uses Amazon Alexa to answer your commands. You can use it with your smart home devices that support Alexa too. Same thing for Google Assistant or [**Allo**](https://allo.google.com/), both apps use Google Assistant servers. So I'm not focusing on the devices here but the service underneath, although some differences between the two have to do with the devices themselves.

### Amazon Alexa

{{< lazyimg "images/amazon.jpg" "Amazon Echo & Dot devices" >}}

On one hand the standard way to use Amazon Alexa is using skills, either the ones you can create or third party ones available in the Amazon Alexa web page. Here you have skills to control your [**Philips Hue**](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338044841&mpre=https%3A%2F%2Fwww.ebay.com%2Fsch%2Fi.html%3F_from%3DR40%26_trksid%3Dp2047675.m570.l1313.TR0.TRC0.H0.XPhilips%2BHue.TRS0%26_nkw%3DPhilips%2BHue%26_sacat%3D0) [Ebay] or your [**Belkin Wemo**](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338044841&mpre=https%3A%2F%2Fwww.ebay.com%2Fsch%2Fi.html%3F_nkw%3DBelkin%2BWemo) switches [Ebay], amongst others.

The case of the Wemo switches is well known since the protocol was reverse engineered. Basically, your Amazon Alexa powered device is listening for a key work to wake up ("Alexa"or "Echo"), then it records your command and sends it to Amazon servers to decode it. The command, now converted in something your device can understand, comes back to it and it executes the action. For Wemo switches it an be a discovery process (Belkin uses a standard uPNP process) or an event, a custom TCP connection requesting the switch to turn on or off.

Since we know the protocol we can emulate being a Wemo switch and accept commands from an Amazon Echo or Dot, that's how my [**fauxmoESP** library for ESP8266](/post/emulate-wemo-device-esp8266/) works. Unfortunately, not all Amazon Alexa powered devices support the Wemo protocol the same way Echo or Dot does, and that means that if you are using a [**AlexaPi**](https://github.com/alexa-pi/AlexaPi) on a **Raspberry Pi** or [**Yonomi**](https://yonomi.co/) on your mobile phone you are out of luck. Reverb is a brilliant exception, since it looks like it communicates with your Amazon devices and commands them to execute the actions.

Anyway, for this specific use case (emulating being an Alexa compatible smart switch), only the translation from your voice command to machine language is performed outside your network. The communication with **Alexa Voice Service (AVS)** is done using HTTPS so, as long as you trust Amazon you are good and secure.

### Google Assistant

{{< lazyimg "images/google.jpg" "Google Home" >}}

As for Google Assistant (GA), everything is a service, as you might have guessed. Google is partnering with different providers to include options for their services on Google Assistant. One of such partners is [IFTTT](https://ifttt.com) (IF This Then That) which means you can create custom behaviours responding to your voice commands. That's pretty cool and IFTTT has already a ton of different options. One of this options is WebHooks, which provides an API to perform HTTP requests.

So the standard way to interact with your IoT devices using Google Assistant implies that:

  1. You speak your command
  2. Google voice services transcript it and search the different providers for one that can handle it, including IFTTT
  3. IFTTT grabs the command and tells GA "yeah, I can do that"
  4. IFTTT translates it again based on an applet you have defined
  5. The action in your applet instructs IFTTT to tell WebHooks to do an HTTP request
  6. WebHooks performs the request to an address that points to your device
  7. Your device executes the action

Some very important things are missing in the previous explanation. On point 6 you have to have a way to redirect that request to your device but that's exactly what I was explaining on my previous post about [secure access your network](/post/secure-remote-access-to-your-iot-devices/). And on point 7 your device has to understand the command. Well, ESPurna does have an HTTP API!!

The main differences with the way Amazon Alexa works are obvious here. You have 3 different cloud services involved here: Google Assistant itself, IFTTT and WebHooks. Again it's a matter of trusting them (!!). And the final command comes from outside your network, always.

## HTTP API for your DIY IoT device

Stop writing and let's do something to make this work. First thing is to be able to send an HTTP request to your device from outside your network. If you have read my previous post about [secure accessing your home network](/post/secure-remote-access-to-your-iot-devices/) you already know the basics. Go ahead and install a **reverse proxy with HTTPS support**. That's the first thing to do.

Second requirement is to have an device with an **HTTP API**. Some commercially available do. If you are a maker and you use **ESP8266** on your home devices (and if you are reading this blog chances are you are) you know some open source firmwares for ESP8266-based smart switches/sensors do have an HTTP API. And [ESPurna](https://github.com/xoseperez/espurna) does have. Of course, you can also build your own firmware or use an REST library like [**aRest.io**](https://arest.io/).

For instance. I can control my living room light using my browser by browsing to `http://livinglamp.local/api/relay/0?apikey=0123456789ABCDEF&value=2` (faked API key :). That URL will toggle the light (value=2 means "toggle", 0 for "off" and 1 for "on").

From the command line using cURL I would do a PUT request instead since it's more ortodox (ESPurna supports GET when setting values out of convenience):

```
curl -X PUT http://livinglamp.local/api/relay/0 --data "apikey=0123456789ABCDEF&amp;amp;value=2"
```

But ESPurna uses HTTP, not HTTPS. Inside your home network it's fine (mostly) but that's not an option from the outside. Here's where the reverse proxy comes to play. The configuration in my previous post maps this device inside my home to a certain domain and URL inside the domain and only allow HTTPS communication, so I can do the same test from a machine outside my network, using also cURL:

```
curl -X PUT https://geek.fibonacci.com/livinglamp/api/relay/0 --data "apikey=0123456789ABCDEF&amp;amp;value=2"
```

Here "geek.fibonacci.com" is the example subdomain I'm using. As you can see the protocol and URI has changed from `http://livinglamp.local/` to `https://geek.fibonacci.com/livinglamp/`, no more changes are needed. And it does toggle my ESPurna powered lamp!

## Creating an IFTTT applet

{{< lazyimg "images/GA_02.jpg" >}}

Now let's configure the applet in [IFTTT](https://ifttt.com). You will have to create an account if you have not created one before. Then go to "My Applets" and click on "New Applet". An applet is a combination of a trigger and an action. Both are provided by "services". Our **trigger** will be provided by Google Assistant. Click on the "this" word and look for "google assistant" on the search box.

Each provider could provide one or more services, here we need the "Say a phrase with a text ingredient". We will use the text ingredient to specify wether we want to switch it on, off or toggle the switch. As you can see in the screenshot below you can configure several (up to 4) different ways to say the command and the dollar sign will be replaced by the ingredient.

Once you have it configured click next and choose an **action**. In this case the only one provided by WebHooks, the "Make a web request". We will just fill the fields with the same option as in the cURL command before replacing the value field in the body with "{{TextField}}", to pass the ingredient that Google Assistant has identified.

{{< lazyimg "images/GA_02i3-819x1024.jpg" >}}

Finally it will ask you to name the applet and once activated you are ready to go. In my case I click and hold the home button in my mobile phone to wake Google Assistant and speak "Turn on living lamp" and enjoy.

NOTE: As you might have noticed the "ingredient" will be a word. ESPurna 1.9.3 (still not realeased) supports "ON", "OFF" and "TOGGLE" to change the status of the relay. If using a previous version or your device only supports numbers you might want to test the "Say a phrase with a number" trigger service instead. In this case you might want to be able to say "Set living room status to 1", for instance. Less human-friendly but still usable.

All an all this approach is more flexible than that from Amazon, even thou there are more services involved. Using IFTTT alone, without Google Assistant, can provide an easy way to do schedules or actions depending on, for instance, an email or a tweet. That's something you can also do with** Node-RED** for instance but IFTTT provides an easier way to do it...

As always, comments welcome!

		

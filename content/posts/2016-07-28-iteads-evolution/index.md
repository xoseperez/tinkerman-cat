---
author: Xose
comments: true
date: 2016-07-28 13:45:41+00:00
layout: post
slug: iteads-evolution
title: "ITead's Evolution"
post_id: 797
image: "images/800px-Sonoff_TH10A16A_design.png"
categories:
- Analysis
tags:
- esp8266
- esp8285
- home automation
- iot
- itead
- psa wifi module
- psf-a85
- s20 smart socket
- slampher
- sonoff
- wifi
---

I was not the first to arrive at the party but since I discovered the ESP8266 I've been enjoying it. Then I stumbled upon the Sonoff and dude was I amazed. They are cheap and so very hackable you cannot help buying them, tear them open and customize them.

Sure they are not CE or UL compliant, yet. My previous post about adding a [custom RF module to a Sonoff HT](/post/adding-rf-to-a-non-rf-itead-sonoff/) got some visibility as it was [published at hackaday.com](http://hackaday.com/2016/07/07/dumbing-down-a-smart-switch/). Most of the commentors there  where concerned about safety measures in the device. The truth is that early Sonoffs looked more like products for the DIY market, not for end customers.

But ITead's home automation product line is evolving quite fast. It all started with several Sonoff models (with or without RF, with or without temperature and humidity sensors, with or without AC/DC transformer) and the [Slampher](https://www.itead.cc/slampher-wifi-wireless-light-holder.html) I'll be reviewing it soon.

<!-- more -->

I first ordered 3 Sonoffs and once I have them deployed at home I ordered 3 more. There were some minor but significant changes on this second batch. First there was a label with a text in chinese that (according to Google Translator) says "tore invalid", preventing the unwanted (!!) opening of the case, as if they were telling you: from here on, under your responsibility. Also they used a slightly taller button that sticks out of the case almost 2 millimetres.

But the most curious change is that they added blobs of tin to the unpopulated RF module header!! Wow!! I almost feel like if it was my fault. I don't really know the reason: is it because they don't want you to solder anything there? or is it an improvement on their production flow? The first option is somewhat hard to believe... anyone willing to solder anything there would know how to remove those blobs of tin (or even use them). As for me I just heat them a bit with the iron tip and then slam down the board against the table. Anyway: small changes and no layout redesign.

{{< lazyimg "images/IMG_20160728_134410x.jpg" "A Sonoff from the second batch I bought. Notice the label and the button popping out of the hole." >}}

{{< lazyimg "images/20160709_222811x.jpg" "Blobs of tin ion the RF header" >}}

But things are changing fast. If you visit ITead's online store you will probably see new products every one or two weeks. And they just released the new [Sonoff TH 10A/16A](https://www.itead.cc/sonoff-th.html). Now this is a complete redesign with several interesting features:

  * It still has a programming header and a button (hopefully attached to GPIO0), good!
  * Up to 16A relay for the Sonoff TH 16A.
  * No enclosure included, but there is a free 3D model available.
  * A 3.5 jack to attach sensors, they are selling 3 different temperature and humidity sensors, but probably any sensor you can think of that requires power, ground and a digital pin would fit (PIRs, Sharp distance sensors or Parallax PING,...).
  * There are some changes in the AC/DC design, they added an input fuse and a thermistor (?)  at least.
  * Two LEDs, let me guess: there is one attached to the same GPIO the relay is.
  * They are using push terminals, not screw ones. But even thou there are 6 of them, apparently they are not meant for NC/NO operation, according to the wiring diagram in the product page.

{{< lazyimg "images/sonoff_th_smart_switch-16.jpg" "Sonoff TH 16A. Picture by Itead." >}}

Pics don't show the bottom of the board, so no idea about track thickness or creepage, but the new design (with the input and output lines on the same side of the board) provides better isolation between safe and unsafe parts of the board.

There are other new products on their site, most notably the [PSF-A85 module](https://www.itead.cc/psf-a85.html), amongst the first to integrate Espressif ESP8285 chip (an ESP8266 with 1 Mbyte embedded SPI flash memory) and the [PSA Wifi module](http://wiki.iteadstudio.com/PSA_Series_Smart_Switch_Module) (although they are not selling them individually yet). This last one encapsulates smart switch functionality in one module (wifi, relay and configuration button) but it looks like you cannot modify (or even access to) its firmware. There is still little information about it so we will have to wait.

But that's not all. Last week I received a parcel from Itead with some products to review (the [Slampher](https://www.itead.cc/slampher-wifi-wireless-light-holder.html) and the [S20 Smart Socket](https://www.itead.cc/smart-socket.html)). In the box there was a flier with products you can manage from the eWeLink app. Even thou Itead's name is written nowhere most of the products there are available on their site. Except for two: the WiFi Wireless Wall Socket and the Snake Power (an articulated power strip with 3 individually addressable sockets and 2 USB ports). I bet they will show up in their web page soon...

{{< lazyimg "images/IMG_20160728_151908x.jpg" "eWeLink flier with teasing new products" >}}

All in all, Itead is rapidly evolving it's smart home product line, adding new products and redesigning old ones, probably with the goal of being CE/UL compliant soon. Some will argue than wifi is not the best technology for home automation (too power hungry, limited number of devices per AP,...). But right now is probably the best option for the non-makers out there, a plug-n-play solution for your home.		

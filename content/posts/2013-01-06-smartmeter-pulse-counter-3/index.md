---
author: Xose
comments: true
date: 2013-01-06 22:39:37+00:00
layout: post
slug: smartmeter-pulse-counter-3
title: "Smartmeter pulse counter (3)"
post_id: 94
image: "images/schematic1.png"
categories:
- Learning
---

The[ smartmeter pulse counter](http://tinkerman.eldiariblau.net/smartmeter-pulse-counter-1/) will be the first standalone sensor I will deploy so **power economy is a requirement**.

I will have to power an Arduino Pro Mini and an Xbee radio. I plan to power the Xbee from the 3V3 regulated output. The Arduino VCC output can provide as much as 200mA which is far enough to power the Xbee. The built-in regulator requires at least 3.35V and up to 12V so 3 AA cells will provide enough potential to drive the whole setup. But for how long?

The Arduino will wake briefly to count every pulse and the radio has been configured to wake once every minute for 200ms and ask the uC to report the count number for since the last report. The figures for the number of pulses are based on an average consumption of 300W, that is one pulse every 3 seconds or 1200 per hour.

Once I had breadboarded the sensor I started to take some measurements in different operational statuses. You can check my results in the following table:

|Arduino|XBee|mA|ms/event|events/h|ms/h|mA (avg)|
|---|---|---|---|---|---|---|
|normal|transmiting|~54|200|60|12000|0.180|
|normal|sleep|7.940|1|1200|1200|0.003|
|sleep|sleep|0.290|-|-|3586800|0.289|
|||||||0.472|

So 0.472 mA is the average power consumption of the sensor. The AA cells I will be using are rated 2000mAh so the sensor should be able to run for 4237 hours or about 177 days. Not bad. Off course there is room for improvement, like 290uA when doing nothing is really too much but I prefer to give it a try now before over optimizing. After all these are theoretical values and I want to know if the will match the reality.

I plan to test different approaches to power the next sensors: coin cells, LiPo cells, solar panels, rechargable NiMH batteries,...		

---
author: Xose
comments: true
date: 2017-11-10 17:27:32+00:00
layout: post
slug: yet-another-wifi-light-bulb
title: "Yet another WiFi light bulb"
post_id: 2127
image: "images/20171108_124929s-1200x800.jpg"
categories:
- Analysis
- Hacking
tags:
- ai-thin
- ailight
- aithinker
- arilux
- esp8266
- espurna
- home assistant
- itead studio
- light bulb
- lux
- luximeter
- my9231
- my9291
- noduino
- openlight
- sonoff b1
- uni-t ut383
---

Eight months ago I reviewed and hacked the [**AiLight** WiFi light bulb by AiThinker](/post/ailight-hackable-rgbw-light-bulb/). By the time there was a number of people doing the same because of a key reason: it sports an **ESP8266** microcontroller and it is based on the **OpenLight** by **Noduino**, that had already provided open source code for the LED driver inside, the **MY9291**.

Let time pass and I was doing the same with the [**Sonoff B1** light bulb by Itead Studio](/post/sonoff-b1-lights-and-shades/). That was two months ago and the conclusion was that the AiLight is brighter the the B1 but the lacks warm white channel the Sonoff bulb has.

And now here I have yet another WiFi light bulb, the **Arilux E27 Smart Bulb**. It looks pretty much like the other two, same shape, same microcontroller, same driver, but different base again and most important: different LEDs. So how does this one compare to the other ones?

<!-- more -->

## Arilux E27 Smart Bulb

Arilux has a big catalog of lights, from security outdoor lights to fancy Christmas lights or disco lights. The Arilux E27 (its proper name is "Smart Bulb") it's a 7W RGBW light bulb with WiFi (IEEE 802.11b/g/n) that can be used on any E27 socket around the globe (85 to 285VAC supported).

{{< lazyimg "images/20171109_142859s.jpg" >}}

{{< lazyimg "images/20171109_142757s.jpg" >}}

It has an ESP8266 with a 1Mb flash memory like it's siblings and the same light driver as the AiThinker light, the 4-channel MY9291. The Sonoff B1, on the other hand, uses two daisy-chained **MY9231**, three channels each and uses 5 of the 6 channels available.

{{< lazyimg "images/20171109_142639s.jpg" >}}

### Flashing it

Of course, you might want to use custom firmware with your new light bulb, something that supports **MQTT**, **Home Assistant**, **REST API**, with a nice web interface. Something like **[ESPurna](https://github.com/xoseperez/espurna)**, for instance :)

Anyway, you might need to wire the bulb to flash it the first time, from then on OTA is a better option. Like the other two light bulbs, this one has a properly labeled pads on the main circuit board where you can solder thin cables and use your USB2UART programmer.

{{< lazyimg "images/20171108_125303s.jpg" >}}

{{< lazyimg "images/20171108_125324s.jpg" >}}

Wiring it is simpler than it may seem. Heat the pad, leave a drop of tin on it, tin your wire also and then heat them together. You will only need a fraction of a second to have the cable connected to the pad. Then just connect it to your programmer. Remember it's a 3V3 device and you have to **connect IO0 to ground** before powering the board so it enters into flash mode. The follow the instructions of your firmware of choice (here the ones for [ESPurna with AiLight](https://github.com/xoseperez/espurna/wiki/Hardware#markdown-header-ai-thinker-ai-light-noduino-openlight)) and you are good to go.

## AiLight vs. Sonoff B1 vs. Arilux E27

{{< lazyimg "images/20171109_143846s.jpg" >}}

### LEDs

These three bulbs are functionally identical but have obvious differences due to the type of LEDs they use (aside from the warm white channel in the Sonoff B1). The table below tries to summarize the differences between the three:

|Channel|Sonoff B1|Arilux E27|AiThinker AiLight|
|---|---|---|---|
|Red|3x 5050 RGB|6|6|
|Green|(see above)|4|4|
|Blue|(see above)|4|4|
|Cold White|8|8|8|
|Warm White|8|-|-|

But of course the number of LEDs is not enough to compare them. Actually you can see in the pictures that the power LEDs in the bulbs are quite different one from the other (5050 RGB LEDs aside).

{{< lazyimg "images/20170927_180232s.jpg" "Sonoff B1 LEDs, notice the 3 5050 RGB LEDs in the center board, too close to the antenna" >}}

{{< lazyimg "images/20171108_124912s.jpg" "The Arilux light bulbs, very similar to the AiLight but different kind of power LEDs" >}}

{{< lazyimg "images/20170224_210528x.jpg" "The power LEDs in the AiLight are bigger, brighter and evenly located, an overall better result" >}}

### Light power

So, how do they compare when talking about brightness? In my previous post about the Sonoff B1, I noticed the 5050 RGB LEDs were clearly not enough, much much less bright than the power LEDs in the AiLight. It was very obvious (you can check the pictures on that post), but subjective appreciation after all.

This time I have decided to do a more "scientific" experiment. As it happens I have several luximeters at home for another project, so why not use one of those to compare the three bulbs in a controlled environment. The **luximeter** I have used is a **UNI-T UT383**. You can buy one for around 12€ from Aliexpress ([UNI-T UT383 at Aliexpress](http://s.click.aliexpress.com/e/iiIamuv)) or Ebay ([UNI-T UT383 at Ebay](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=undefined&mpre=https%3A%2F%2Fwww.ebay.com%2Fitm%2FUNI-T-UT383-Digital-Luxmeter-Lux-FC-Meter-Luminometer-Photometer-200-000-Lux%2F322836771454)). It is on the cheap side and there is no information about how it performs for different wavelengths (only they have been calibrated at 2856K), but it should be enough to compare the three bulbs brightness.

{{< lazyimg "images/20171110_162814s.jpg" "UNI-T UT3838 luximeter" >}}

So here is the experiment set up: the three light bulbs have been tested using the luximeter at a distance of 50cm, without obstacles that might reflect light (>90 3D degrees clean around the light). I have recorded the value in luxes for each channel at 100% duty cycle (other channels off). I have also tested the three color channels together (red + green + blue) and all the channels together. This chart shows the results:

|Channel|Sonoff B1|Arilux E27|AiThinker AiLight|
|---|---|---|---|
|Red|0|3|51|
|Green|5|27|72|
|Blue|9|55|149|
|Red + Green + Blue|17|92|287|
|Cold White|227|192|282|
|Warm White|220|||
|All|461|282|548|

The AiLight power LEDs really make the difference. The Arilux colour LEDs perform better than the poor 3 5050 RGB LEDs in the Sonoff B1, but the later has the advantage of having a second set of white LEDs (warm white in this case) so when everything is at full power it's actually brighter than the Arilux, but still far from the AiLight.

### Price

So it looks like the AiLight might be the best option for most use cases, except when you need a warm white light bulb. The final argument might be the price.

|Product|Aliexpress|Ebay|
|---|---|---|
|Sonoff B1|14.09€ [[Sonoff B1 @ Aliexpress](http://s.click.aliexpress.com/e/NzVbqFu)]|11.90€ [[Sonoff B1 @ Ebay](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338044841&mpre=https%3A%2F%2Fwww.ebay.com%2Fitm%2FSonoff-B1-Smart-Home-Wifi-RGB-Light-Bulb-Wireless-Dimmable-RGB-LED-Lamp-6W-E27%2F253250105484%3Fhash%3Ditem3af6e1f08c%3Ag%3AK7IAAOSwb-tZzMHF)]|
|Arilux E27|11.24€ [[Arilux E27 @ Aliexpress](http://s.click.aliexpress.com/e/nUnMVbI)]|16.22€ [[Arilux E27 @ Ebay](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338044841&mpre=https%3A%2F%2Fwww.ebay.com%2Fitm%2FARILUX-E27-8W-RGB-White-Dimmable-Smart-WIFI-LED-Light-Bulb%2F202080967542)]|
|AiThinker AiLight||12.58€ [[AiLight @ Ebay](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338044841&mpre=http%3A%2F%2Fwww.ebay.com%2Fitm%2FESP8266-Smart-Home-Wifi-RGB-LED-Light-Lamp-Wireless-E27-Interface-%2F222460076781)]|

So you can buy any of the three light bulbs for around 12€ so there is no big difference here.

## Conclusions

The final verdict is very much the same as when I reviewed the Sonoff B1. If you need light power go for the AiLight. The Sonoff B1 can be a good option if you are looking for a warm white light since it's the only one that offers such possibility. Both the Sonoff and the Arilux lights can be a good option too when looking for a notification light, a traffic light kind of visual notification for instance. The Arilux colors are a bit brighter than the ones in the Sonoff B1 (those 5050 LEDs are not a good idea), but still way fainter than the ones in the AiLight. So for most cases, you will probably want to go for the AiLight unless you happen to find a good deal on any of the three...

**Update**: Timo Rohner has had some trouble importing Arilux light bulbs into Poland and he had to search for CE documentation for the devices. He sent me the documentation so you can also have it in case you will need it. Here you have the Certificate of Confirmity for Arilux CC-01 to CC-08. Thank you, Timo!

{{< lazyimg "images/arilux_ce-1.jpg" >}}		

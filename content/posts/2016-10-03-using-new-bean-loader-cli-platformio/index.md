---
author: Xose
comments: true
date: 2016-10-03 20:56:55+00:00
layout: post
slug: using-new-bean-loader-cli-platformio
title: "Using the new Bean Loader CLI from PlatformIO"
post_id: 1181
image: "images/20161003_230052s-1024x683.jpg"
categories:
- Learning
- Tutorial
tags:
- android
- bean
- lightblue bean
- linux
- node.js
- npm
- platformio
- punchthrough
---

Last Thursday PunchThrough, the people behind the LightBlue Bean and Bean+ boards, released their new Bean Loader, the application that allows you to upload new sketches to your beans. The great news about this is that, for the first time (!!!) the **Bean Loader supports Linux**!!! Yeeeha!

So I quickly looked for my 4 Beans that have been sad and forgotten in a components box for the last 2+ years and put them to work. It was not smooth, but there is a happy ending. So keep reading.

<!-- more -->

## Installing the Bean Loader CLI

This actually was straight forward. Just follow the instructions in the [Bean Load CLI Getting Started](https://punchthrough.com/bean/guides/getting-started/cli-loader) page. Just double check that you have the required versions of Node.js and npm.

{{< lazyimg "images/versions.png" "Node.js, NPM and Bean SDK versions" >}}

## Updating the Lightblue Bean's firmware

Two years ago, when I received my Lightblue Beans (well, when I backed them they were called "Cortados"), Linux support was missing and PunchThrough were not clear about their intentions about it. Their priorities were clear:

  1. Mac OSX
  2. iOS
  3. Windows 8.1
  4. Android

Being a Linux guy that hurt. And I was not the only one. [Some requested a refund](http://community.punchthrough.com/t/android-and-linux-support-or-a-refund-please/1081). Others tried to implement a [non-official API](https://github.com/jacobrosenthal/ble-bean). When **Android support** arrived (April 2015!!) I unpacked one of my Lightblue Beans and started playing with it. The App interface was horrible, user experience was really bad and even thou I was working with a tablet with physical keyboard I soon got fed up.

So now I had 4 Lightblue Beans with a 2 years old firmware and I soon started to see weird things: the log_serial was not working, neither the LED on the board, I could not change the bean's name or even upgrade the firmware!! I started reading the forums and apparently there was an API update somewhen in 2015 that was not backwards compatible. So I was stuck with a useless old firmware. People was walking around it installing old versions of the Bean Loader for MacOS that they were using as a "bridge" between old and new versions. Again, no info for people not using the fruit brand.

Well, why not try the same with the Android App? I removed the latest version from my mobile and looked for an old version of the Bean Loader App for Android. **I tried with the latest of the 1.X series**: version 1.1.1 and it worked. I could upgrade the firmware. Then I removed the old version and reinstalled the latest version from Google Play and tried again. Success!! It actually upgraded the board in two steps until it reached version 201606300000 Img-B. From here on (latest version is 201609290000) firmware updates doesn't work: the Android App does not trigger the update firmware activity and the CLI program_firmware command does not seem to work... But version 201606300000 seems to be good enough to get going.

## Setting up PlatformIO

The [bean-arduino-core repository at Github](https://github.com/PunchThrough/bean-arduino-core) has an outdated setup_platformio.sh script. So best way to get info about what to do to get up to date Lightblue Bean support on PlatformIO is to check what the "**install_bean_arduino_core**" command does. The basics are, like in the previous post for the [4Duino-24](/post/4duino-24-upside-down/), to copy custom core files, pin definitions, libraries and board definitions manually.

So first checked out the [bean-arduino-core repository](https://github.com/PunchThrough/bean-arduino-core) and manually performed these actions:

|action|from|to|
|---|---|---|
|copy|hardware/bean/avr/cores/*|~/.platformio/packages/framework-arduinoavr/cores/|
|copy|hardware/bean/avr/libraries/*|~/.platformio/packages/framework-arduinoavr/libraries/__core__/bean/|
|copy|hardware/bean/avr/variants/*|~/.platformio/packages/framework-arduinoavr/variants/|
|append|hardware/bean/avr/boards.txt|~/.platformio/packages/framework-arduinoavr/boards.txt|

Next I added a board definition to the PlatformIO boards folders for AVR controllers in (~/.platformio/platforms/atmelavr/boards/). You can find a sample file in the previous repository but here is mine:

```
{
  "build": {
    "core": "bean", 
    "extra_flags": "-DARDUINO_ARCH_AVR", 
    "f_cpu": "8000000L", 
    "mcu": "atmega328p", 
    "variant": "bean"
  }, 
  "frameworks": [
    "arduino"
  ], 
  "name": "LightBlue Bean", 
  "upload": {
    "maximum_ram_size": 2048, 
    "maximum_size": 32256
  }, 
  "url": "https://punchthrough.com/bean", 
  "vendor": "Punch Through"
}
```

Now I just have to run these two commands to setup and compile a sketch for the Bean.

    
    pio init -b lightblue-bean
    pio run
    

## Hook the upload process

Now I can build a sketch for the Bean, but how can I upload it?

The Bean Loader CLI provides a command to upload sketches. If you use the Arduino IDE the upload button actually just build and copies the sketch binary to "~/.beansketches/bean/". This is the folder the list_compiled_sketches command of the CLI reads to output available firmwares. So if you have a firmware file there called "led.hex" you can upload it to your Bean with:

```
bean program_sketch led -n bean01
```

Fine. But the program_sketch command also lets you provide a path to the HEX file. So I can actually do from my PlatformIO project folder:

```
bean program_sketch .pioenvs/bean/firmware.hex -n bean01
```

And this process can be easily automated with a hook to the upload target:

```
#!/bin/python

import sys

from SCons.Script import DefaultEnvironment
env = DefaultEnvironment()

def before_upload(source, target, env):
    cmd = "bean program_sketch %s -n %s" % (source[0], env['UPLOAD_PORT'])
    env.Execute(cmd)
    sys.exit(0)
env.AddPreAction("upload", before_upload)
```

This script will add a pre-action hook to the "upload" target, it will gather the source file (the actual HEX file) and upload port as the bean name and will exit to prevent the default action to be run. I can wire this script to my project with a small modification to the platformio.ini file:

```
[env:bean]
platform = atmelavr
board = lightblue-bean
framework = arduino
extra_script = bean_upload.py
upload_port = bean01
```

So when I execute:

```
pio run -t upload
```
    
It will compile (if necessary) and upload the new sketch to my "bean01" Lightblue Bean. I can also specify the target in the command line by overriding the upload_port value:

```
pio run -t upload --upload-port bean02
```

## Ready, steady,...

So now that I can code my Lightblue Beans sketches with my favourite IDE (Atom) and upload the sketches from the command line it's time to put them to work...		

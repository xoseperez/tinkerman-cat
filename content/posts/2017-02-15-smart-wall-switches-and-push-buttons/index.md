---
author: Xose
comments: true
date: 2017-02-15 12:00:17+00:00
layout: post
slug: smart-wall-switches-and-push-buttons
title: "Smart wall switches and push buttons"
post_id: 1538
image: "images/IMG_20170206_214354x.jpg"
categories:
- Hacking
- Projects
tags:
- electrodragon
- espurna
- itead
- itead studio
- multi-way
- push button
- silicon labs f330
- sonoff
- sonoff dual
- wall switch
---

One might think that one of the typical uses for a smart wireless switch (like Sonoff devices) is to be **embedded behind a normal wall switch** so it becomes a "smart" wall switch. It may seem obvious but it's not that straight forward. There are several things that get in the middle.

  * Most (all?) the boards have momentary push buttons while wall switches are (normally) toggle switches
  * Most of the available boards in the market are SPST, even those with SPDT relays often only provide terminals for COM and NO, not NC. I only have one one-throw switch at home, all the others are one-way-two-throw and are being used as part of a [**multi-way switch**](https://en.wikipedia.org/wiki/Multiway_switching).

First problem can be easily solved in code. Instead of detecting one edge of the button signal (usually the rising edge since most push buttons are configured with pull-ups) you can detect both edges.

Second problem is harder since it depends on the hard-ware (ehem). But there are a few boards out there with SPDT relays like the [Itead 1CH board](/post/the-mysterious-ic/) I recently covered, the [e_Goto Wifi Relay Switch Module](http://s.click.aliexpress.com/e/bIA2nyR) [Aliexpress] or the [Your Cee ESP8266 5V WiFi Relay Module](http://s.click.aliexpress.com/e/vBEMFE6) [Aliexpress]. There are also two-relay boards you can use for a multiway switch by **syncing the relays** in opposition mode using [ESPurna firmware](https://github.com/xoseperez/espurna) for instance (one and only one relay on). The [Electrodragon ESP Relay](/post/esp8266-multi-relay-boards-sonoff-dual-electrodragon/) board is one such boards. Another one is the [**Sonoff Dual**](http://sonoff.itead.cc/en/products/sonoff/sonoff-dual).

{{< lazyimg "images/20161207_003729s-1200x800.jpg" "The Electrodragon ESP Relay board and Itead's Sonoff Dual" >}}

<!-- more -->

## Tricking the Dual

Recently a user of ESPurna asked me how to use the Sonoff Dual with a wall switch. No problem since you can change the firmware to support toggle twitches, right? Wrong. There is a problem with this device because there is a second microcontroller on board that drives the button (and the relays), a [Silicon Labs F330](https://www.silabs.com/Support%20Documents/TechnicalDocs/C8051F33x.pdf). You can control the relays from the ESP8266 sending the appropriate instructions to the F330 and it reports the button events via the same serial line, but only rising edges.

That means you can't have long click events for instance (since you don't know when the user pressed the button, only when she released it). But it also means it doesn't work for toggle switches (wall switches).

Well, it does not work easy, but you can make it work. The trick is to add a little additional circuitry to force the wall switch behave like a momentary push button. At least for those that work in multi-way switch configurations.

If your wall switch is a single-pole-double-throw (SPDT) device it has not 2, but **3 possible states**. The third one being when the pole is moving from one output to the other. Its floating. If we connect the pole to ground and the two outputs together to a NPN with a pull-up the wall switch will normally tie the NPN gate to ground and prevent current from flowing across it, except when the pole is moving, during that few milliseconds the output is pulled up by the resistor and the NPN allows the current to flow. You have a push button!

{{< lazyimg "images/WallSwitch2Pulse.png" "The schematic of the solution. BTN has a hardware pull-up." >}}

## It works!

It adds a bit of complexity but it's working great. I was afraid there was some debounce code that would remove the signal shape (when the pole moving from on contact to the other) but it is very sensitive. A slight push will make the pole lose contact and the relay to trigger, even if it does not travel all the way to the other contact. So you might actually want to add some hardware debounce to the solution.

{{< lazyimg "images/IMG_20170206_214431x.jpg" >}}The Dual has a header to connect BUTTON0 and BUTTON1. Any of those will work since they are directly managed by the F330. You also have GND and 3V3 (in a different header).

		

---
author: Xose
comments: true
date: 2018-01-29 23:19:24+00:00
layout: post
slug: arduino-mkr-wan-1300
title: "Arduino MKR WAN 1300"
post_id: 2242
image: "images/20180130_012920s-1200x800.jpg"
categories:
- Analysis
- Learning
tags:
- abp
- arduino
- lora
- lorawan
- mkr wan 1300
- mkrwan
- murata
- otaa
- platformio
- rak811
- rfm95
- rn2483
- samd21
- stm32
---

I've been testing quite a few **LoRaWan** nodes lately for [TheThingsNetwork.cat](http://thethingsnetwork.cat), some based on [HopeRF RFM95W](http://www.hoperf.com/rf_transceiver/lora/RFM95W.html) (over AVR, ESP8266, ESP32,...) others using [Microchip's RN2483](https://www.microchip.com/wwwproducts/en/RN2483) ([an old friend of mine](/post/low-power-in-lorawan-world-meet-the-rn2483/)). I have a [RAK811](http://www.rakwireless.com/en/module/lora/RAK811) waiting in the INBOX but the last one I've been playing with has been the new [Arduino MKRWAN 1300](https://store.arduino.cc/mkr-wan-1300) (so new there is no product page yet) and I liked it, quite a lot.

<!-- more -->

The device is one of the MKR series Arduino is pushing forward. So far the family includes the MKR ZERO (<del>did they name it after the RPi Zero?</del> actually, it's named after the Cortex-M0+ MCU, and it had that name "far before the RPi", as David Cuartielles has pointed out), the MKR 1000 with WiFi, the MKR FOX 1200 with SigFox support, the MKR WAN 1300 and the MKR GSM 1400.

They all sport a [SAMD21 Cortex-M0+](http://ww1.microchip.com/downloads/en/DeviceDoc/40001882A.pdf) [pdf, datasheet] 32bit low power ARM MCU clocked at 48MHz, 256Kb of flash memory (to store your code) and 32Kb of SRAM. That's quite an improvement from the Uno family (ATMega329 / ATMega32u4). It also has a bunch of digital IO, most of them PWM, several ADC of up to 12bits depth, hardware UART, SPI and I2C and even one DAC (!!).

{{< lazyimg "images/20180130_013123s.jpg" >}}

But the reason this is my first MKR dev board is its LoRaWan compatible module. A [Murata CMWX1ZZABZ](https://wireless.murata.com/eng/products/rf-modules-1/lpwa/type-abz.html) that includes a STM32 microcontroller with an AT firmware you can interface with from within your sketches.

{{< lazyimg "images/20180130_013102s.jpg" >}}

There is still little information about the board in the Internet. Sure you can find press notes but there is little hands-on stuff yet: the MKRWAN library repo and a ["Getting started" repo](https://github.com/gonzalocasas/arduino-mkr-wan-1300) by Gonzalo Casas from TTN Zurich. Gonzalo uses the Arduino IDE to explain the first steps with the board. But I'm going to write here about my experience using it from my favourite environment: PlatformIO.

**UPDATE**: Just a small update since I have been doing some research on the board and I found out there is no info about the LDO it uses. The schematic just states a generic "LDO2SOT89-3L". After trying to decode the label in the LDO package with a magnifier ("N8 7IT") and googling around I think the LDO is an [AP7215](https://www.diodes.com/assets/Datasheets/AP7215.pdf) [PDF, datasheet] by Diodes Incorporated. This is a low dropout voltage LDO (aprox 1:1 to Iout, i.e. 300mV at 300mA), with a maximum input voltage of 5.5V and 3.3V output at stable 600mA (max of 750mA), more than enough to power the SAMD21 and the Murata module (128mA max transmitting at full power). The LDO has also a pretty low Iq of 50uA. 

## Configuring PlatformIO for the MKR WAN 1300

Yes. The MKR WAN 1300 works just fine (almost, see below) with latest Arduino IDE versions. But I just happen to use not Arduino IDE. So I quickly headed to my favourite builder, PlatformIO, listed the supported boards and... no luck. The 1300 was not there.

There are quite a few SAMD21 boards supported, starting with Adafruit's M0 boards, some Arduino MKR and others. But not the MKR WAN 1300. Next I tried using MKR ZERO board assuming that since it is the minimum common denominator for all MKR boards I could get something out of it. Wrong.

As it turns out there are some small details as the device signature that prevent it from working with other boards. Albeit more obvious differences like available GPIOs in the headers.

{{< lazyimg "images/20180130_013208s.jpg" >}}

Long story short, I migrated the configuration from Arduino IDE to PlatformIO, comparing them with other MKR boards already defined. There are two packages that require an update and I have submitted pull-requests for that to PlatformIO. I don't know what's the policy they have about supporting Arduino boards. Somewhere I read something that made me think they have an automatic process, but as of today the packages are obviously outdated.

Anyway, the changes are in the AtmelSAM platform ([see PR here](https://github.com/platformio/platform-atmelsam/pull/25)) and ArduinoSAM framework ([PR here](https://github.com/platformio/platformio-pkg-framework-arduinosam/pull/5)). While the PRs are not merged you can install support for any SAMD board and then patch those packages in you .platformio folder.

```
$ pio boards | grep -i mkr
mkr1000USB            SAMD21G18A     48Mhz     256kB   32kB   Arduino MKR1000
mkrfox1200            SAMD21G18A     48Mhz     256kB   32kB   Arduino MKRFox1200
mkrwan1300            SAMD21G18A     48Mhz     256kB   32kB   Arduino MKRWAN1300
mkrzero               SAMD21G18A     48Mhz     256kB   32kB   Arduino MKRZero
```

Voilà

## The MKRWAN library

No that I can flash the MKR WAN 1300 using PlatformIO let's go to the LoRaWan part. The people from Arduino have released a library just for that. The Murata module in the device has a STM32 frontend that exposes an AT command interface you can use from the SAMD21 using the MKRWAN library.

Both the firmware for the STM32 ([mkrwan1300-fw](https://github.com/arduino/mkrwan1300-fw)) and the [MKRWAN](https://github.com/arduino-libraries/MKRWAN) library are available on GitHub. But you can also install the library using the PlatformIO library manager.

```
$ pio lib search mkrwan
Found 1 libraries:

MKRWAN
======
#ID: 2009
Support library for MKR WAN 1300

Keywords: communication
Compatible frameworks: Arduino
Compatible platforms: Atmel SAM
Authors: Arduino
```

Install and test. There is a full example called FirstConfiguration.ino that does it all. Shows the device EUI so you can register it in your TTN backend and allows you to join via OTAA or ABP and send a test message. Cool.

```
$ cd
$ mkdir -p workspace/mkrwan1300_test
$ cd workspace/mkrwan1300_test
$ pio init -b mkrwan1300
$ pio lib install mkrwan
$ cp .piolibdeps/MKRWAN_ID2009/examples/FirstConfiguration/FirstConfiguration.ino src/
$ pio run -t upload
$ pio device monitor -b 115200
Welcome to MKRWAN1300 first configuration sketch
Register to your favourite LoRa network and we are ready to go!
Your module version is: ARD-078 1.1.2
Your device EUI is: 0123456789abcdef
Are you connecting via OTAA (1) or ABP (2)?
1
Enter your APP EUI
0123456789012345
Enter your APP KEY
01234567890123456789012345678901
Message sent correctly!
```

Wow. That was amazing for a first test. I was very pleased to see that OTAA worked out of the box. But ABP did not.

After digging a bit on the problem with ABP I hit a dead end when I couldn't find the documentation for the STM32 firmware. So I decided to open an issue on the library repository on GitHub last Friday and on Monday it was solved. Not bad! As a side note, the STM32 firmware repository was not public yet (that's why I couldn't find any docs) but that has changed this Monday 29th January.

## Updating the firmware in the STM32 inside the Murata module

Once a solution was proposed I had to flash the firmware on the STM32 and update the MKRWAN library. Fortunately the developer of the library (Martino Facchin, thank you very much) had added a firmware upgrade sketch in the examples folder that includes the latest firmware image in the fw.h file. So I removed the old library, cloned the new one and run the upgrader example:

```
$ pio lib uninstall mkrwan
$ git clone https://github.com/arduino-libraries/MKRWAN lib/MKRWAN
$ rm -rf src
$ cp -r lib/MKRWAN/examples/MKRWANFWUpdate_standalone src
$ pio run -t upload
$ pio device monitor -b 115200
```

You will now see the progress of the upgrade and a final "Flashing ok :) ARD-078 1.1.3" message. Good.

## And finally: testing it!

Now back to the FirstConfiguration example, flash, open a connection to the device and...

```
$ rm -rf src
$ cp -r lib/MKRWAN/examples/FirstConfiguration src
$ pio run -t upload
$ pio device monitor -b 115200
Welcome to MKRWAN1300 first configuration sketch
Register to your favourite LoRa network and we are ready to go!
Your module version is: ARD-078 1.1.3
Your device EUI is: 0123456789abcdef
Are you connecting via OTAA (1) or ABP (2)?
2
Enter your Device Address
22334455
Enter your NWS KEY
01234567890123456789012345678901
Enter your APP SKEY
01234567890123456789012345678901
Error sending message
```

Now what? Well, looking at the TTN console I noticed the message actually went through... Looks like it timed out before receiving the ACK.

{{< lazyimg "images/20180130_012842s.jpg" >}}

There are some things to improve yet but the overall initial experience is very good. They are actively working on fixing problems and that even better. Now it's time to do some real life tests to see if this is a dev board of choice.		

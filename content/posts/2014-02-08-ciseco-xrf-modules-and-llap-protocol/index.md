---
author: Xose
comments: true
date: 2014-02-08 15:19:57+00:00
layout: post
slug: ciseco-xrf-modules-and-llap-protocol
title: "Ciseco XRF modules & LLAP Protocol"
post_id: 563
image: "images/xrf.jpg"
categories:
- Code
- Learning
- Projects
tags:
- arduino
- llap
- protocol
- weather station
- xbee
- xrf
---

In my last post about [counting events with Arduino and PCF8583](http://tinkerman.eldiariblau.net/counting-events-with-arduino-and-pcf8583/) I talked about this "yet another weather station" project I was working on last summer. The station was deployed in the garden of a cute apartment we rented in an old "[masia](http://en.wikipedia.org/wiki/Masia)" near Olot, 100 km north of Barcelona. It is in the mountainside, surrounded by woods and 10 minutes walking from the near town. It has a beautiful garden with plenty of space. We spent there most of the summer but now we are still driving there on the weekends. It's colder, sometimes freezing, and when it rains, well, it does rain. Off course it was the perfect excuse to build another weather station.

One decision I had to take when designing the new weather station was how to send data from the nice housing I built for it in the garden to my home server in Barcelona. I needed some kind of internet connection in the house but that's something I will talk about in another post. I could have used a RN-XV WIFI module like the one I'm using for the [rentalito](http://tinkerman.eldiariblau.net/the-rentalito/) but it's expensive and I really wanted something simpler to use.

I had already a couple of [Ciseco's XRF](https://web.archive.org/web/20160624000448/https://www.wirelessthings.net/xrf-wireless-rf-radio-uart-serial-data-module-xbee-shaped) (archive.org) radios and decided to give them a try (they are now selling version 2 of theses radios, I have v1.5 modules). These modules provide an easy way to create a wireless transparent RF serial connection between two nodes, no need to configure anything. They have a better range than Bluetooth, WIFI or Zigbee, since they use a longer wavelength to operate (868 to 915 MHz). Off course they can do a lot more than that. They are based on Texas Instruments’ CC1110, a low-power System-on-Chip and you can write and load your own firmware on them. Ciseco provides a series of closed-source firmwares (they call them "[personalities](http://openmicros.org/index.php/articles/87-llap-lightweight-local-automation-protocol/llap-devices-commands-and-instructions)") for these radios, focused on different sensor inputs. You can find more information in the [openmicros.org wiki](http://openmicros.org/index.php/articles/247-xrf-document-index), there is enough to spend a couple of hours reading but I have to say the wiki is kind of a mess, although they have improved it a lot in the last year or so.

{{< lazyimg "images/xrf.jpg" "Ciseco XRF wireless RF radio v1.5" >}}

Anyway, out-of-the-box these modules are a transparent RF link and their footprint is compatible with XBee modules, so you can use them with your XBee Explorers, Arduino FIOs,... (well played Ciseco). Like in my previous [weather station](http://tinkerman.eldiariblau.net/weather-station/) I decided to use an Arduino FIO as a controller (DC-IN, LiPo battery backed, XBee socket,...) so I just stacked one of these modules on it. Inside the house I prepared the "gateway": an Arduino Leonardo, with an Ethernet shield and a Wireless Shield with another XRF module. The Ethernet shield connected the Arduino to a [TP-LINK WR703N WIFI router](http://wiki.openwrt.org/toh/tp-link/tl-wr703n) loaded with openWRT with a 3G USB stick. The small router provides internet connection to the Arduino and to any other device inside the house via WIFI. The WR703N is a really awesome, small and hackable piece of hardware. But as I have already said, you will have to wait for the whole picture of the connection between the weather station and my home server, I want to focus on the radios and the protocol now.

Now that we have the hardware it's time to think about the message. Ciseco promotes the use a a light-weigth protocol named, well, Lightweight Local Automation Protocol, or LLAP. You can read all about it in the [LLAP Reference Guide](http://openmicros.org/index.php/articles/85-llap-lightweight-local-automation-protocol/297-llap-reference) in the openmicros.org wiki. The protocol defines two node types: controller and device; a message format formed by a start byte ('a'), device identification (2 bytes) and a payload (9 bytes); and a communications protocol (address allocation, request/response pairs,...). The different "personalities" provided by Ciseco use this message protocol to report sensor values and they can even be configured remotely this way. But Ciseco also provides an [Arduino LLAPSerial library](https://github.com/CisecoPlc/LLAPSerial) so anyone can easily create LLAP devices using XRF radios or other products from the company that integrate a MUC.

Ciseco LLAP library for Arduino is OK, and it works, but it looks like a draft, something you can use to build upon it. So I decided to do just that. You can checkout my version of the [LLAPSerial library for Arduino](https://bitbucket.org/xoseperez/llapserial) from Bitbucket. Initially I did a fork of Ciseco version but finally I decided to break the dependency with it because some features I added made it incompatible with the original one, although the protocol is 100% backwards compatible. The differences are summarized in the README file but here you have a quick-view:

  * Removed power management code (this library focuses on LLAP protocol and messaging)
  * Added support to use different Hardware and Software serial ports
  * Provided a unique overloaded sendMessage method that supports sending char/int/float messages
  * Provided a way to broadcast messages (see below) using special device ID '..'
  * Defined the "coordinator" node, which will always process all messages, regardless the destination
  * Disallow CHDEVID to coordinator nodes
  * Major renaming and refactoring (sorry)
  * Added doc comments
  * Address negotiation and persistence **NEW!!**

This sample code shows the use of the library to report data from a DHT22 temperature and humidity sensor using LLAP.

```
#include <LLAPSerial.h>
#include <DHT22.h>

#define DEVICE_ID "DI"
#define DHT_PIN 2

// DHT22 connections:
// Connect pin 1 (on the left) of the sensor to 3.3V
// Connect pin 2 of the sensor to whatever your DHT_PIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

DHT22 dht(DHT_PIN);
LLAPSerial LLAP(Serial);

void setup() {

   // This should match your radio baud rate
   Serial.begin(115200);

   // This device has a static ID
   LLAP.begin(DEVICE_ID);

}

void loop() {

   static unsigned long lastTime = millis();
   if (millis() - lastTime >= 10000) {

      lastTime = millis();

      DHT22_ERROR_t errorCode = dht.readData();
      if (errorCode == DHT_ERROR_NONE) {
         float t = dht.getTemperatureC();
         float h = dht.getHumidity();
         LLAP.sendMessage("HUM", h, 1);
         LLAP.sendMessage("TMP", t, 1);
      } else {
         LLAP.sendMessage("ERROR", (int) errorCode);
      }

   }

}

```

<del>There are some things missing from the library, being the main one the address allocation feature the protocol describes (also missing from Ciseco's implementation). I will try to add it soon. In the meantime feel free to use any of the two libraries and enjoy the simplicity of LLAP.</del> The last version of the library supports address negotiation between the nodes and the coordinator following the guidelines at [LLAP Reference Guide](http://openmicros.org/index.php/articles/85-llap-lightweight-local-automation-protocol/297-llap-reference), including address persistence, so a node will keep its address after a reboot.		

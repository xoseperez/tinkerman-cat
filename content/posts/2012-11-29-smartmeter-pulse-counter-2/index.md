---
author: Xose
comments: true
date: 2012-11-29 00:06:25+00:00
layout: post
slug: smartmeter-pulse-counter-2
title: "Smartmeter pulse counter (2)"
post_id: 74
image: "images/schematic1.png"
categories:
- Projects
tags:
- arduino
- low power
- photocell
- rc filter
- xbee
---

This week I have had some spare time - well, I should say I've borrowed some time from my sleep - to work on the smart-meter pulse counter setup.

In my last post about this I analized the [signal from my photocell sensor](http://tinkerman.eldiariblau.net/smartmeter-pulse-counter-1/). My conclusion was that the signal was clean and neat, even before throwing in a schmitt trigger to make it more "digital". I have found out that it is also very dependent on the environmental light so I good isolation is a must for the sensor.

I had already prepared a sensor probe with a 3.5mm audio jack on one end and the photocell on the other. But the photocell is aligned with the cable, which is quite rigid and I am concerned about how to attach it to the smart meter case. For now I will leave it like that and do some real scenario tests. Since it is an external sensor I can change it whenever I want.

{{< lazyimg "images/sensor.jpg" >}}

Finally I have also added an RC filter to the sensor input. The previous data suggests it is not necessary but I needed to do some tests with a faked signal source (a mini button) and it won't harm.

Aside from these things I've done some very important steps forward. I have a final schema for the board.

{{< lazyimg "images/schematic1.png" >}}

There are some points worth noticing:

	
  * I've added a ceramic capacitor across VDD and GND as the Xbee documentation recommends. I didn't have the 1uF and 8.2pF ones but I have seen some boards with the *standard* 100nF.

	
  * There is a voltage divider for the battery monitoring on the top right corner.

	
  * On the bottom you can see the voltage divider with the photocell in series with the RC filter

	
  * I'm wiring DIO9 *and* DTR to the Arduino, this way I can switch from Xbee-driven to Arduino-driven from code (more on this later).

	
  * <del>Finally I decided to wire DOUT to the Arduino (I'm using SoftwareSerial, so RX and TX pins are digital 5 and 6 respectively)</del> Not any more. I am using hardware serial on the final version...

This setup allows me to choose between two very different approaches: I can make Arduino drive the sleep cycle by changing DTR pin (Xbee in mode SM1 or "pin sleep") or I can make the Xbee control the sleep cycle (Xbee in mode SM4/5 or "cycle sleep") triggering an Arduino interrupt  when it awakes.

The first option lets me send a report every N pulses. In my case 4 pulses equal 1Wh. Lets say I want to report chunks of 5Wh, that's 20 pulses. That means a report every 4.5 to 90 seconds depending on the current load (200W to 4kW range), being 60 seconds (300W) the average.

With the second option I can report average power consumption every minute. The Arduino counts the pulses and when the Xbee awakes it asks the Arduino (i.e. triggers an interrupt) to calculate and report the average power consumption over the last minute. For instance, if the pulse count is 28, that's 7Wh over the last minute or an average of 420W.

I prefer the later because the reports are evenly spaced which means they are somewhat easier to graph and the graph is more meaningful: if you graph the power in the Y axis and time in the X axis the area below the power line is the energy (watts * hour). With the former the data should be post-processed to get some useful information like average Wh per day.

I've already done some tests but, so far, I have not been able to program the Xbee to follow an accurate sleep cycle. To configure the sleep period you can use SP and SN, the first one represents the time in tens of milliseconds, the second is a multiplier. So with SP=0x03E8 (1000 in hex) and SN=0x06 that's 60 seconds. The problem is that the awake period has it's own counter, ST, which is reset "each time serial or RF data is received". That introduces a random time in the calculation which translates into a slight shift in the reports.

Option B would be to use some RTC to awake the Xbee with an alarm trigger. But I would rather avoid adding more hardware to the sensor.

Well, still a lot of things to figure out... more to follow :)

		

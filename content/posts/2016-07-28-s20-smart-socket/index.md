---
author: Xose
comments: true
date: 2016-07-28 22:41:10+00:00
layout: post
slug: s20-smart-socket
title: "S20 Smart Socket"
post_id: 810
image: "images/IMG_20160728_230609x.jpg"
categories:
- Analysis
tags:
- blynk
- broadlink
- esp8266
- espurna
- ewelink
- free source
- ifttt
- interoperatibility
- itead
- ldr
- mqtt
- node-red
- open api
- orvibo
- pir
- s20
- sonoff
- stg15
---

Since I discovered the [Sonoff](https://www.itead.cc/sonoff-wifi-wireless-switch.html) I've been thinking about embedding it inside a switch. I started looking for old power meters, timers,... I had at home but the Sonoff is a bit too long. Why didn't they design a square board? I event bought a bulky [Kemo STG15](https://www.kemo-electronic.de/en/Components/Cases/Closed/STG15-Connector-case-with-socket.php) case with socket.

Next I decided to design my own board. It is meant to be the "official" hardware for the ESPurna project so it's called ESPurna too. It's opensource hardware and available at the [ESPurna project repository](https://github.com/xoseperez/espurna) at Bitbucket. I have some boards already for the first iteration (version 0.1). They are mostly OK but I'm already working on a revision.

But then ITead's released their [S20 Smart Socket](https://www.itead.cc/smart-socket.html). It's the Sonoff in a wall socket enclosure. Almost 100% what I wanted. And at 11.70€ it's hard to beat. There are other wifi smart sockets available, mainly Orvibo and BroadLink (an SP2 Centros should be arriving home anyday now) but ITead's is cheaper and you can easily re-flash it. Just solder a 4 pins header, connect it to your FTDI programmer, hold the S20 button, connect the programmer to your computer and flash. Done.

OK, not so fast. Why would I do that? Why would I change the stock firmware?

The answer for me is a mixed up of philosophy and practicity. But you are right. Let's go step by step.

<!-- more -->

## Hardware

First let's take a look at the device. The wall socket is nice looking and pretty small (at least compared to the STG15!!). In the socket you can read "16 / 250 ~" which I don't know what it means since the relay inside is the same the Sonoff TH has and it's rated at 10A and the label in the back says 2kW max, but maybe there will be a 16A version like the new [Sonoff TH 16A](https://www.itead.cc/sonoff-th.html). The ground terminals (connected from input to output) are not tightly fit and they make a metallic sound when you shake the device (OK, why should you shake it?). The ON/OFF button symbol is upside down (<del>ehem</del> looks like the designer thought about it with the button over the plug, which makes perfect sense since most of the time the cable hangs down). But the overall feeling is really good.

To take a look inside the enclosure you have to remove a single screw on the back of the device, but there is a "tore invalid" label covering it (hey, blame Google translate), so don't do it.

{{< lazyimg "images/IMG_20160728_161811x.jpg" "S20 front view, yes, the ON/OFF button icon is upside down..." >}}

{{< lazyimg "images/IMG_20160728_163848x.jpg" "S20 back with the 'tore invalid' label" >}}

But if you do, this is what you will see:

{{< lazyimg "images/IMG_20160728_225813x.jpg" "A detail of the 'upside down' button from the inside. It looks like a communication problem between the guy that was working on the enclosure and the guy that designed the button itself..." >}}

{{< lazyimg "images/20160722_195356x.jpg" "S20 guts. The ground clip has been removed. You can see the programming header: GND, RX, TX and VCC, everything a hacker needs (almost)." >}}

{{< lazyimg "images/20160722_195814x.jpg" "On the back of the board you can see the short live tracks, the ESP8266EX, SPI flash memory and the AMS1117-3V3." >}}

The guts of the [S20](https://www.itead.cc/smart-socket.html) are very much like those of the Sonoff albeit with a different layout. Connections to mains are in the upper side, that provides a better isolation between the safe part and the unsafe part, whilst on the Sonoff you have hot tracks running all along the board. Like in the Sonoff it has live lines duplicated on both sides of the board and an extra of tin on the bottom. There are air gaps between the mains lines and the low voltage side, but they had to leave a bridge, probably to provide mechanical strength to the board.

On the bottom of the board you have several components including the ESP8266EX, a [Winbond 25Q80DVSIG](https://www.winbond.com/resource-files/w25q80dv_revf_02112015.pdf) 8Mbit SPI Flash memory and an ASM1117-3V3 that provides regulated 3.3 volts to the ESP8266 and the flash chips from the 5V output of the AC/DC power supply. Whilst 1Mbyte on flash ROM is enough for most applications, even with OTA functionality (check my [ESPurna firmware](https://github.com/xoseperez/espurna)) some might find it a bit too short. If that's your case there are cheap and easy ways to [upgrade the memory chip](http://www.esp8266.com/viewtopic.php?f=45&t=6967) to a 4Mbytes one.

There is a button attached to GPIO0 and two LEDs, a green one connected to GPIO13 like in the Sonoff and a blue one to the GPIO12, like the relay, so whenever the relay is closed the LED will lit blue. Close by the blue LED there is an unpopulated header perfectly labelled with VCC, RX, TX and GND lines. That's everything you need to flash the ESP8266 onboard custom firmware.

## Software

ITead's home automation products are managed through the **eWeLink app** for [Android](https://play.google.com/store/apps/details?id=com.coolkit) and [iPhone](https://itunes.apple.com/us/app/ewelink/id1035163158?mt=8). The goal of the app is to become a hub for different wifi home automation devices: switches, lamps, power strips, fans,... and the operation should be straight forward: set the device in pairing mode (AP mode), configure wifi credentials and control it.

I swear I have tried hard to use it. Several times. Even accepting weird permissions I would have not accepted to any other app (why should it ask for my phone number or my location?). But I never ever managed to make it work. I have not been able to pass the pairing step. Not even after giving the app permissions over my personal life. Impossible. I quitted. If anyone has a clue on how to do it, please let me know. Permissions and connection problems are common between the comments on Android app market. No one has commented yet on iTunes. Weird.

{{< lazyimg "images/Screenshot_20160728-230957x.png" "This is the furthest I got" >}}

But even thou the app had worked fine I wouldn't use it. I'm not saying you shouldn't use it. My case might not be yours. Why would I flash my own firmware? Basically to **really own the device**.

We are in the very early days of the IoT and we are not even sure of what it means. Companies are trying to capitalize the concept providing their users all-in-one solutions including the device, the app and the cloud (whatever that is). But why should anyone know when you set your livingroom lights in romantic mode? And what happens if the company shuts off? [Greenwave by TCP](http://www.connectedbytcp.co.uk/faqs/) or [Revolv](https://web.archive.org/web/20160301002424/http://revolv.com/) cases are well know. Google has a reputed history of closed services (not IoT related, true).

And what about **interoperatibility**? You can easily end up having to use an app to switch your reading lamp and a different one for your ceiling light! Even those that provide open cloud solutions have weak days: Phillips recently released a trial balloon about [locking out 3rd party hardware from Hue brigde](http://hackaday.com/2015/12/15/philips-says-no-internet-of-things-for-you/). Elliot Williams at hackday put it simply: "The 900-pound gorilla in the corner of the Internet of Things (IoT) hype that everyone is trying to ignore is interoperability".

That's why i would always prefer **open APIs and protocols** over proprietary ones. I'm not even talking about open/free source software or hardware but protocols. **You don't own a device and the information it manages if you cannot talk to it directly, without middlemen.**

The great thing about the Sonoff or the [S20 Smart Socket](https://www.itead.cc/smart-socket.html) is that you can easily flash your own firmware on them. So now I can control it with through my MQTT local broker, add schedules, behaviours,... through my local Node-RED instance or rely on 3rd party cloud services like Blynk or IFTTT if I want to, because if they ever shut off, it's no big deal.

## Conclusions

From the end-user point of view the device is a great deal: cheap, nice, wifi-enabled. But the mobile app is a disaster and I would expect people returning their devices because of this.

From the hacker point of view it's a even greater deal. You can easily flash custom firmware and make it talk to your local services. It could have more flash memory. It could have more GPIOs exposed in the header to add sensors like a simple LDR to switch ON/OFF depending on the ambient light or a PIR to detect that someone's near. Or maybe it could have a jack out like the new Sonoff TH 10A/16A.

I will recommend the [S20 Smart Socket](https://www.itead.cc/smart-socket.html) to anyone with the skills to customize it. For less than 12€ it's an amazing device you can own.		

---
author: Xose
comments: true
date: 2016-11-12 15:44:10+00:00
layout: post
slug: the-sonoff-pow
title: "The Sonoff POW"
post_id: 1320
image: "images/20161105_112709x.jpg"
categories:
- Analysis
- Code
tags:
- esp8266
- espurna
- hlw8012
- itead
- relay
- sonoff
- sonoff pow
---

Some months ago I wrote about a hack I did to one of my [Sonoff](http://sonoff.itead.cc/en/) devices to be able to use a simple current sensor to monitor my washer machine process and alert me whenever [my laundry was done](/post/your-laundry-is-done/).

A few weeks ago [Itead Studio](https://www.itead.cc/) released two new models for their Sonoff line, the POW and the DUAL. And the POW is Itead's answer to my hack. I'm not saying they copied me, just that the Sonoff POW makes my hack utterly unnecessary. Do you want to remotely monitor your devices energy consumption? [Buy a POW](https://www.itead.cc/sonoff-pow.html).

<!-- more -->

## What's new?

Actually, the [Sonoff POW](https://www.itead.cc/sonoff-pow.html) layout shows some very significant differences to that of the [Sonoff TH16](/post/sonoff-th10-th16-sensors-displays-actuators/), for instance.

{{< lazyimg "images/20161105_114214x.jpg" "The Sonoff TH16 (left) and the Sonoff POW (right), spot the differences..." >}}

{{< lazyimg "images/20161105_113510x.jpg" "Again, the Sonoff TH16 (left) and the Sonoff POW (right)" >}}

Obviously the POW has some circuitry for the power monitoring. The main component of this is the **HLW8012** (the SOP8 packaged IC in the picture above). I wrote about the [HLW8012](/post/hlw8012-ic-new-sonoff-pow/) a few days ago, so I will not talk a lot about it here.  The schema of the POW around the HLW8012 is almost the same as in the datasheet. You can see the** 1 milliOhm manganese-copper resistor** in the center of the board. The IC reads the difference in voltage at the edges of the resistor to calculate the current flowing through. Also, the 5 0603 470kOhm resistors left of the manganese one are the upstream side of the voltage divider that feeds the voltage monitor pin of the HLW8012.

{{< lazyimg "images/20161108_000330x.jpg" "Detail of the HLW8012" >}}

A lot of components have been moved to new positions or removed. The [HLW8012](http://s.click.aliexpress.com/e/Fq7URjq) [Aliexpress] sits exactly where the header for the RF module is in the TH16. So no chance for an RF+POW version. The diode bridge has been moved closer to the edge of the board and the creepage slots run deeper into the DC side of the board.  **The relay** is almost exactly the same as in the TH16, a [Honfa HF152F-005-1HST](http://www.hongfa.com/pro/pdf/HF152F_en.pdf), also **rated 16A@250VAC** or 20A@125VAC. And the programming header sits in the same place.

{{< lazyimg "images/20161105_113551x.jpg" "The relay is rated 16A@250VAC" >}}

{{< lazyimg "images/20161105_114543x.jpg" "Detail of the AC/DC components" >}}

Like the TH16 it has two LEDs, a red attached to GPIO12 like the relay (so it will be on whenever the realy is closed) and a green one on GPIO15. The button is, of course, tied to GPIO0 so you can use it to enter flash mode on boot.

But the most important difference from my point of view is that the **Sonoff POW lacks the sensor interface** I talked about in [my post about the Sonoff TH10 and TH16](/post/sonoff-th10-th16-sensors-displays-actuators/). This is actually a pity. It's like if Itead was teasing us with different options but forcing us to choose between a nice interface for external sensors or the power monitor feature. I will even add a third option to the list and it will make my perfect device, and hopefully next Itead release:

  * Power monitoring
  * External sensors
  * Socket enclosure (like the S20)

If they can sell that for under 15€ it's a winner.

Quality of the new Sonoff line (TH10, TH16, POW and DUAL) is pretty good and that's why they recently got the **CE mark** from the EU. The device looks solid, albeit a bit to big. The enclosure is the same as in the TH10 and 16. I still love how the button peeks out of the box. It almost looks like part of the enclosure.

## ESPurned!

You can use the POW with the official eWeLink app. But if you have read me before you might already know I'm not going that way. Instead I soldered a 4 pin header and flashed my own firmware to the device. For the last days I've been mostly playing with the HLW8012. Check my [post about the HLW8012](/post/hlw8012-ic-new-sonoff-pow/) a few days ago for a deeper look into it and a library for ESP8266 and Arduino to use it.

Today I've been updating my ESPurna firmware with the HLW8012 library to support the [Sonoff POW](http://sonoff.itead.cc/en/products/sonoff/sonoff-pow). It's still under development but you can give it a try.

> The ESPurna firmware is released as **free open software **and can be checked out at my [**Espurna repository**](https://github.com/xoseperez/espurna) on GitHub.

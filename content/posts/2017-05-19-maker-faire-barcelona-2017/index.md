---
author: Xose
comments: true
date: 2017-05-19 22:13:30+00:00
layout: post
slug: maker-faire-barcelona-2017
title: "Maker Faire Barcelona 2017"
post_id: 1799
image: "images/maker_faire_barcelona_badge_logo_catalan.png"
categories:
- Projects
tags:
- barcelona
- maker faire
---

I'm having some busy weeks again and the blog is suffering from my absence :) I had a bunch of comments to accept (please remember all comments are being moderated). And even more projects pending (including a new version of the ESPurna firmware). But I just wanted to let you know that I will be at the Barcelona Maker Faire next June 17 & 18.

Hope to see you there, it will be fun for sure!		

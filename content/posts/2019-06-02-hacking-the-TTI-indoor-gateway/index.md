---
title: "Hacking the TTI Indoor Gateway"
date: 2019-06-02T22:08:00+02:00
post_id: 20190602
author: Xose
comments: true
layout: post
image: "images/20190517_175109_HDR.jpg"
description: "The Indoor Gateway by The Things Industries is a small cheap multichannel gateway meant to improve network capilarity for SOHO or small industry. It's also a great device to hack around :)"
categories:
- Analysis
- Hacking
tags:
- ttn
- the things network
- ttncat
- tti
- rs-online
- the things industries
- lora
- lorawan
- kickstarter
- gateway
- pci express
- mean well
- rak833
- ipex
- cp2102n
- 3d printing
- schuko
- antenna
- dipole
- orientation
- polarization
- sma
- mqtt
- rssi
- stanislav palo
- 5/8 wave
- esp2866
- uart
- debug
- basic station
- semtech
- tls
- ssl
- centralized update and configuration management
- cups
- lorawan network server
- lns
- muxs
- infos
- listen before talk
- lbt
- tabs
- tracknet
---

[Update 2021-08-11] Added information on how to enable the CP2102N on the TTIG thanks to [@_pub_feuerrot](https://twitter.com/_pub_feuerrot).

Four years ago [The Things Network](http://thethingsnetwork.org) (TTN) kind of revolutionized the IoT world with their effort to reduce hardware and software costs to implement a commons LoRaWAN network. Their bet was backed on [Kickstarter](https://www.kickstarter.com/projects/419277966/the-things-network) by almost a thousand people with almost 300k€ to help create a low-cost LRaWan gateway (and a platform and end devices too).

The gateway took it's time to become a reality (kickstarter-effect) but by the time it was released the project had already impacted other players in the industry and low cost DIY gateways were a reality. We might complain it was not the fastest delivery, but they actually accomplished the original goal. Now there are more than 73000 people in 138 countries involved in their local communities.

Then the same people behind the The Things Network project founded [The Things Industries](http://www.thethingsindustries.com) (TTI), a company devoted to provide a high-availability LoRaWan network. Some communities are paying careful atention to this movement and while the governance model settles down TTI has started to deliver new products to fullfil specific requirements.

One of these products is the [TTI Indoor Gateway](https://www.thethingsnetwork.org/docs/gateways/thethingsindoor/), a low-cost multichannel indoor LoRaWAN gateway meant to provide capilarity (also known as last-mile coverage) for SOHO and small industry environments. For makers this is also a great product due to it's incredible price tag: slightly above 70€!!

The device was first introduced last February at the The Things Industries Conference and given away to all attendants. The remaining stock became available in RS but they ran out of stock in a few hours. Only a few of us got the chance to purchase one. Currently it is back-ordered and estimated delivery time is by June 17, just a few weeks from now.

<!-- more -->

## What's inside?

### Opening it

The case is pretty simple to take appart except for the star screws which are not "standard" but we have already seen them in many other devices. If you have one of those screwdrivers with multiples bits you probably have a small star bit too. I used the "Torx T4" from my iFixit kit. Once removed gently pull the two halves of the case. There are 2 hooks on each side.

{{< lazyimg "images/20190516_193916x.jpg" "Top lid removed, you can see the 'setup' button and the status LED" >}}

{{< lazyimg "images/20190516_193926x.jpg" "Bottom lid removed, reset button and USB-C charger" >}}

{{< lazyimg "images/20190514_130013_HDRx.jpg" "The two halves appart, notice the 4 plastic hooks" >}}

We can see there are two different boards inside. One of them is responsible for the AC interface, using an [IRM-10-5 5V 2A transformer by Mean Well](https://www.mouser.es/datasheet/2/260/r1937_3-1109586.pdf) [PDF, datasheet]. The connection to the other board is done using a simple pair of cables with a JST PH-2pin interface. 

{{< lazyimg "images/20190514_130019_HDRx.jpg" "One of the boards basically houses the AC/DC transformer" >}}

On one side of the second board we can see the LoRaWan concentrator with a PCI Express interface, similar to what we have seen in the RAK 833 or the Microchip concentrator inside the original The Things Network Gateway. But that does not mean they are cross-compatible.

{{< lazyimg "images/20190514_130025_HDRx.jpg" "LoRaWAN concentrator with a PCI Express form factor, there is a termal sink in between the concentrator and the main board" >}}

On the opposite side of the board we have two RF shields with identical labeling (23K-400-0666R V00). They are protecting the main microcontroller and WiFi interface. According to some other reviews, the shields are also covering a I2C expander and (of course) a Windbond flash.

{{< lazyimg "images/20190514_130440_HDRx.jpg" "The microcontroller (an ESP8266) and several other components are RF shielded. Notice the WiFi PCB antenna and also a iPEX-like connector, maybe for an external WiFi antenna" >}}

An interesting fact is that the concentrator and the antenna are connected using a short iPEX cable. That makes it very easy to add an external antenna as we will see later.

{{< lazyimg "images/20190530_151800x.jpg" "The 868MHz internal antenna is a dipole, please notice orientation is very important" >}}

We can also spot a CP2102N USB2UART bridge connected to the USB-C connector but, apparently, disabled. Also a unkown chip with a small hole (a sensor?) labelled "P310 7J" and a UART interface for the ESP8266.

{{< lazyimg "images/20190530_151358x.jpg" "A CP2102N USB2UART chip. I'm not sure what is it for?" >}}

{{< lazyimg "images/20190530_151410x.jpg" "What is this? Anyone? P310 7J" >}}

{{< lazyimg "images/20190530_151341x.jpg" "The UART interface for the ESP8266, will go back to his later" >}}

## DIY Schuko plug for the TTI Indoor Gateway

The gateway came home is a loosy cardboard box with a short USB-C cable and nothing else... Not great as a packaging. In particular there was no power adapter to connect it to a wall plug which was kind of deceiving. Of course the community took a step forward and there are a number of DIY solutions to solve this:

* [The Things indoor gateway EU power plug](https://www.thingiverse.com/thing:3556358) by Rdfo, member of the TTN community in Zaragoza.
* [Gateway Stand Indoor The Things Industries](https://www.thingiverse.com/thing:3542579) by LittleJoeMuc
* [TTI Indoor Gateway Phone Charger Adapter](https://www.tinkercad.com/things/esUL49osFBZ) by CRIF Lab and the TTN community in Madrid.

There are probably more options available. These are only the ones that I could find or I was suggested. I decided to print the schuko connector by Rdfo. The print went fine but the hooks broke right away. I'm sure the problem was that my printer is not perfectly calibrated. I still need a bunch of hours of 3D printing to gain some expertise here. I ended up using the iron to insert the plugs and screws and adding a metal hook to keep the plugs in contact to the metallic spring in the gateway. I bit hack-ish but it works!

{{< lazyimg "images/20190517_113343x.jpg" "The noisy excitation of a new 3D printed piece" >}}

{{< lazyimg "images/20190517_174959_HDRx.jpg" "The final result is (almost) perfect..." >}}

{{< lazyimg "images/20190517_175010_HDRx.jpg" "..if you don't turn it around. Well, it works" >}}

{{< lazyimg "images/20190517_175059_HDRx.jpg" "And looks great when plugged into the gateway" >}}

{{< lazyimg "images/20190517_175109_HDR.jpg" >}}

## Adding an external antenna, is it worth it?

As I already said the connection between the radio transceiver and the antenna is done using a short iPEX cable. This is surprising and convenient at the same time. Whatever the reason for this is, truth is that adding an external antenna is very simple, just drill a hole for a SMA connector and done. I've seen people drilling the hole in the top grey lid. I found it easier to do it in the front half.

{{< lazyimg "images/20190516_193615_HDRx.jpg" >}}

{{< lazyimg "images/20190516_193641_HDRx.jpg" >}}

{{< lazyimg "images/20190517_175142x.jpg" "Plug and play gateway!" >}}

As you can see it's a very easy hack. But, is it worth it? To test it I grabbed several RSSI values using the internal antenna and with different external ones. I also tested it with the internal antenna and two different orientations: vertical (like when plugged on a wall plug) and horizontal (laying flat on a desktop). To get the data I used a small bash command to subscribe to the MQTT topic of the end device sending an uplink every 20 seconds and filtering only the metadata from the gateway I was interested in monitoring. Here you have the command:

```
$ export APP=my-ttn-application
$ export PASS=my-ttn-credentials
$ export GWID=my-gateway-eui
$ mosquitto_sub -h eu.thethings.network -t "$APP/devices/+/up" -u $APP -P $PASS | jq -c ".metadata.gateways[] | select (.gtw_id == \"$GWID\") | .rssi "
-19
-17
-19
-18
-19
-19
-18
-18
-19
```

The results were somewhat unexpected but very interesting. As you can see the internal antenna performance is slighlty better than a regular "cheap" 868MHz external antenna (rated 2dBi, i.e. a dipole antenna). So it looks like it's not worth the time to add an external antenna. But there are two things to note:

* The internal antenna is very sensitive to orientation, **always place the gateway in vertical orientation**. This is only possible when using a wall plug or any of the three 3D printed addons above since the USB-C connector is in the bottom lid. Or you can use an external antenna too.
* Adding and external SMA connector allows you to add any type of antenna. In particular I tested a 5/8 wave antenna by [Stanislav Palo](https://rover.ebay.com/rover/1/1185-53479-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338044841&mpre=https%3A%2F%2Fwww.ebay.es%2Fusr%2Fstanislavpalo130) [ebay] and the performance boost is amazing, a +7dB gain over the internal one.

|Antenna|Average RSSI (dB)|Standard Deviation (dB)|
|---|:-:|:-:|
|External 2dBi|-20.1|1.1|
|External 5dBi Elbow|-22.5|0.9|
|External 5/8|-10.9|1.0|
|Internal (vertical)|-18.3|0.8|
|Internal (horizontal)|-32.7|1.6|

As you can see the quality of the antenna is key here. I had already tested the alegedly 5dBi external elbow antennas in my [previous post using a N1201SA](/post/analyze-your-antennas-using-an-aai-n1201sa) and noticed they are way below the expected performance. 

{{< lazyimg "images/antenna_rssi.png" >}}

{{< lazyimg "images/20190530_095126x.jpg" "The 5/8 wave antenna by Stanislav Palo has an amazing performance" >}}

So, is it worth it? Yes, I think it is if you have a good external antenna. But if you are not impressed by the results, remember to always place the gateway vertical.

## Peaking into the firmware

The main board exposes a UART interface so you can easily peak into the firmware debug log to know a little more on what it does, or even what goes wrong.

{{< lazyimg "images/20190530_155331x.jpg" >}}

{{< lazyimg "images/20190530_155401x.jpg" >}}

The first log is the boot log. You can see it reporting some info about the device and the current firmware which is 2.0.0(minihub/debug) built on 2018-12-06. 

Then you can see some interesting info. The TTI Indoor Gateway is one of the first ones to use the [Basic Station](https://doc.sm.tc/station/) LoRa Packet Forwarder instead of the Semtech Legacy one. This implementation uses a CUPS (Centralized Update and Configuration Management) server for configuration and system updates. Connection to the CUPS and the LNS (LoRaWAN Network Server) are always **secured with TLS or a token-based scheme**. You can also notice that the connection to the LNS is done using a secured websocket.

In the log you can also spot the WiFi scanning and connection, the LoRa Concentrator configuration, the connections to the Discovery Service (INFOS) and local Gateway Service instance (MUXS). And also a repetitive message with the current free heap (which dramatically goes down when the gateway starts the secured connections).

Notice the gateway also supports **Listen Before Talk** (LBT, search for `INFO: FPGA supported features: [TX filter] [Spectral Scan] [LBT]`) but it's disabled. This could be an interesting feature to achieve supporting more devices and also decrease the number of missed messages.


```
1970-01-01 00:00:02.139 [SYS:INFO] FSCK found section marker A0
1970-01-01 00:00:02.150 [SYS:INFO] FSCK section A: 64 records, 10620 bytes used, 1013380 bytes free
1970-01-01 00:00:02.621 [SYS:INFO] FSCK section A followed by erased flash - all clear.
1970-01-01 00:00:02.629 [any:INFO] HW pipe bound with fd=0
1970-01-01 00:00:02.631 [any:INFO] AIO registered for HW pipe &aio=0x3ffeea30
1970-01-01 00:00:00.006 [SYS:DEBU] ======= VER ======
1970-01-01 00:00:00.008 [SYS:DEBU] Station Version   2.0.0(minihub/debug)
1970-01-01 00:00:00.010 [SYS:DEBU] Version Commit    e17c5af
1970-01-01 00:00:00.014 [SYS:DEBU] Station Build     2018-12-06 09:30:37
1970-01-01 00:00:00.020 [SYS:DEBU] Firmware Version  2.0.0
1970-01-01 00:00:00.025 [SYS:DEBU] FW Flavor ID      semtech0
1970-01-01 00:00:00.031 [SYS:DEBU] Model             minihub
1970-01-01 00:00:00.036 [SYS:DEBU] ======= SYS ======
1970-01-01 00:00:00.045 [SYS:DEBU] CPU Freq          80 / 80000000 / 80000000
1970-01-01 00:00:00.048 [SYS:DEBU] Random Number     607715723
1970-01-01 00:00:00.053 [SYS:DEBU] Reset cause       0
1970-01-01 00:00:00.058 [SYS:DEBU] Booting USER_BIN  1
1970-01-01 00:00:00.063 [SYS:DEBU] FW start addr     0x00001000
1970-01-01 00:00:00.069 [SYS:DEBU] SDK version       2.0-dev(9ec59b5)
1970-01-01 00:00:00.075 [SYS:DEBU] Free Heap Startup 56160 bytes
1970-01-01 00:00:00.081 [SYS:DEBU] ======= MFG ======
1970-01-01 00:00:00.086 [SYS:DEBU] SN                TBMH100868000487
1970-01-01 00:00:00.091 [SYS:DEBU] MTIME             2018-12-11 07:04:18
1970-01-01 00:00:00.098 [SYS:DEBU] PERSO_HASH        0x78bcfd56
1970-01-01 00:00:00.106 [SYS:DEBU] AP_PW             cLrkpeGa
1970-01-01 00:00:00.109 [SYS:DEBU] AP MAC            XX:XX:XX:XX:XX:XX
1970-01-01 00:00:00.115 [SYS:DEBU] EUI               XX-XX-XX-FF-FE-XX-XX-XX
1970-01-01 00:00:00.122 [SYS:DEBU] STA MAC           XX:XX:XX:XX:XX:XX
1970-01-01 00:00:00.128 [SYS:DEBU] ======= CRD ======
1970-01-01 00:00:00.140 [SYS:DEBU] [cups] auth=3 -> https://mh.sm.tc:7007
1970-01-01 00:00:00.143 [SYS:DEBU]   type=0  -> /s2/cups.trust
1970-01-01 00:00:00.145 [SYS:DEBU]   type=2  -> /s2/cups.key
1970-01-01 00:00:00.158 [SYS:DEBU] [cups-bak] auth=3 -> https://mh.sm.tc:7007
1970-01-01 00:00:00.165 [SYS:DEBU]   type=0  -> /s2/cups-bak.trust
1970-01-01 00:00:00.167 [SYS:DEBU]   type=2  -> /s2/cups-bak.key
1970-01-01 00:00:00.179 [SYS:DEBU] [cups-boot] auth=3 -> https://mh.sm.tc:7007
1970-01-01 00:00:00.181 [SYS:DEBU]   type=0  -> /s2/cups-boot.trust
1970-01-01 00:00:00.183 [SYS:DEBU]   type=2  -> /s2/cups-boot.key
1970-01-01 00:00:00.193 [SYS:DEBU] [tc] auth=3 -> wss://lns.eu.thethings.network:443
1970-01-01 00:00:00.195 [SYS:DEBU]   type=0  -> /s2/tc.trust
1970-01-01 00:00:00.200 [SYS:DEBU]   type=2  -> /s2/tc.key
1970-01-01 00:00:00.207 [SYS:DEBU] [tc-bak] auth=3 -> wss://lns.eu.thethings.network:443
1970-01-01 00:00:00.214 [SYS:DEBU]   type=0  -> /s2/tc-bak.trust
1970-01-01 00:00:00.219 [SYS:DEBU]   type=2  -> /s2/tc-bak.key
1970-01-01 00:00:00.231 [SYS:DEBU] [tc-boot] - not available
1970-01-01 00:00:00.233 [SYS:DEBU] ======= MEM ======
data  : 0x3ffe8000 ~ 0x3ffe8cd8, len: 3288
rodata: 0x3ffe8ce0 ~ 0x3ffe8f70, len: 656
bss   : 0x3ffe8f70 ~ 0x3fff0420, len: 29872
heap  : 0x3fff0420 ~ 0x40000000, len: 64480
1970-01-01 00:00:00.250 [SYS:DEBU] ==================
1970-01-01 00:00:00.261 [SYS:DEBU] Start WiFi Scan
1970-01-01 00:00:01.255 [SYS:DEBU]   Free Heap: 55224 (min=54848) wifi=0 mh=1 cups=0 tc=0
1970-01-01 00:00:02.258 [SYS:DEBU]   Free Heap: 54840 (min=54712) wifi=0 mh=1 cups=0 tc=0
scandone
1970-01-01 00:00:02.688 [SYS:DEBU] mh_scanDoneCB: status=0
1970-01-01 00:00:02.690 [SYS:DEBU]                SSID |   CH @ RSSI   |  auth  | b/g/n | wps
1970-01-01 00:00:02.693 [SYS:DEBU]       XXXXXXXXXXXXX | ch-01@-65 dBm | auth=3 | 1/1/1 | 1
1970-01-01 00:00:02.700 [SYS:DEBU]          XXXXXXXXXX | ch-01@-94 dBm | auth=3 | 1/1/1 | 1
1970-01-01 00:00:02.708 [SYS:DEBU]               XXXXX | ch-06@-79 dBm | auth=3 | 1/1/1 | 0
1970-01-01 00:00:02.716 [SYS:DEBU]               XXXXX | ch-06@-81 dBm | auth=3 | 1/1/1 | 1
1970-01-01 00:00:02.724 [SYS:DEBU]        XXXXXXXXXXXX | ch-06@-93 dBm | auth=3 | 1/1/1 | 1
1970-01-01 00:00:02.732 [SYS:DEBU]      XXXXXXXXXXXXXX | ch-06@-87 dBm | auth=0 | 1/1/0 | 0
1970-01-01 00:00:02.740 [SYS:DEBU]       XXXXXXXXXXXXX | ch-06@-93 dBm | auth=3 | 1/1/1 | 1
1970-01-01 00:00:02.748 [SYS:DEBU]       XXXXXXXXXXXXX | ch-11@-77 dBm | auth=2 | 1/1/1 | 1
1970-01-01 00:00:02.756 [SYS:DEBU]               XXXXX | ch-11@-23 dBm | auth=3 | 1/1/1 | 1
1970-01-01 00:00:02.764 [SYS:DEBU]           XXXXXXXXX | ch-11@-27 dBm | auth=3 | 1/1/1 | 0
1970-01-01 00:00:02.772 [SYS:DEBU]        XXXXXXXXXXXX | ch-13@-94 dBm | auth=3 | 1/1/1 | 0
1970-01-01 00:00:03.261 [SYS:DEBU]   Free Heap: 55784 (min=54600) wifi=0 mh=2 cups=0 tc=0
1970-01-01 00:00:03.268 [SYS:DEBU] WiFi connecting to 'XXXXX'
1970-01-01 00:00:03.270 [SYS:DEBU] Setting up hostname: MiniHub-XXXXXX
1970-01-01 00:00:03.272 [SYS:INFO] DHCP is not started. Starting it...
1970-01-01 00:00:04.264 [SYS:DEBU]   Free Heap: 55784 (min=54600) wifi=1 mh=3 cups=0 tc=0
1970-01-01 00:00:05.267 [SYS:DEBU]   Free Heap: 55784 (min=54600) wifi=1 mh=3 cups=0 tc=0
scandone
state: 0 -> 2 (b0)
state: 2 -> 3 (0)
state: 3 -> 5 (10)
add 0
aid 11
pm open phy_2,type:2 0 0
cnt 
connected with XXXXX, channel 11
dhcp client start...
1970-01-01 00:00:06.270 [SYS:DEBU]   Free Heap: 54768 (min=53256) wifi=1 mh=3 cups=0 tc=0
ip:192.168.1.127,mask:255.255.255.0,gw:192.168.1.1
1970-01-01 00:00:07.274 [SYS:DEBU]   Free Heap: 54768 (min=53256) wifi=5 mh=3 cups=0 tc=0
1970-01-01 00:00:07.283 [CUP:INFO] Stopping TC and starting CUPS
1970-01-01 00:00:07.285 [CUP:INFO] Starting a CUPS session
1970-01-01 00:00:07.297 [CUP:INFO] Connecting to CUPS ... https://mh.sm.tc:7007 (try #1)
1970-01-01 00:00:07.311 [any:INFO] cert. version     : 1
serial number     : 01
issuer name       : CN=Root CA, OU=TrackCentral (ez5Cj0eN), O=TrackNet.io, C=CH
subject name      : CN=Root CA, OU=TrackCentral (ez5Cj0eN), O=TrackNet.io, C=CH
issued  on        : 2018-11-22 10:23:38
expires on        : 2024-11-20 10:23:38
signed using      : ECDSA with SHA256
EC key size       : 256 bits
1970-01-01 00:00:07.338 [AIO:INFO] cups has no cert configured - running server auth and client auth with token
1970-01-01 00:00:07.503 [CUP:VERB] Retrieving update-info from CUPS https://mh.sm.tc:7007...
1970-01-01 00:00:14.066 [SYS:DEBU]   Free Heap: 26384 (min=25880) wifi=5 mh=7 cups=1 tc=0
1970-01-01 00:00:14.305 [AIO:DEBU] [2] HTTP connection shutdown...
1970-01-01 00:00:14.312 [SYS:INFO] sys_inState - Ignoring state transition: 5
1970-01-01 00:00:14.314 [CUP:INFO] Interaction with CUPS done (no updates) - next regular check in 1d
1970-01-01 00:00:14.318 [TCE:INFO] Starting TC engine
1970-01-01 00:00:14.335 [any:INFO] cert. version     : 3
serial number     : XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX
issuer name       : O=Digital Signature Trust Co., CN=DST Root CA X3
subject name      : C=US, O=Let's Encrypt, CN=Let's Encrypt Authority X3
issued  on        : 2016-03-17 16:40:46
expires on        : 2021-03-17 16:40:46
signed using      : RSA with SHA-256
RSA key size      : 2048 bits
basic constraints : CA=true, max_pathlen=0
key usage         : Digital Signature, Key Cert Sign, 1970-01-01 00:00:14.374 [AIO:INFO] tc has no cert configured - running server auth and client auth with token
1970-01-01 00:00:14.469 [TCE:INFO] Connecting to INFOS: wss://lns.eu.thethings.network:443
1970-01-01 00:00:15.075 [SYS:DEBU]   Free Heap: 23768 (min=23496) wifi=5 mh=7 cups=8 tc=1
1970-01-01 00:00:15.364 [TCE:INFO] Infos: 58a0:cbff:fe80:2e5 muxs-::0 wss://lns.eu.thethings.network:443/router-xxxx:xxxx:xxxx:xxxx
1970-01-01 00:00:15.369 [AIO:DEBU] [2] ws_close reason=1000
1970-01-01 00:00:15.372 [AIO:DEBU] [2] Server sent close: reason=1000
1970-01-01 00:00:15.377 [AIO:DEBU] [2] WS connection shutdown...
1970-01-01 00:00:15.399 [any:INFO] cert. version     : 3
serial number     : XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX:XX
issuer name       : O=Digital Signature Trust Co., CN=DST Root CA X3
subject name      : C=US, O=Let's Encrypt, CN=Let's Encrypt Authority X3
issued  on        : 2016-03-17 16:40:46
expires on        : 2021-03-17 16:40:46
signed using      : RSA with SHA-256
RSA key size      : 2048 bits
basic constraints : CA=true, max_pathlen=0
key usage         : Digital Signature, Key Cert Sign, 1970-01-01 00:00:15.437 [AIO:INFO] tc has no cert configured - running server auth and client auth with token
1970-01-01 00:00:15.527 [TCE:VERB] Connecting to MUXS...
1970-01-01 00:00:16.103 [SYS:DEBU]   Free Heap: 23536 (min=17752) wifi=5 mh=7 cups=8 tc=3
1970-01-01 00:00:16.374 [TCE:VERB] Connected to MUXS.
1970-01-01 00:00:16.685 [S2E:WARN] Unknown field in router_config - ignored: bcning (0x1EE5E245)
1970-01-01 00:00:16.689 [S2E:WARN] Unknown field in router_config - ignored: config (0xF7A3E35F)
1970-01-01 00:00:16.693 [S2E:WARN] Unknown field in router_config - ignored: protocol (0xFD309030)
1970-01-01 00:00:16.701 [S2E:WARN] Unknown field in router_config - ignored: regionid (0xE6FFB211)
1970-01-01 00:00:16.713 [S2E:WARN] Unknown field in router_config - ignored: upchannels (0x7FCAA9EB)
2019-05-30 13:40:06.558 [RAL:INFO] Lora gateway library version: Version: 4.1.1;
2019-05-30 13:40:06.562 [RAL:DEBU] SX1301 txlut table (0 entries)
2019-05-30 13:40:06.564 [RAL:VERB] SX1301 rxrfchain 0: enable=1 freq=867500000 rssi_offset=-166.000000 type=2 tx_enable=1 tx_notch_freq=0
2019-05-30 13:40:06.573 [RAL:VERB] SX1301 rxrfchain 1: enable=1 freq=868500000 rssi_offset=-166.000000 type=2 tx_enable=0 tx_notch_freq=0
2019-05-30 13:40:06.585 [RAL:VERB] SX1301 ifchain  0: enable=1 rf_chain=1 freq=-400000 bandwidth=0 datarate=0 sync_word=0/0
2019-05-30 13:40:06.596 [RAL:VERB] SX1301 ifchain  1: enable=1 rf_chain=1 freq=-200000 bandwidth=0 datarate=0 sync_word=0/0
2019-05-30 13:40:06.607 [RAL:VERB] SX1301 ifchain  2: enable=1 rf_chain=1 freq=0 bandwidth=0 datarate=0 sync_word=0/0
2019-05-30 13:40:06.617 [RAL:VERB] SX1301 ifchain  3: enable=1 rf_chain=0 freq=-400000 bandwidth=0 datarate=0 sync_word=0/0
2019-05-30 13:40:06.628 [RAL:VERB] SX1301 ifchain  4: enable=1 rf_chain=0 freq=-200000 bandwidth=0 datarate=0 sync_word=0/0
2019-05-30 13:40:06.639 [RAL:VERB] SX1301 ifchain  5: enable=1 rf_chain=0 freq=0 bandwidth=0 datarate=0 sync_word=0/0
2019-05-30 13:40:06.649 [RAL:VERB] SX1301 ifchain  6: enable=1 rf_chain=0 freq=200000 bandwidth=0 datarate=0 sync_word=0/0
2019-05-30 13:40:06.660 [RAL:VERB] SX1301 ifchain  7: enable=1 rf_chain=0 freq=400000 bandwidth=0 datarate=0 sync_word=0/0
2019-05-30 13:40:06.671 [RAL:VERB] SX1301 ifchain  8: enable=1 rf_chain=0 freq=-200000 bandwidth=2 datarate=2 sync_word=0/0
2019-05-30 13:40:06.681 [RAL:VERB] SX1301 ifchain  9: enable=1 rf_chain=1 freq=300000 bandwidth=3 datarate=50000 sync_word=0/0
2019-05-30 13:40:06.693 [RAL:VERB] SX1301 LBT not enabled
2019-05-30 13:40:06.699 [RAL:INFO] Station device:  (PPS capture disabled)
INFO: FPGA supported features: [TX filter]  [Spectral Scan]  [LBT] 
2019-05-30 13:40:11.084 [S2E:INFO] Configuring for region: EU863 -- 863.0MHz..870.0MHz
2019-05-30 13:40:11.087 [S2E:VERB]   DR0  SF12/BW125 
2019-05-30 13:40:11.090 [S2E:VERB]   DR1  SF11/BW125 
2019-05-30 13:40:11.092 [S2E:VERB]   DR2  SF10/BW125 
2019-05-30 13:40:11.096 [S2E:VERB]   DR3  SF9/BW125 
2019-05-30 13:40:11.100 [S2E:VERB]   DR4  SF8/BW125 
2019-05-30 13:40:11.105 [S2E:VERB]   DR5  SF7/BW125 
2019-05-30 13:40:11.110 [S2E:VERB]   DR6  SF7/BW250 
2019-05-30 13:40:11.115 [S2E:VERB]   DR7  FSK 
2019-05-30 13:40:11.119 [S2E:VERB]   DR8  undefined
2019-05-30 13:40:11.123 [S2E:VERB]   DR9  undefined
2019-05-30 13:40:11.128 [S2E:VERB]   DR10 undefined
2019-05-30 13:40:11.132 [S2E:VERB]   DR11 undefined
2019-05-30 13:40:11.141 [S2E:VERB]   DR12 undefined
2019-05-30 13:40:11.143 [S2E:VERB]   DR13 undefined
2019-05-30 13:40:11.146 [S2E:VERB]   DR14 undefined
2019-05-30 13:40:11.151 [S2E:VERB]   DR15 undefined
2019-05-30 13:40:11.155 [S2E:VERB]   TX power: 16.0 dBm EIRP
2019-05-30 13:40:11.161 [S2E:VERB]             27.0 dBm EIRP for 869.4MHz..869.65MHz
2019-05-30 13:40:11.168 [S2E:VERB]   JoinEui list: 0 entries
2019-05-30 13:40:11.174 [S2E:VERB]   NetID filter: FFFFFFFF-FFFFFFFF-FFFFFFFF-FFFFFFFF
2019-05-30 13:40:11.181 [S2E:VERB]   Dev/test settings: nocca=0 nodc=0 nodwell=0
2019-05-30 13:40:11.194 [SYS:DEBU]   Free Heap: 19064 (min=17544) wifi=5 mh=7 cups=8 tc=4
2019-05-30 13:40:12.199 [SYS:DEBU]   Free Heap: 19064 (min=17544) wifi=5 mh=7 cups=8 tc=4
2019-05-30 13:40:13.203 [SYS:DEBU]   Free Heap: 19064 (min=17544) wifi=5 mh=7 cups=8 tc=4
2019-05-30 13:40:14.207 [SYS:DEBU]   Free Heap: 19064 (min=17544) wifi=5 mh=7 cups=8 tc=4
```

When the gateway receives a message form a node the debug log reports something like this:

```
2019-05-30 13:42:33.899 [S2E:VERB] RX 867.9MHz DR5 SF7/BW125 snr=8.2 rssi=-12 xtime=0xB0000013C9E8C - updf mhdr=40 DevAddr=26011A31 FCtrl=00 FCnt=3 FOpts=[] 0148 mic=1005211449 (14 bytes)
```

Error debug you say? Yes, actually the gateway has been reported to "miss" packets from time to time. Not my experience, but I had already noticed the LED starts blinking every few minutes. And this is what the debug log says when that happens:

```
2019-05-30 13:41:04.410 [SYS:DEBU]   Free Heap: 19064 (min=17544) wifi=5 mh=7 cups=8 tc=4
2019-05-30 13:41:05.413 [SYS:DEBU]   Free Heap: 19064 (min=17544) wifi=5 mh=7 cups=8 tc=4
2019-05-30 13:41:06.417 [SYS:DEBU]   Free Heap: 19064 (min=17544) wifi=5 mh=7 cups=8 tc=4
2019-05-30 13:41:06.522 [AIO:DEBU] ssl_tls.c:6546 MBEDTLS[1]: mbedtls_ssl_read_record() returned -30848 (-0x7880)
2019-05-30 13:41:06.526 [AIO:ERRO] Recv failed: SSL - The peer notified us that the connection is going to be closed
2019-05-30 13:41:06.532 [AIO:DEBU] [2] WS connection shutdown...
2019-05-30 13:41:06.543 [TCE:VERB] Connection to MUXS closed in state 4
2019-05-30 13:41:06.551 [TCE:INFO] MUXS reconnect backoff 1s (retry 0)
2019-05-30 13:41:07.421 [SYS:DEBU]   Free Heap: 47312 (min=17544) wifi=5 mh=7 cups=8 tc=5
2019-05-30 13:41:07.562 [any:INFO] cert. version     : 3
```

So apparently the gateway gets disconnected from the LNS due to a SSL error... I'm sure this is something that can be solved in software and apparently is due to some instability in the network server... **There will soon be software and firmware updates**, so hopefully this will be solved then.


## Final touch

Under the TTI sticker there is the original The Tnings Industries Conference sticker (this confirms this is one of the gateways from first batch that were given away during the conference) and under this one the logo of manufacturer: tabs. Well, the [Tabs website](https://tabs.io/) is down at the moment but the project was initially created by [TrackNet](http://tracknet.io/), a swiss company. 

{{< lazyimg "images/20190523_120729x.jpg" "When you remove the The Things Industries logo you see the original TTI Conference one" >}}

{{< lazyimg "images/20190523_121013_HDRx.jpg" "And below that the carved logo of Tabs" >}}

{{< lazyimg "images/20190523_121104_HDRx.jpg" "Final 'hack' ;)" >}}


## How to enable the CP2102N on the TTIG

**Content and pictures in this section by [@_pub_feuerrot](https://twitter.com/_pub_feuerrot), licensed under [CC0](https://creativecommons.org/share-your-work/public-domain/cc0/). Thank you very much!!**

The TTIG contains an USB-UART interface, which is disabled by default.

{{< lazyimg "images/ttig_overviewx.jpg" "Overview of the TTIG main PCB with the CP2102N on the bottom of the image" >}}
{{< lazyimg "images/ttig_usbserialx.jpg" "Detail on the CP2102N USB to serial IC" >}}

[The TTIG FAQ](https://www.thethingsnetwork.org/forum/t/ttig-the-things-indoor-gateway-faq/22178#i-want-to-get-in-depth-insight-in-what-my-gateway-is-doing-is-that-possible-22) links two posts which either recommend to [just short](https://www.thethingsnetwork.org/forum/t/new-gateway-the-things-indoor-gateway/22049/224) or [place a resistor](https://www.thethingsnetwork.org/forum/t/ttig-problems-no-location-data-wrong-date-time-wrong-channel-and-stability-issues/29652/102) on R86 to enable the CP2102N.

[The datasheet](https://www.silabs.com/documents/public/data-sheets/cp2102n-datasheet.pdf) recommends a 1k resistor:

> In all cases, a 1 kΩ pull-up on the RSTb pin is recommended. This pull-up should be tied to VIO on devices that have it. On devices where VIO is connected to VDD or devices that do not have VIO, this pull-up should be tied to VDD. The RSTb pin will be driven low during power-on and power failure reset events.

As the CP2102N tries to drive the pin low I'd recommend a resistor instead of a short. The pads of R86 are quite small, it could be easier to solder a non-SMD resistor between [the square pad of J1 and TP5](https://www.thethingsnetwork.org/forum/t/new-gateway-the-things-indoor-gateway/22049/227). There is an additional via just above the upper pad of R86, which might connect to the ESP8266 (so it can control the USB-UART interface), but without reverse engineering the schematic by sanding down each layer of the PCB, I can't say for sure.

{{< lazyimg "images/ttig_resistorx.jpg" "1kΩ pull-up on R86 to enable the CP2102N" >}}


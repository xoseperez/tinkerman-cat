---
author: Xose
comments: true
date: 2017-03-30 16:33:51+00:00
layout: post
slug: kk2015-based-ai-light
title: "KK2015 based Ai Light"
post_id: 1634
image: "images/AiLight_KK2015_2s-1200x800.jpg"
categories:
- Analysis
tags:
- ailight
- esp8266
- kk2015
---

Really busy these days. I have some drafts ongoing but I wanted to publish this short post right away.

One of the readers of this blog, Michel Clavette, sent me these pics just yesterday. He bought 5 Ai Light bulbs and to his surprise two of them do not have an ESP8266 microcontroller but instead this IC labelled **KK2015**.

{{< lazyimg "images/AiLight_KK2015_1s.jpg" "KK2015 powered Ai Light. Picture by Michel Clavette" >}}

It looks like a drop-in replacement for the ESP8266 since it has the same footprint and all the other components are (apparently) the same. But we have not been able to find even the slight reference to this one on the whole Internet...

<del>So this is an open question: does anyone know about this chip?</del>

**UPDATE 20170407**: I've been confirmed **the KK2015 is the very same ESP8266** marked with a different label, reason unknown yet.

**UPDATE** **20170407** (bis): A new update thanks to a contact  that was involved in design of the Ai Light. The mark belongs to [Konke](http://www.ikonke.com/), "a big customer of Espressif, so Espressif provides mark service for Konke in 2016." So after all, **the KK2015 is a rebranding of the ESP8266**, nothing more.

Michel will try to flash it using the same procedure as for the ESP8266. Hope we will have some info from him soon.		

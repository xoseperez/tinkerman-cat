---
author: Xose
comments: true
date: 2017-06-28 23:04:32+00:00
layout: post
slug: itead-studio-sonoff-sc-revisited
title: "Itead Studio Sonoff SC Revisited"
post_id: 1907
image: "images/20170628_211524s-1200x800.jpg"
categories:
- Projects
tags:
- adafruit
- atmega328
- dht11
- dht22
- dust
- ekmb
- esp8266
- fan
- ftdi
- harm aldick
- itead studio
- mg15b
- microwave
- mqtt
- neopixels
- node-red
- panasonic
- pir
- rgb led
- sharp gp2y1010au0f
- sonoff sc
- ws2812
- wsn
---

A few months ago I wrote about the **[Sonoff SC](/post/sonoff-sc-with-mqtt-and-domoticz-support/)** sensor hub by Itead Studio. It's a device with a [**Sharp GP2Y1010AU0F**](http://s.click.aliexpress.com/e/JyJYbEA) [Aliexpress] dust sensor, a **DHT11** humidity and temperature sensor, an LDR as light sensor and a mic. The sensors are driven by an **ATMega328P** microcontroller but there is also an **ESP8266** on board for WiFi communication, a pretty standard set up when you have several sensors and the ESP8266 GPIOs are just not enough.

{{< lazyimg "images/20170105_125851s-1200x800.jpg" >}}

On the first post I already did a small mod to replace the DHT11 humidity and temperature sensor with a more accurate and pin compatible **DHT22**. Since then, several readers have contributed with code and ideas. My progress implementing and testing them is slow, so I though about writing a first post about some modifications I (and others) have done to the device.

So this is a **work in progress** post. At the moment, all the code for these modifications is in the **dev branch** of the repository.

> This custom Sonoff SC firmware is released as **free open software **and can be checked out at my [**SonoffSC repository**](https://github.com/xoseperez/sonoffsc) on Github.

<!-- more -->

## Flashing the ATMega328p via bootloader

First I'd like to say is that, although I used a AVR programmer for the ATMega328 in my last post this is not required. You can use an **[FTDI USB to TTL](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338044841&mpre=http%3A%2F%2Fwww.ebay.com%2Fitm%2FFTDI-FT232RL-USB-to-TTL-Serial-Converter-Adapter-Module-5V-3-3V-For-Arduino-BE-%2F122434085129)** [Ebay, also [Aliexpress](http://s.click.aliexpress.com/e/ufYRRrb)] board to flash the AVR too since it comes with the Arduino bootloader.

Just remember to connect TX to TX and RX to RX since the pins in the board are labeled from the programmer point of view. Connect RST to the DTR line and the power lines and you are good to go.

{{< lazyimg "images/20170105_130308s.jpg" >}}

## Adding a microwave radar as presence sensor

Now, this is a suggestion in the issues section of the repository. Since the usual PIR sensors are quite bulky my first idea was to use one of those nice **[Panasonic EKMB PIR](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338044841&mpre=http%3A%2F%2Fwww.ebay.com%2Fitm%2FNEW-Genuine-OEM-Panasonic-EKMB1101111-Sensor-Motion-Pyroelectric-PIR-197-16-4-%2F252570294249)** [Ebay]. They are small, very sensitive (with versions with up to 12m detection) and really expensive. They are ultra low power devices meant for low power sensors, but that's not the case here.

So I moved to a microwave sensor I first spotted at [Scargill tech blog](http://tech.scargill.net/microwave-radar/). This is a **[5.8GHz microwave sensor](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338044841&mpre=http%3A%2F%2Fwww.ebay.com%2Fitm%2F5-8GHZ-Microwave-Radar-Sensor-6-9M-Smart-Switch-for-Home-Control-3-3-20V-DC-ST-%2F172264554543)** [Ebay, also on [Aliexpress](http://s.click.aliexpress.com/e/nQnmYvZ)] that costs around 1.5 USD, has a configurable range of up to 8 meters and a detection pulse that's 30s by default. The device can "sense" movement even through a thin wall so the SonoffSC enclosure is no big deal. But after a few tests I realized it was capturing people on the street outside my house so I decided to** replace the resistor in R9_1 with a 100k** one. Now the range is about 3-4 meters, safely covering the main walking paths in my livingroom. I also added a **noise filter** in the power line using a capacitor and a resistor as per Pete's advice.

{{< lazyimg "images/20170515_091255s.jpg" >}}

The microwave sensor is connected to GPIO13 (SCK) in the ISP header. A bit of solder to the VCC, GND and SCK lines and hot glue again to prevent it from going loose.

{{< lazyimg "images/20170513_232054s.jpg" >}}

The placement of the sensor is still under testing. Right now I have it on the base of the SonoffSC, hot glued to it with the cables running to the oposite side of the board. Even thou the placement is neat, I have found that sometimes it misses a moving target, still testing if it's due to the microwave beam or the code.

{{< lazyimg "images/20170513_232224s.jpg" >}}

Since GPIO13 has no interrupts on the ATMega328P the code checks in the main loop for the pin state. If it changes it immediately reports the change to the ESP8266 via serial.

## Adding an RGB LED ring - again

Actually this one was already done thanks to the contributions by Blair Thompson (check his [video here](https://www.youtube.com/watch?v=oyOfErs7rtM)). But I decided to go for a slighly different path. The ring is a **[24 WS2812 LED ring](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338044841&mpre=http%3A%2F%2Fwww.ebay.com%2Fitm%2F1PCS-WS2812B-5050-RGB-LED-Ring-24Bit-RGB-LED-Integrated-Drivers-For-Arduino-NE-%2F401165798963)** [Ebay, also on [Aliexpress](http://s.click.aliexpress.com/e/qby7eqR)] placed in the middle compartment of the enclosure. The VCC, GND and DI lines are brought out and connected to the ISP header like with the microwave sensor using GPIO 12 (MISO) now. Just like Blair's approach.

{{< lazyimg "images/20170513_232204s.jpg" >}}

## WS2812FX

The difference with Blair's code is in the library I'm using. On top of  the Adafruit NeoPixel library I'm using the **[WS2812FX library](https://github.com/kitesurfer1404/WS2812FX)** by Harm Aldick (kitesurfer1404). The library defines an indexed list of effects for LED stripes and so. Very convenient to be called from a remote MQTT based RPC.

Another incompatible change is that by default (although this might change in the final version) it performs a blue fade in when movement is detected and fades out when no movement. This behaviour is hardcoded in the current dev branch but I might:

  1. Allow the user to configure the behaviour via the web interface
  2. Use an external service (**Node-RED**) to write the business inteligence and just send data and receive instructions via **MQTT**

I like the second option better, moving the intelligence outside the device looks reasonable and more flexible. But I'm aware most users might want to have the behaviour embedded...

Blair's code is still in the master branch, version 0.3.0. Once the current dev branch is merged to master it will be tagged as version 1.0.0.

## Adding a mini-fan to improve dust sensor readings

One of the issues several people have detected is that the dust sensor has very little range. This is probably due to it being inside an enclosure with little air vent. There was a little debate about it in the repo and I decided to try a small [**SEPA MF15B-05 5V**](http://s.click.aliexpress.com/e/AeeEeUr) fan I found on Aliexpress. It's a 15x15mm 0.06A fan.

{{< lazyimg "images/20170526_101539s.jpg" >}}

The fan is driven by the LED signal in the board (GPIO7). I decided to hot glue it on top of the dust sensor so it forces the air to flow from the outside of the enclosure down. Whenever a new dust reading is scheduled or requested, the fan starts up for 5 seconds before the reading is actually performed. I have also changed the code to use ug/m3 instead of mg/m3.

{{< lazyimg "images/20170526_101351s.jpg" >}}

I have been testing it for the last 2 days. Still not enough data but it looks like the daily average value is twice the value it used to be and the standard deviation is also greater. I still have to test with the fan running for a longer time (or even all the time) before reading the sensor data. The good thing is that the fan is noise-less (as far as I can hear).

## Changes in the code

The code has also gone through a major review. Basically importing features from the [ESPurna project](https://github.com/xoseperez/espurna). Aside from the changes to support the new functionalities in this post, other main changes include:

  * Improvements in the **SerialLink** library (now it allows long integers as argument, good for RGB values)
  * **Debug via UDP** (using Serial is not possible since it's the communication channel between both microcontrollers)
  * Moved **debug strings to program memory**
  * **Embed the webserver pages** and resources into the firmware (faster, more free room for code): will write about it soon

There are already some other minor changes, improvements, bug fixes. I want to include more issues from the repo before merging this to master. Some of these improvements include new sensors. At some point you will have to choose from the different options since there are just so many GPIOs.

## Flashing

Remember you will have to** flash both microcontrollers again.** But this time it won't be necessary to flash the SPIFFS partition on the ESP8266. You can flash both using an FTDI USB to TTL programmer like the one I linked before. While flashing remove the communication jumpers. Also remember that the pins are labeled from the programmer point of view.

Since there are changes in the **memory layout**, erase the ESP8266 flash memory prior to upload the new code. The new layout is the 1m64 (1 Mbyte with 64Kbytes for SPIFFS). If you are using PlatformIO these settings are defined in the project file. Otherwise you will have to select the right option from the Arduino menu if you want OTA to work.

That's it. As I have already said this is a work in progress so there might be changes and new additions to the project. Check the repo from time to time. I'm aware there are missing details here and there so don't hesitate in asking. And thanks for reading me :)		

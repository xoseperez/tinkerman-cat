---
author: Xose
comments: true
date: 2014-01-23 00:04:57+00:00
layout: post
slug: counting-events-with-arduino-and-pcf8583
title: "Counting events with Arduino and PCF8583"
post_id: 544
image: "images/6710-WIND01_1.jpg"
categories:
- Code
- Learning
- Projects
tags:
- arduino
- code
- events
- i2c
- pcf8583
---

Hey!

I've been away for some time. It's not that I had stopped tinkering, but work and laziness have kept me away from the blog. During these months I have been working mostly on a new weather station (yes, yet another weather station or YAWS). The project was a step forward in a lot of aspects, from carpentry to remote reporting hacking cheap chinese routers and 3G dongles, from new libraries for Arduino to a bidirectional HTTP to MQTT bridge in node.js...

The result has been kind of a fail... mostly due to improper casing, humidity has been a major enemy. Anyway there are quite a few aspects to talk about, so I'll try to write a series of posts about different hardware and software components involved in the project.

## Counting the wind and rain

Two of these components share the same interface: when an event happens a pulse is triggered. The anemometer is a cheap plastic model I bought from [todoelectronica.com](http://todoelectronica.com/sensor-viento-brazo-flexible-p-9978.html). It's specifications state than a 10km/h wind correlates with 4 pulses per second, so it is a simple pulse counter.

{{< lazyimg "images/6710-WIND01_1.jpg" "Anemometer" >}}

The rain gauge is a little more tricky... I bought a [wireless rain gauge at ebay](http://www.ebay.com/sch/i.html?_sacat=0&_nkw=wireless+rain+gauge) but I really didn't need the wireless feature, it was just a convenient model to hack. The sensor has a seesaw with two small compartments where rain drops fall when passing through the top hole. The seesaw has a small magnet in the middle. When the water weight moves the seesaw down the magnet closes a reed switch triggering the pulse. The water evacuates and the other compartment starts receiving the water.

{{< lazyimg "images/raingauge.jpg" "Wireless Rain Gauge" >}}

I hacked the rain sensor disabling all the circuitry and soldering a couple of wires to the reed switch with a load resistor in series. By "disabling the circuitry" I mean cutting the lines to the switch to avoid spurious currents to create noise but leaving the circuit, so it fits nicely inside the sensor.

I decided to use a standard interface for the cables, a 3.5 stereo jack (a mono jack would have been enough but I didn't have any around). They plug into the main board of the weather station and the signals go to the event counter pins of a couple of PCF8583.

The [PCF8583](http://www.nxp.com/documents/data_sheet/PCF8583.pdf) is very similar to the [PCF8563](http://www.nxp.com/documents/data_sheet/PCF8563.pdf) but it adds several nice features: hardware configurable I2C address with 2 different addresses available (you can throw 2 of these IC in your project without mush hassle), 240 bytes of free RAM, more alarm options and an event counter mode.

According to the datasheet, "The event counter mode is used to count pulses externally applied to the oscillator input (OSCO left open-circuit).". The count is stored as BCD values in 3 registers. BCD stands for [Binary-coded decimal](http://en.wikipedia.org/wiki/Binary-coded_decimal), the most common implementation is to split a byte into 2 groups of 4 bits and use each group to represent a decimal number from 0 to 9. So, for instance, 37 would be encoded as "0011 0111". Thus, 3 registers or 3 bytes allow a maximum of 1 million events. This IC is very convenient for any project where you want to remove the responsibility of counting events from your microprocessor, and it would have been a very good choice for my [smartmeter pulse counter](http://tinkerman.eldiariblau.net/smartmeter-pulse-counter-1/).

To easily access the IC features I have written an [Arduino library for the PCF8586](https://bitbucket.org/xoseperez/pcf8583). The library is not complete, there are some methods to be implemented yet. But the core time and event counter methods are already there.

To library comes with a simple exemple of usage (I have to add more examples) but to use it as an event counter you will only have to write something like:

```
#include

// declare an instance of the library for IC at address 0xA0
// (A0 pin connected to ground)
PCF8583 counter(0xA0);

void setup {

    // configure serial link to debug
    Serial.begin(9600);

    // configure PCF8586 to event counter mode and reset counts
    counter.setMode(MODE_EVENT_COUNTER);
    counter.setCount(0);

}

void loop {

    // report the events every second
    Serial.println(counter.getCount());
    delay(1000);

}

```

## Issues

There are a couple of issues to take into consideration.

First: the counter counts double. That's it: for every pulse it looks like it counts the rising AND the falling edges so you end up with double the counts. I have not been able to find a reason for this, nor even in the datasheet. I have tested it with a controlled set up, cleaning the input with an RC filter and a Schmidt Trigger and it counts exactly double it should...

Second: the maximum external clock frequency in event counter mode is rated to 1MHz. In the real world of my projects noise is an issue with frequencies way bellow that mark, so I only accept sensible values depending on what I'm measure. For instance, the above anemometer sends 4 pulses per second for a 10km/h wind. Fortunately, where I live there are no hurricanes and I have not checked but I doubt that there have ever been gusts of more than 150km/h. That sets a maximum of 60Hz. So in my code I have a check to not trust any value over 60 counts per second.

## Final note

Finally, there are some other libraries for the PCF8586, like [this one by Ryan Mick](http://wiki.netduino.com/PCF8583-Real-Time-Clock-and-Event-Counter.ashx)for Netduino or [this other one by Eric Debill](https://github.com/edebill/PCF8583) for Arduino, that are worth checking as a reference.

As always, any comment will be more than welcomed.

It's nice to be back :)		

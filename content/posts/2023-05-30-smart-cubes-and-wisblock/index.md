---
title: "Smart Cubes and WisBlock"
date: 2023-05-30T14:00:00+02:00
post_id: 20230525
author: Xose
comments: true
layout: post
image: "images/20230529_141845x.jpg"
description: "Ten years ago I tried to learn to solve a Rubik's cube. I'm not a speedcuber, but the difference in technology between those cubes 10 years ago and nowadays cubes is incredible. Now, with smartcubes it's also fun!"
categories:
- Code
tags:
- smartcube
- speedcubing
- wisblock
- ble
- rakwireless
- rubik
- rubik's cube
- maglev
---

Ten years ago I had one of those geek urges: learning to solve Rubik's Cube or Magic Cube. Goal was not to do it fast, just to do it. Back then I managed to memorize the 6-7 algorithms for a novice solver and get in done in less than 5 minutes. Not a world record, I know, still I was pretty happy with the experience.

These last months there has been some hype around the cube in our local school and my younger daughter wanted to learn to do it. So I searched for my old cheatsheet and, using my now 10 years old cube, I tought her the basics of the algorithm notation so she could read the steps and learn to solve it. The next day she was doing it in less than 90 seconds. Well, not a speedcuber either but she's a faster learner than I am. Also, her fingers move way faster than mine. Actually she's fast enough that my old cube started crackling and a few times a cubelet just popped out. 

So I thought I should invest a few euros in a new cube for her. And this is where it all started again.

<!-- more -->

## Hardware drives improvement

This table shows te evolution of the WR and the average world record (AV5) every 5 years for the last 20 years.

|Year|WR|AV5 WR|
|---|:-:|:-:|
|2003|16.53|20.00|
|2008|7.08|11.28|
|2013|5.55|8.53|
|2018|4.22|5.80|
|2023|3.47|4.69|

Fastest solving depends on a lot of things. Current WR has been unbeaten for 4 years now. I think it's more interesting to focus on AV5 (average of 5 solves removing the fastest and the slowest one). Here the progression, from my point of view, is due to 2 main factors: the increasing number of people solving the cube and the technology changes in the cubes. I think we can safely discard human evolution as a key factor here.

Have there been improvements in the technology of the cubes? Sure! Changes in the shapes of the cubelets to allow cut and countercuts at wild angles, the textures of the inside faces to help sliding, special coating in the outside faces of the cubelets to prevent it from slipping, special cores, magnets to fix the positions and even magnetic levitation solutions to replace springs! I'm sure all this things have contributed to faster, more reliable cubes and hence more chances to get better times.

## Hardware drives fun

But the one thing that really caught my attention was "smart cubes". These are cubes with bluetooth connectivity that report a series of data to be used from a mobile application. These data include turns, cube status and optionally gyro information and battery status. These cubes are not yet as fast as top speedcubes, but they are getting closer and closer and I do not doubt they will soon be standard in speedcubing competitions which will lead to further time improvements (no need for manual start/stop) and analysis of the solves.

I'm 2-3 years late to this "revolution". Manufacturers of these cubes provide their own applications (TBH, the ones I have tested are really bad) but in this time the open source community has been working on web applications compatible with these cubes. Most of the times (always?) developers have had to reverse engineer the cubes protocols in order to add compatibility. Job done. 

One of these projects, the one I would recommend because it works great and is open source is [cstimer](https://cstimer.net/). Code can be found on the [cstimer repo at github](https://github.com/cs0x7f/cstimer). There, under `src/js/bluetooth.js`, you have the decoding of the protocols for different manufacturers: Giiker, Gan, GoCube and Moyu. Here you have links to some of these cubes on Aliexpress (please note these are affiliate links):

* [Giiker Electronic Bluetooth Smart Cube](https://s.click.aliexpress.com/e/_Dm1GTOV)
* [XiaoMi Bluetooth Magic Cube](https://s.click.aliexpress.com/e/_DFSlXJR)
* [GAN 356 i3 3x3 Magnetic Magic Cube](https://s.click.aliexpress.com/e/_DejEzdL)
* [GAN i3 Carry Smart Magic Cube](https://s.click.aliexpress.com/e/_DeeLuhR)
* [MoYu Weilong Ai Smart Cube](https://s.click.aliexpress.com/e/_DBygZmN)

{{< lazyimg "images/20230529_141717x.jpg" "Giiker and GAN 356 i3 Smart Cubes" >}}

## WisBlock Smart Cube Companion device

I couldn't help get two of those cubes to play with: the GAN 356 i3 and the Giiker Smart Cube. They are quite different, I get better times with the GAN but I like the feeling of the Giiker better. Anyways, since I'm not a speedcuber I'm not going to focus on the play-ability of the cubes but on the "smart" part of them.

it was a perfect excuse to trigger a personal project using BLE with the WisBlock platform.

> Disclaimer: I'm currently working for RAKwireless, the manufacturer of the WisBlock platform. That meant that I had some boards and accessories laying around already and that I have access to some resources that are not still available. Having had the chance to play with it a lot I can say WisBlock is the best prototyping platform I've worked with so far.

I'm using the RAK4631 Core for WisBlock which is based on Nordic nRF52840 and includes BLE and LoRa connectivity. I am not using LoRa for this project thou. Added to it I needed a nice display (RAK14014) and a buzzer for simple notifications (RAK18001). A colleague sent me the 3D for a custom enclosure for the display based on the Unified Enclosure system for WisBlock. This is not yet available for purchase, hence I had to 3D print it myself. It will be soon in the store. In the last minute I added an audio jack to support timers like [QiYi Timer Magic Cube Speed Timer](https://s.click.aliexpress.com/e/_DlMhpDL) (affiliate link).

So this is the bill of materials:

* [RAK19007 WisBlock Base Board 2nd Gen](https://store.rakwireless.com/products/rak19007-wisblock-base-board-2nd-gen)
* [RAK4631 Nordic nRF52840 BLE Core](https://store.rakwireless.com/products/rak4631-lpwan-node)
* [RAK14014 230x320 Full Color TFT Display](https://store.rakwireless.com/products/240x320-pixel-full-color-tft-display-with-touch-screen-rak14014)
* [RAK18001 Buzzer module](https://store.rakwireless.com/products/wisblock-buzzer-module-rak18001)
* Custom 3D printed enclosure
* Small LiPo battery

{{< lazyimg "images/20230527_090326x.jpg" "WisBlock connections" >}}

The setup is pretty straight forward. I recommend you to read the basics about the [WisBlock platform](https://docs.rakwireless.com/Product-Categories/WisBlock/RAK19007/Datasheet). You only need to know that the code expects the buzzer to be on slot A and the audio jack is connected to the secondary hardware serial in the J10 header. This is optional, of course, but it will require some soldering if you want support for a StackMat. 

## Features

As said, the initial goal was to support Smart Cubes. There are several brands out there but there are basically 4 different manufacturers: GAN, Moyu, Giiker and GoCube. GAN and Moyu are well known for their speedcubes and they also do smart cubes. Giiker and GoCube are focused only on smart cubes and they also white label them: Xiaomi uses a Giiker cube and Rubik's themselves have the Rubik's Connected based on GoCube's.

But for now I only have access to a Giiker and a GAN 365 i3, so those are the ones the device supports so far. Shouldn't be hard to implement support for the others but since I could not test them I focused on adding features instead of more cubes. 

List of features right now:

* Support for GAN and Giiker smart cubes (tested with GAN 356 i3 and original Giiker cubes)
* Support for StackMat protocol via an audio jack
* Support for manual interface (swipe to move from menu to menu, tap to start/stop timer)
* Cube actions (only for smart cubes):
    * 4U to go to scramble page, 4U again to start inspect without scrambling
    * Any movement after solving the cube to see stats
* Keeps statistics for 4 different users: PB, AVG5, AV12 and last 12 individual solves including time, turns and TPS (the later 2 only available for smart cubes)
* Reset user stats (long tap on the screen while at the user's stat page)
* Touchscreen, colorful interface
* Visual representation of the cube state in 2D and 3D (only for smart cubes)
* 21 movements scramble generation following WCA rules
* Automatic start/stop timer for smart cubes
* Built-in LiPo battery for several hours of usage

> The WisBlock SmartCube Companion code is released under **GPL-3.0 license** and can be checked out at my [**WisBlock SmartCube Companion repository**](https://github.com/xoseperez/wisblock-smartcube-companion) on GitHub.


## Gallery

{{< lazyimg "images/20230425_221214x.jpg" "Enclosure top" >}}
{{< lazyimg "images/20230425_221229x.jpg" "USB connector hole (power and flash)" >}}
{{< lazyimg "images/20230527_090548x.jpg" "GAN Smart Cube pairing" >}}
{{< lazyimg "images/20230527_090845x.jpg" "Configuration menu" >}}
{{< lazyimg "images/20230527_090713x.jpg" "Stats for user" >}}
{{< lazyimg "images/20230527_090732x.jpg" "2D cube sync" >}}
{{< lazyimg "images/20230527_090751x.jpg" "3D cube sync" >}}
{{< lazyimg "images/20230527_090809x.jpg" "Scrambling a smart cube" >}}
{{< lazyimg "images/20230527_090943x.jpg" "Scrambling a regular cube to use a StackMat" >}}
{{< lazyimg "images/20230527_091018x.jpg" "Timing using a StackMat" >}}


## Improvements

So far this has been a hobby project, therefore I don't plan to invest loads of time on it. I would certainly like to **support more cubes**, should not be hard but I first have to get a GoCube or a Moyu in my hands in order to test them. Also, at least GoCube and Giiker have 2x2x2 smart cubes as well. Supporting these will mean some changes have to be done on the storage and UI to **accomodate for other cubes besides 3x3x3** (maybe also for pyraminx, megaminx,... there are no smart versions for these but you could use them with the Stackmat). Talking about the Stackmat, support for it currently relies on SoftwareSerial since the timer uses inverse UART logic. This makes it somewhat slow, probably **adding a UART hardware inverter** and using the available Hardware Serial port will improve this too.

All in all I think it's a great project and I keep improving my times since I use it. My personal best (PB) is now 62 seconds (my daughter's is 42 seconds) and I average (AV5) between 80 and 90 seconds. Again, I'm not a speedcuber!!

If you find the project interesting or want to reproduce it, ping me! I'll be happy to hear your suggestions and help you out.
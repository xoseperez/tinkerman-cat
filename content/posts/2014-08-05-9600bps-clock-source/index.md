---
author: Xose
comments: true
date: 2014-08-05 09:50:33+00:00
layout: post
slug: 9600bps-clock-source
title: "9600bps clock source"
image: "images/2013-04-17-00.09.45_small.jpg"
post_id: 509
categories:
- Learning
tags:
- 74hc590
- crystal
- documentation
- oscillator
- scope
- uart
---

Quick post from an old draft, mainly as documentation.

A 9600Hz oscillator circuit based on a 2.4576MHz crystal and a 74HC590 binary counter. The idea was to reproduce the set up from [robotroom.com site](http://www.robotroom.com/Multimeter-Reviews-4.html) with a bar crystal but I had some trouble making it work. The solution came from this document about [crystal oscillator circuits](http://lorien.die.upm.es/~macias/docencia/datasheets/osc-clock/circuits.pdf) that describes different circuits depending on the crystal frequency. Here you have the schema and a picture of the circuit:

{{< lazyimg "images/schema1.png" "9600Hz oscillator circuit" >}}

{{< lazyimg "images/2013-04-17-00.09.45_small.jpg" "Prototyping it in a breadboard. The DSO Nano is out of focus but shows a 9.60kHz signal." >}}		

---
author: Xose
comments: true
date: 2016-10-05 00:03:08+00:00
layout: post
slug: sonoff-th10-th16-sensors-displays-actuators
title: "Sonoff TH10 & TH16: sensors, displays, actuators,..."
post_id: 1209
image: "images/20161005_005107s-1200x800.jpg"
categories:
- Analysis
- Hacking
tags:
- actuators
- amd2301
- bmp085
- dht22
- ds18b20
- electrodragon
- espurna
- hc-sr04
- itead
- led display
- oled display
- pir
- relay
- sensors
- sonoff
- sonoff dual
- sonoff pow
- sonoff th10
- sonoff th16
- trrs
---

Itead Studio keep on creating interesting products for the hacker community. A few weeks ago a new version of the already classic Sonoff TH came to life. This new version comes in two flavours: the [Sonoff TH10](https://www.itead.cc/smart-home/sonoff-th.html) and [TH16](https://www.itead.cc/smart-home/sonoff-th.html) and you can buy them at Aliexpress: [Sonoff TH 10A/16A Temperature And Humidity Monitoring WiFi Smart Switch](http://s.click.aliexpress.com/e/VneuBYf).

In this article I will briefly talk about what's new in this device to quickly go to explore one of those novelties: it's external interface.

<!-- more -->

## What's new?

The first thing I noticed is it's **size**. The new [Sonoff TH10](https://www.itead.cc/smart-home/sonoff-th.html) (or [TH16](https://www.itead.cc/smart-home/sonoff-th.html), they are the same except for the relay) It's a lot bigger than the old Sonoff TH. The old case is 87.5x38.8x23.5mm while the new one is 114x51.6x32.2mm. It's bigger in every dimension and it they were cubes (they are not) the new Sonoff would be 2.37 times bigger in volume!!

{{< lazyimg "images/20161004_232359s.jpg" "'Classic' Sonoff TH (right) versus the new Sonoff TH10 (left). Does size matter?" >}}

Maybe we can find an explanation for this growth in the inside but my first though is that one of the good things about the Sonoff was it's relatively small size (compared to it's competitor by [Electrodragon](http://www.electrodragon.com/product/wifi-iot-relay-board-based-esp8266/), for instance).

In the inside we have a **completely different PCB layout**. The PCB is also 1.82 times bigger (64.8x33.9mm versus 88.9x45mm) but it certainly breathes. Having all the connectors on the same side reduces trace distance and improves the separation between high and low voltage sides of the board. High voltage traces are a lot thicker and also have an extra of tin on top. All in all it looks like they have tried to answer people concerns about the safety of the device. But they still don't have the CE mark (actually the CE mark in the case means "China Export", check for [the right Conformité Européene mark](http://www.ce-marking.org/what-is-ce-marking.html)).

**UPDATE 2016/10/08**: Itead has contacted me to say that they are working on the CE certification and they expect to have it in no more than a month.

**UPDATE 2016/10/20**: Itead has contacted me again to communicate that some of their Sonoff line has now officially the **CE certification** ([document of the CE certificate for new Sonoff products](https://www.itead.cc/wiki/images/f/f7/CE_Certificate_for_Sonoff_Series.pdf)). The certification covers Sonoff TH10 and TH16 and also Sonoff DUAL and POW. They have also been certified as **RoHS compliant**.

{{< lazyimg "images/20161004_232445s.jpg" "Now the same comparison from the inside..." >}}

The only difference between the [Sonoff TH10](https://www.itead.cc/smart-home/sonoff-th.html) and [TH16](https://www.itead.cc/smart-home/sonoff-th.html) is the **relay**. The TH10 uses a [Hui Ke HK3FF-DC5V-SHG subminiature high power relay](http://www.megasan.com/service/pdfhandler.ashx?fileid=4480) rated 10A while the TH16 uses a [Honfa HF152F-005-1HTQ(104)](http://www.hongfa.com/pro/pdf/HF152F_en.pdf). The old Sonoff TH uses a [HKE HRS3FNH-S-DC5V-A](http://en.hke.cn/bg_attach/2015/07/93.pdf). They are all driven with GPIO12 through a transistor labeled 12W59. Don't know enough about relays to know which one is better and why, what I know that <del>none of them is a SPDT relay</del> actually the Hul Ke is a SPDT relay, but the NC pole is not exposed and connected to the input line.... So **crossover switching is still not possible**...

Then there are these new **push terminals**. I guess they are easy to use while sticking the cable in the hole. Also the designers have added two neutral and two ground terminals so the user does not need a terminal bar to power the device and the load. There is also a very welcome F20AL250V 20A **fuse**.

{{< lazyimg "images/20161004_211907s.jpg" "The new push terminals and the fuse in the Sonoff TH10" >}}

The user interface has also improved with a **new LED** that can be driven with GPIO13 (the other one is tied to GPIO12, so it follows the relay state). The enclosure is thin enough so the LEDs are visible through it. Also the **new button** is much better in terms of usability and aesthetic pleasure. And finally there is the **new external interface**... which I will explore in the next sections.

## The new interface

Using an audio jack as an interface is quite common. I used it in my [Smartmeter pulse counter](/post/smartmeter-pulse-counter-2/) years ago. It's also good news for anyone willing to add their own sensors. But it has two caveats: first they have decided to go for a **2.5mm jack**, which is not as common as the 3.5mm one. Harder to find and more expensive. Second, even thou they are using a **4 channels jack** (or **TRRS** for tip-ring-ring-sleeve) they are only using 3 of them for 3V3, GND and GPIO14. So what happened to the fourth?

{{< lazyimg "images/20161004_220416_LABELSs.jpg" "Named pads, headers and test points in the Sonoff TH10 board" >}}

Well as you can see in the image the engineering team at Itead Studio actually thought about it. Only it's not enabled. In the image above you can see that GPIO14 is tied to the jack tip with a 0Ohm resitor and a 5KOhm pull-up (actually two 10k in parallel). Side by side to this 0Ohm resistor there is a pad ready to connect the first ring in the jack to GPIO4 and another one to add a pull-up too. So you just have to solder a 0Ohm SMD resistor to that pad (the one with the GPIO4 label) to bring out a second IO to connect the sensor...

{{< lazyimg "images/20161004_110845s.jpg" "I don't have 0603 SMD parts (too small for me) but a 0805 resistor fits just fine." >}}

Have you read pull-up resistors? Me too. Do you know any 2 wire protocol that needs pull-ups? Me too!! So now we have a whole bunch of digital sensors we can attach to the [Sonoff TH10](https://www.itead.cc/smart-home/sonoff-th.html)/[TH16](https://www.itead.cc/smart-home/sonoff-th.html).

## A zillion of sensors, actuators, displays...

Itead Studio sells two sensors for the Sonoff TH10/TH16: an [**AMD2301**](https://www.itead.cc/sonoff-th.html) temperature and humidity sensor (DHT22 compatible) and a [**DS18B20**](https://www.itead.cc/sonoff-th.html) waterproof temperature sensor.

{{< lazyimg "images/20161005_010139s.jpg" "Look at the size of that AM2301 enclosure!" >}}

But now we can connect any digital device that requires one or two GPIO pins to drive it. And that includes I2C devices. We still have a limitation: the interface has a 3V3 channel, so we won't be able to power 5V sensors with it... unless we hack that too (cut that thick trace going to the "3V3 / SLEEVE" point and solder a wire to any of the 5V test points in the image).

First I worked my testing cable. A [2.5mm TRRS jack](http://s.click.aliexpress.com/e/mUzjQ3R) [Aliexpress] to 4-channels Dupont connector. You can also purchase a convenient [2.5mm jack to screw terminal block](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338044841&mpre=http%3A%2F%2Fwww.ebay.com%2Fitm%2F5-Pack-2-5mm-TRRS-Male-Jack-to-AV-4-Screw-Terminal-Block-Balun-Connector-%2F391456073249%3Fhash%3Ditem5b2499a621%3Ag%3AqXcAAOSwvU5XNQys) for prototyping.

{{< lazyimg "images/20161004_112007s.jpg" "Sometime ago a friend told me I should always carry a wooden clothes pin with me..." >}}

{{< lazyimg "images/20161004_113538s.jpg" "Now ready to test sensors..." >}}

**UPDATE**: Teun Scheffel from The Netherlands sent me a pic of the jack pin out in combination with a BME280 I2C sensor that makes it clearer what cable goes where. Thanks Teun!

{{< lazyimg "images/pic1.png" "Jack pinout and sensor. Thanks to Teun Scheffel!" >}}

So let's start playing! A [**BMP085**](http://s.click.aliexpress.com/e/iiQ7QrJ) [Aliexpress] pressure sensor, a [**HC-SR04**](http://s.click.aliexpress.com/e/rrfqV3j) [Aliexpress] ultrasonic range module, a **[PIR](http://s.click.aliexpress.com/e/EEIU3JU)** [Aliexpress], an [**I2C LED display**](http://s.click.aliexpress.com/e/7IAYFa2) [Aliexpress] or an nice little **[0.96" I2C OLED display](http://s.click.aliexpress.com/e/EaybyrJ)** [Aliexpress]...

{{< lazyimg "images/20161004_123410s.jpg" "A BMP085 sensor using I2C protocol" >}}

{{< lazyimg "images/20161004_125243s.jpg" "The HC-SR04 ultrasonic range module needs two GPIOs to drive the trigger and echo pins, but it only works with 5V." >}}

{{< lazyimg "images/20161004_131127s.jpg" "This PIR works great with 3V3 and can be driven with just one GPIO. This is actually quite useful for a light switch..." >}}

{{< lazyimg "images/20161004_132844s.jpg" "You will need 5V to drive this I2C LED display..." >}}

{{< lazyimg "images/20161005_005107s-1200x800.jpg" "But this 0.96 inches I2C OLED is a perfect match!" >}}

What else... well, almost anything digital out there: displays, humidity sensors, distance sensors, touch sensors, encoders, magnetometers, gyroscopes, accelerometers, tilt switches, reed switches, real time clocks,... and also servos, external relays, buzzers, and WS2812 strips!

{{< lazyimg "images/20161005_015826s.jpg" "A zillion of sensors AND actuators" >}}

## Flashing it

Last note before letting you go to find something to attach to your new [Sonoff TH10](https://www.itead.cc/smart-home/sonoff-th.html)/[TH16](https://www.itead.cc/smart-home/sonoff-th.html). Tech people at Itead Studio know some of their clients will open the product enclosure and own the device by flashing their own firmware into it. They even use it as an advertising slogan: "**For hackers**, [Sonoff Pow](https://www.itead.cc/sonoff-pow.html) is another exciting board.". So go ahead. The programming header is labelled and goes like this (check the picture above): VCC, RX, TX, GND. The button is tied to GPIO0 so just press it while powering the board and you are in flash mode.

If you don't know what to flash give a try to the [**Espurna firmware**](https://github.com/xoseperez/espurna). It's a work in progress but will soon support I2C devices or two relays (for the Electrodragon or the [Sonoff Dual](https://www.itead.cc/sonoff-dual.html)) in different configuration modes including crossover switching.

By the way: I hope I will have a [Sonoff Pow](https://www.itead.cc/sonoff-pow.html) soon in my hands...		

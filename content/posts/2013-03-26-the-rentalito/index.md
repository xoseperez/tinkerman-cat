---
author: Xose
comments: true
date: 2013-03-26 22:37:02+00:00
layout: post
slug: the-rentalito
title: "The Rentalito"
post_id: 315
image: "images/IMG_20111116_202739.jpg"
categories:
- Projects
tags:
- arduino
- code
- dot matrix display
- ht1632c
- mqtt
- python
- wifly
---

# "Say Hello to The Rentalito!"

That was the first message The Rentalito displayed almost 2 years ago. The Rentalito means something like "small rental monitoring thingy" (free translation from "rental" and "ito", which is the suffix for "small" in Spanish). It was born to display the rentals in a VOD project my team was working at that time. The Rentalito displayed total rentals, rentals by day and it beeped every time a new rental showed up in the database displaying the movie that was just rented.

{{< lazyimg "images/IMG_20111116_202739.jpg" >}}

In the beginning it was funny and kind of a party every time it beeped. The time between beeps was quite spaced but it was just the beginning, you know. But as the project went on and the rentals didn't reach the required pace my team was moved to other projects and shortly after I left the company and The Rentalito came back home with me.

The Rentalito uses two [32x16 RG Dot Matrix display](http://www.sureelectronics.net/goods.php?id=1122)s by SureElectronics. They cost about $32 each on the official store, I bought it on ebay. They use a [Holtek HT1632C driver](http://www.holtek.com.tw/english/docum/consumer/1632c.htm) and there are a bunch of libraries (in [github](https://github.com/search?q=ht1632+sure&type=Repositories&ref=searchresult), in [google code](http://code.google.com/hosting/search?q=ht1632&btn=Search+Projects)) and examples on using these displays with Arduino. I grabbed one that suited my needs (it works, it's easy to use, and has scrolling text functionality) and added a couple of functionalities. You can check it out [here](http://code.google.com/r/xoseperez-ht1632c/source/checkout).

The first version of The Rentalito was based on an Arduino UNO with an Ethernet shield and the display was wired with jumper wires to the shield headers. Quite messy. So every thing was hidden from view with a cardboard (!!) box with a sticker on it designed by the team designer (yeah, it's a VHS video tape). Everything was held together with a sheet of plywood, some screws and spacers.

The code was not better looking but it worked. The Arduino requested the data to a PHP web application that was running on my laptop, it queried the production database to get the latest data and built a message in a proprietary format that was sent back to the Arduino, there it got parsed and displayed. The server application used a SQLite database to store aggregated data and custom messages that could be scheduled based on date and time (like "Good morning" before 10am or "Today is Einar's birthday!!" when it was Einar's birthday). The response could contain more than one message. Each message had title and body (or just body for a full screen message) and some control codes (text color, beep,...). The system worked fine but it was not very flexible.

# The Rentalito goes WIFI

In its second life The Rentalito went WIFI. Using a Roving Networks RN-XV module and a custom Arduino shield that had a male header to plug the cable from the display. Tidier.

{{< lazyimg "images/rentalito.png" "Rentalito Schematics" >}}

Software side I replaced the Ethernet library with [WiFlyHQ](https://github.com/harlequin-tech/WiFlyHQ) by harlequin-tech. The library is really good, easy to use and very configurable but it doesn't implement the Ethernet Client API so it is not a drop-in replacement for the stock Ethernet library.

# The Rentalito goes MQTT!!

The last evolution of The Rentalito has been integrating into my home network as a MQTT client using excellent Nick O'Leary's [PubSubClient](https://github.com/knolleary/pubsubclient) library for Arduino. But to make it work  I had to switch to a WIFI library that uses the Ethernet Client API. After testing some existing libraries I chose [Sparkfun's Wifly Shield library](https://github.com/jcrouchley/WiFly-Shield), basically because it worked.

Although there are several points for improvement in the code (mainly the ht1632c library) the approach is much more flexible now. The main problem is some ugly incompatibility between the ht1632c library and the WiFly library which causes the connection to hang sometimes when a new message arrives while refreshing the display. It's probably due to the fact that I am using SoftwareSerial library instead of hardware serial. Anyway, if not using scrolling messages the display works for hours without problems and even if there is a problem there is code to reconnect and keep going. The code is available on [github](https://github.com/xoseperez/rentalito).

On the outside the new version has also seen some changes. I finally got rid of the wooden sheet and then cardboard box replacing them by two sheets of methacrylate which make it look cooler (my opinion at least) and give it  some support to stand over a flat surface.

{{< lazyimg "images/2013-03-17-14.53.01-small.jpg" "The Rentalito perspective" >}}

{{< lazyimg "images/2013-03-17-14.52.21-small.jpg" "The Rentalito close view" >}}

{{< lazyimg "images/2013-03-19-22.21.49-small.jpg" "The Rentalito displaying the current power consumption" >}}

The Rentalito has suffered different metamorphosis for the past months and I am sure this won't be it's last reincarnation.

		

---
author: Xose
comments: true
date: 2015-10-01 16:16:14+00:00
layout: post
slug: pcb-milling
title: "PCB milling"
post_id: 685
image: "images/20140625_232223x.jpg"
categories:
- Projects
tags:
- ateneu de fabricació de les corts
- atmega328
- attiny85
- bubble display
- cnc mill
- espaisco
- fablab
- laser cutter
- low power
- neil gershenfeld
- veecad
- water consumption
---

it's been a while (ok, more than a whole year) since my last post. I could say I've been busy and it'd be true but I regret myself not writting here for so long... Anyway if you want to know what I've been doing just visit [my family blog](http://sinpiedrasenlosbolsillos.com) (only in spanish, sorry).

{{< lazyimg "images/P1160789.jpg" "Blue footed boobies (yes, I know). Nothing to do with this blog but part of our family trip." >}}

My idea now is to revisit old projects, the ones I've been working on for the last 18 months and even older, and also to write about new projects I'm involved right now. Good bye chronological order.

The months prior to our travel to South America I was working on some collaborative projects related to [Barcelona's network of public fablabs](http://ateneusdefabricacio.barcelona.cat/en/) (named Ateneus), and here I am to share with you one of those projects, not only because I found the initiative interesting and the project worth sharing, but because it was cool to see how a 1.8x3 meters CNC mill (that's 5.9x9.8 feet working area, for you imperialists) will remove the copper from a 2x3cm clad.

{{< lazyimg "images/20140606_192825x.jpg" "Do you see the copper clad? Me neither, but it's there..." >}}

<!-- more -->

## EspaisCO: solutions for real problems

The initiative was the EspaisCO (for CO-working, CO-llaborative, CO-whatever space) where entities and people worked together to find solutions for social problems in the district. And the idea behind our project was to create a family game where players would win tokens for their good energetic behaviour. Actions that give you tokens were things such as don't let the tap open longer than necessary, switch off the lights when you leave the room,... the idea was than everyone in the family would audit other's actions. We found it fun for the kids to "punish" their parents for leaving the fridge door open for too long.

An important part of the game was the documentation (why shoud we ... ?). Then there was a wooden box to hold the instructions and the tokens. The box was designed so it resembled a house (roof, living, kitchen, rooms, ...) with tips on each room and a scoreboard with an awesome gear system that unfortunatelly didn't work as expected.

Designing and making the box was great using the LaserPro laser cutting machine the Ateneu has. But we also wanted to add some electronics to the project so I started designing a couple of pigeons for the fridge and the shower.

The idea was simple: buzz when the fridge door is open for more than, say, 10 seconds, and evaluate the time spent showering with a traffic light (green "ok", orange "not so" and red "you have spent too much water"), then translate this into -negative- tokens on the scoreboard.

## Fridge sensor

Social innovation is great and also is playing with toys. So I used the chance to throw in some new stuff like a [HP QDSP-6064 7 Seven Segment Four Digit Bubble Display](http://www.ebay.com/itm/7-Seven-Segment-Four-Digit-Bubble-Display-HP-QDSP-6064-Arduino-Compatible-/281727791134) and also watching a HUGE CNC mill remove copper from a TINY clad.

{{< lazyimg "images/20140530_011045x.jpg" "Testing the bubble display... ain't it cool?" >}}

As always, code and schematics are open source and you can check them out from the [repo](https://bitbucket.org/xoseperez/espaisco-contador-nevera) at bitbucket (wow, blatant lack of documentation there, got to fix that).

For the fridge sensor I used an overkill atmega328 with plenty of pins to drive the 7-segments and also monitor an LDR to know when the fridge door is open. The schema is really simple and the board layout was thought to be easyly transformed into something the mill would undestand and actually do (ona layer, room between tracks, minimum copper residues).

{{< lazyimg "images/schema_v1.2.png" "Fridge pigeon schema" >}}

{{< lazyimg "images/board_v1.2.png" "Fridge pigeon layout" >}}

The code uses [SevSeg library](http://playground.arduino.cc/Main/SevenSegmentLibrary) so it's really simple to drive the LEDs. I'm using [RocketScream LowPower library](https://github.com/rocketscream/Low-Power) again to sleep the uC until the door opens so the 2 AA batteries that power the thing last for literally an unknown amount of time (ok, I have not measured it, but I'd say it's in the order of years if you do a reasonably use of your fridge).

## Eppur si muove!

Milling out the copper from the clad was awesome. Manu from the Ateneu translated my layout into something the machine would understand and set up the thing. If you step back it was hard to say the drill was moving at all.

{{< lazyimg "images/20140606_185021x.jpg" "Manu getting ready" >}}

{{< lazyimg "images/20140606_191022x.jpg" "And the bit biting the copper" >}}

The final result not only worked, it was nice to see and even funnier to explain.

{{< lazyimg "images/20140625_232223x.jpg" "Back view of the clad with the drilled out tracks" >}}

{{< lazyimg "images/20140625_232159x.jpg" "And the actual thing. The size was such that the 2 AA battery holder will hide behind" >}}

## Shower pigeon

For the shower pigeon I had no time to actually design the board so I used a simple stripboard. We alse designed a metacrylate case with a drop-shaped button that bent just about enough to press the small button inside the case. The result is not really production ready (ehem) but it's 100% functional.

{{< lazyimg "images/shower.png" "The schema for the shower pigeon, done with VeeCAD" >}}

{{< lazyimg "images/20140628_153739x.jpg" "The board" >}}

{{< lazyimg "images/20150716_112445x.jpg" "The shower sensor, water-proof. Notice the drop-shaped plastic button and the 3 holes for the colour LEDs" >}}

The code for the ATTINY85 uses a state machine to track the process. The thing is powered by a CR2032 cell. RealWorld (TM) values on a breadboard had shown that with a 100Ohm resistor the consumption is ~6mA for every LED. That means that a 250mAh CR2032 battery should last around 167 full showers (15 minutes of water flowing). The consumption when in powerdown mode is under my multimeter precission which is around 400 uA. According to the ATTINY85 datasheet, powerdown supply current at 3V and 25C is around 0.2uA (1uA would mean 28 years!). CR2032 are also very good at preventing leakage, tipycally less tha 1pa.

## A public FabLab

Finally, the whole EspaisCO initiative with it's products were presented in the inauguration of the Ateneu de Fabricació de Les Corts, honored by the presence of [Neil Gershenfeld](https://en.wikipedia.org/wiki/Neil_Gershenfeld), MIT professor and founder of the Center for Bits and Atoms, were the FabLab were born.

{{< lazyimg "images/20140702_201038x.jpg" "This guy is quite a celebrity but talking to him feels like you are making him lose his time..." >}}		

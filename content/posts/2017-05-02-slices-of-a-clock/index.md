---
author: Xose
comments: true
date: 2017-05-02 09:33:56+00:00
layout: post
slug: slices-of-a-clock
title: "Slices of a clock"
post_id: 1762
image: "images/20170426_224506s-1200x800.jpg"
categories:
- Code
- Projects
tags:
- adafruit
- ateneus
- atmega328
- binary clock
- clock
- diy
- esp8266
- fablab
- fibonacci
- game of life
- gfx
- github
- laser cut
- led matrix
- mini maker fair
- neomatrix
- ntp
- openscad
- pcb
- seeedstudio
- spirit ge 100w
- wordclock
- ws2812
---

There are so many ways to tell the time. DIYers have been doing clocks since the Ancient Egypt (obelisks lacked portability, thou). Every modern maker has a clock amongst her first projects. I have done some myself, including a fibonacci clock, a [wordclock](/post/wordclock/) with a fancy green matrix effect and an unreleased project that hopefully will see the light someday soon.

But recently I came back to the idea behind the wordclock before, to extend it in different ways:

  * Replace the ATMega328P with an ESP8266 (NTP support and user interaction)
  * Smaller sizes (8x8 LED matrices)
  * Smaller PCB, less buttons
  * Add buzzer for alarms
  * Replace the 3D printed part with a wooden grid cut in laser
  * Completely closed enclosure, better presentation
  * Fix some issues with the original board (like the lack of a beefy capacitor across the LED matrix power lines).

{{< lazyimg "images/20170426_224545s.jpg" >}}

<!-- more -->

## Shrinking the PCB

One of the goals was to be able to create smaller displays using **8x8 LED matrices** instead of the original 16x16. They are much cheaper and almost the same fun. The 16x16 I was using are the flexible type you can find on [Ebay](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338044841&mpre=http%3A%2F%2Fwww.ebay.com%2Fitm%2FWS2812B-WS2811-LED-Digital-Panel-Matrix-Screen-5050-RGB-Addressabl-e-256-Pixels-%2F252402714898) or [Aliexpress](http://s.click.aliexpress.com/e/vFuV72j) for about 40-50€.

The 8x8 ones are also available for 7-9€ at [Ebay](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338044841&mpre=http%3A%2F%2Fwww.ebay.com%2Fitm%2F8x8-64-LED-Matrix-WS2812-LED-5050-RGB-Full-Color-Driver-Board-For-Arduino-%2F301796845648) and [Aliexpress](http://s.click.aliexpress.com/e/FuJiEi6) and have a smaller pitch (8mm versus 10mm for the **16x16 RGB LED matrices**). This allows for a smaller version so I decided to design a PCB with the same features but a 50x50mm size. The controller board sports an **[ESP8266 ESP-12E](http://s.click.aliexpress.com/e/mUZjemI)** [Aliexpress, also on [Ebay](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338044841&mpre=http%3A%2F%2Fwww.ebay.com%2Fitm%2FESP8266-Remote-Serial-Port-WIFI-Transceiver-Wireless-Module-Esp-12F-AP-STA-%2F272408386985%3Fhash%3Ditem3f6cce1da9%3Ag%3ArywAAOSwZJBX-7RV)] and a **[DS1337S RTC](http://s.click.aliexpress.com/e/qbaAUr3)** [Aliexpress and [Ebay](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338044841&mpre=http%3A%2F%2Fwww.ebay.com%2Fitm%2F10-PCS-DS1337S-SOP-8-DS1337-1337-SMD-I2C-Serial-Real-Time-Clock-New-Original-%2F291774646835)]. A buzzer for alarms and connectors for a WS2812 matrix with a beefy capacitor across the power lines.

> The DaClock board is released under the [Creative Commons Attribution-ShareAlike 3.0 Unported License](http://creativecommons.org/licenses/by-sa/3.0/) (CC-BY-SA 3.0) and follows the terms of the [OSHW (Open-source hardware)](http://freedomdefined.org/OSHW) Statement of Principles 1.0. It can be checked out at my **DaClock**** board repository** on Github.

{{< lazyimg "images/daclock-3.0-schema.png" >}}

{{< lazyimg "images/20170430_220914s.jpg" >}}

## SeeedStudio Fusion PCB service

You can use the latest gerblers from the repository or open the project using Eagle +8.0. I sent the gerblers to fab using **[SeeedStudio Fusion PCB](https://www.seeedstudio.com/fusion_pcb.html)** service. It is the first time I use their PCB service but I have already sent a couple more projects to them, including a PCBA service and I plan to do a review about it soon.

Quality of the PCB is really good and price/quality ratio is hard to beat. I like the fact that you are not limited to the green color for the cheapest price tag: green, blue, red, yellow, black, white, they all cost the same and for a standard board under 10x10cm and 2 layers the price starts at USD 4.90 (USD 12.90 lead free) for 10 pieces!!

{{< lazyimg "images/20170426_111523s.jpg" >}}

The slices PCB in black looks awesome. The only small glitch I noticed is a small (0.3mm) shift on the silkscreen. Not critical by any means.

{{< lazyimg "images/20170426_111048s.jpg" "Not the same board but the same Eagle part, the black one from SeeedStudio is slightly shifted towards the top left corner in the picture." >}}

You can check the bill of materials at the [README page in the repo](https://github.com/xoseperez/daclock/blob/master/README.md) for the rest of components.

## Slices

Well, here's where the name comes from.

My [wordclock](/post/wordclock/) project had some very good reviews. People at the Mini Maker Faire in Barcelona last year loved it for the display effects and the overall built. "You should be selling it" was a common advice. The only issue someone reported was that it has an open enclosure (that sounds like an oxymoron). So I started thinking about a case for the electronics to prevent them from gathering dust.

On the other hand I wanted to get rid of the 3D printed part, the grid that serves as an isolator between LEDs. So I tried to laser cut it on a 5mm MDF board and the result is much better. 5mm thickness are enough to allow the light to cover the entire 9x9mm square and it looks nicer (better finished), easier to build because I'd be already using a laser cut for the rest of the pieces and much cheaper: a few cents versus a few euros for the 3D printed part, only in material.

{{< lazyimg "images/20170502_080914s.jpg" "I love the color of MDF after a session of laser cut!" >}}

And to close the box I designed simple hollow layers inspired (again) on the Pibow cases by Pimoroni. The only drawback is that you waste a lot of wood but luckily you can fit 4 layers of the smaller version inside the hollow layers of the 16x16 version since they are smaller than half the length/width (half the LEDs in each direction and smaller pitch).

The end result is a layered, "sliced", box with room for the LED matrix and the electronics. The layers from front to back are:

  * a smoke colored acrylic layer cut with a laser cutter
  * a diffusor, a thin white paper sheet
  * the LED isolator grid, laser cut in MDF
  * the LED matrix
  * LED matrix frame in MDF for the 8x8 version
  * MDF hollow layers to allow room for the electronics
  * the controller
  * MDF again with holes for the power connector and button

{{< lazyimg "images/20170426_124503s.jpg" >}}

{{< lazyimg "images/20170426_140239s.jpg" >}} 

{{< lazyimg "images/20170426_140246s.jpg" >}}

{{< lazyimg "images/20170426_140325s-e1493708893992.jpg" >}}

To cut the layers I have used the **Laser Spirit GE 100W** at the Ateneu de Fabricació de Les Corts, one of the three public fablabs in Barcelona: [**Xarxa d'Ateneus de Fabricació de Barcelona**](http://ateneusdefabricacio.barcelona.cat/).

> The Slices case models (in OpenSCAD format) and the code examples below are released as **free open source software & hardware** under the **GPLv3 licence**. You can checked them out at my [**Slices repository**](http://github.com/xoseperez/slices) on Github.

## Coding clocks and more

Time for the code. I'm using the framework of my [ESPurna project](https://github.com/xoseperez/espurna) to add basic functionality (WIFI, WebServer, NTP, MQTT,...). On top of that I'm adding a matrix manager based on Adafruit's NeoMatrix library and a driver manager. Each "mode" is handled by a "driver". Drivers have common methods like setup, start, stop and loop that the driver manager is responsible to call when necessary.

NOTE: the code is still a work in progress.

### Basic clock

A really basic clock, either static (like in the picture) or scrolling for 8x8 displays. I'm using the built-in font for Adafruit GFX library but I'd really want to use a thicker one (sans bold or similar). So far I have not been able to find or create a font for the GFX library that suits me. I'd like it to be 7 or 8 points height, easy to understand by a kid and with the full character set, including accented characters. If you happen to know of such font for this library, please let me know!

{{< lazyimg "images/20170501_225614s.jpg" >}}

### Wordclock

Not the same wordclock as in my previous project, this one scrolls the hour in a readable text. Currently only in catalan language but, believe me, english or spanish are waaaay easier.

### Fibonacci clock

First thing I thought when I saw this [Kickstarter campaign](https://www.kickstarter.com/projects/basbrun/fibonacci-clock-an-open-source-clock-for-nerds-wit) was "I want one!". Next thing was "F**k! $100". An still the next one: "I could build it myself". Actually, Philippe Chrétien released the code and build instructions in [Instructables](http://www.instructables.com/id/The-Fibonacci-Clock/) even before the Kickstarter campaign, so anyone can build hers. Even if you are not good at electronics, for instance, you can buy certain parts from [Philippe's](http://store.basbrun.com/).

{{< lazyimg "images/20160108_170107s.jpg" >}}

So I built my own. I have never written about it here but here you have a picture:

{{< lazyimg "images/20170428_031306s.jpg" >}}

Instead of CNC'd thick wood I used MDF and a laser cut. The overall finish is good but improvable. Anyway I thought it could be easier to do with the "Slices".

{{< lazyimg "images/20170426_224545s.jpg" "10:28, obviously" >}}

Since the fibonacci clock has a resolution of 5 minutes (you cannot read 19:33, but 19:30 or 19:35) I added the small "dots" at the bottom to account for minutes to add to every 5-minutes block.

Problem here is that you have to know the sizes and positions of each block to read it since they are built from several pixels each. So I cut off several separators from the MDF grid with an [exacto knife](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338044841&mpre=http%3A%2F%2Fwww.ebay.com%2Fitm%2F17-Piece-Hobby-Knife-Set-exacto-style-razor-for-model-making-crafts-etc-Knives-%2F111653288223) [Ebay] to draw the different squares of side 1, 1, 2, 3 and 5 respectively and make it clearer:

{{< lazyimg "images/20170428_031306s.jpg" "2:55, who would have tell?" >}}

### Binary clock

The classical geek clock. Hours, minutes and seconds in a BCD format, each column represents a digit of the HH:MM:SS string in binary.

{{< lazyimg "images/20170426_224506s-1200x800.jpg" "22:27:48, easier then the fibonacci clock, rigth?" >}}

### Game of life

Because why not? New cells are painted green and old ones blue. The game and its inhabitants are mesmerising.

{{< lazyimg "images/20170502_082956s.jpg" >}}

### Drawing canvas

And finally a little game for my daughters, draw something on the computer and see it in the LED matrix. Currently you can only choose the color and paint the pixels one by one. There is no eraser (you can erase the whole matrix instead). After a couple of drawings my eldest daughter wrote a TODO list for me: allow erasing pixels individually, add a color picker and save images so it can show a slideshow of them.

{{< lazyimg "images/20170501_225529s-e1493671637543.jpg" >}}

Any other clocks you can think of that could be represented in a LED matrix? A good thing about the ESP8266 is that I still have plenty of room to add more drivers...		

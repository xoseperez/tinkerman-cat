---
author: Xose
comments: true
date: 2013-02-28 23:09:27+00:00
layout: post
slug: decoding-433mhz-rf-data-from-wireless-switches-the-data
title: "Decoding 433MHz RF data from wireless switches. The data"
post_id: 261
image: "images/4tribits.jpg"
categories:
- Learning
tags:
- 433 mhz rf
---

[UPDATE 2013-03-01] I have added links to the encoder chips these two remote use and removed some miss-assumptions for the Noru remotes. The explanation now is simpler, but the main questions about the Noru codes remain.

In my [previous post](http://tinkerman.eldiariblau.net/decoding-433mhz-rf-data-from-wireless-switches/) I explained how I decoded the data from two wireless outlet remotes using a couple of libraries for Arduino and a Bus Pirate in logic analyser mode. Now I want to go more in-depth, showing the captures from OLS and the data I obtained.

# A bit of theory

Just a little bit. These remotes send the information using a common type of modulation called [Amplitude-Shift Keying (ASK)](http://en.wikipedia.org/wiki/Amplitude-shift_keying). This is the digital equivalent to the [Amplitude Modulation (AM)](http://en.wikipedia.org/wiki/Amplitude_modulation) used in long distance radio broadcast. The information is encoded as an increase or decrease in the amplitude of a carrier wave of (in this case) 433.92MHz. The information itself is sent at lower frequencies (some tens of KHz).

Since ASK is meant for digital communications, the information itself is encoded as a stream of 0's and 1's. A pattern of 0's and 1's define a "state". The remotes like the Avidsen I have use 3 different states, code bits or tribits. They all begin with a high pulse (a 1) and end with a low pulse (a 0). The next image shows the pattern for these 3 states:

{{< lazyimg "images/3tribits1.jpg" >}}

Note that you can speak these patterns in different ways: as a succession of pulse lengths, like "short long short long" or "1313" (the long pulses are three times longer that the short ones) for the tri-bit 0, or as a tuple of bits like "00" (1 for a long high, 0 for a short high).

Now, the full signal is a series of code bits, a code word. 12 for the two remotes I have checked. These 12 code bits can be writen down as "11111FF0FFF0" (this is the ON command for outlet C in channel 31 for the Avidsen remotes).

Since the tribits are, well, 3-state bits, you can think of the previous code as a base-3 number, where the 'F' is a '2': 111112202220. the RemoteSwitch library has a method that accepts the decimal equivalent for this base-3 number, with in this case is (read back-wards, the least significative bit is the rightmost one):

```
0*3^0 + 2*3^1 + 2*3^2 + 2*3^3 +
0*3^4 + 2*3^5 + 2*3^6 + 1*3^7 +
1*3^8 + 1*3^9 + 1*3^10 + 1*3^11 = 266649
```

But this number is just a convenience and we are moving away from the actual signal representation.

# Avidsen remote

These remotes use the PT2262 parallel to serial encoder ([datasheet](http://www.instructables.com/files/orig/FT7/GCJB/GFRWRWM6/FT7GCJBGFRWRWM6.pdf)). These chips have 12 parallel input lines you can individually set to HIGH, LOW o leave them floating for the three different states or tri-bits. When the input lines are set you assert low the TE pin and the data is encoded and output through the DOUT pin. They are supported by existing Arduino libraries (more or less, see the patch for the RCSwitch library in my [previous post](http://tinkerman.eldiariblau.net/decoding-433mhz-rf-data-from-wireless-switches/)). The code word is a stream of 12 tribits arranged this way:

	
  * The first set of 5 are the channel (the one you configure in the dip switch of the remotes and the outlets). This is a value from 0 to 31 which is actually encoded in binary where the tribit 1 represents a 1 and the tribit F represents a 0.

	
  * The second set of 5 tribits is the outlet identification you configure in the second set of 5 dip switches of each outlet. All the tribits in this set must be set to F except for one, which represents the outlet selection in the remote from A to E.

	
  * The last set of 2 tribits represent the action, and it can be "F0" for ON, or "0F" for OFF.

Now, some captures.

{{< lazyimg "images/00A1-codes.jpg" "Channel 00, Outlet A, ON - FFFFF0FFFFF0" >}}

{{< lazyimg "images/31C1-codes.jpg" "Channel 31, Outlet C ON - 11111FF0FFF0" >}}

{{< lazyimg "images/31A0-cropped.jpg" "Channel 31, Outlet A OFF - 111110FFFF0F" >}}

[
]images/31C1-codes.jpg)With these information you can actually control up to 32*5=160 different outlets from your favourite microcontroller.

# Noru remote

This remote uses the HS1527 programmable encoder ([datasheet](http://sc-tech.cn/en/HS1527.pdf)). These chips have a hardcoded preamble of 20 bits (normal 0/1 bits) and 4 data lines to set 4 data bits. The total length of the message is the 24 bits. Mind these are normal bits: 0 is encoded as a short high pulse followed by a long low one whilst 1 is encoded as a long high pulse followed by a short low one.

The message length is thus the same as in the previous remote but it is not 100% compatible with the Arduino libraries since you can encode a forth combination of highs and lows, a fourth state or tetra-bit mimicking the naming convention:

{{< lazyimg "images/4tribits.jpg" >}}

The codes for the 14 buttons of the Noru remote are (in binary format and tetra-bit format):

|Button|Binary|Tetrabits|
|---|---|---|
|ON1|110100000000001101011000|1F000001FFX0|
|OFF1|110100000000001101010110|1F000001FFFX|
|SET1|110100000000001101011101|1F000001FF1F|
|ON2|110100000000001101011100|1F000001FF10|
|OFF2|110100000000001101010100|1F000001FFF0|
|SET2|110100000000001101011011|1F000001FFX1|
|ON3|110100000000001101011010|1F000001FFXX|
|OFF3|110100000000001101010010|1F000001FF0X|
|SET3|110100000000001101011110|1F000001FF1X|
|1H|110100000000001101011001|1F000001FFXF|
|2H|110100000000001101010111|1F000001FFF1|
|4H|110100000000001101010011|1F000001FF01|
|8H|110100000000001101010001|1F000001FF0F|
|ALL OFF|110100000000001101010101|1F000001FFFF|

As you can see the first 20 bits are always de same. It's the identification hardcoded in the HS1527. Only the four rightmost bits change. There are 16 possible combinations and 14 are being used by the 14 buttons (only the 00 and 11 endings are missing from the table). But, is there any pattern in this 4 bits?

There are some clues to start working with:

	
  * Some codes are common for all the outlets (the timeouts and the ALL OFF button)

	
  * Some codes are specific for a certain outlet (ON, OFF and SET)

	
  * When you start using an outlet you first have to set it in learning mode and press the ON button in the remote for the number you want to assign to it. From then on the outlet will start responding to that button, but also to the OFF and SET buttons for the same number. So there has to be some pattern that links ON, OFF and SET buttons for the same outlet.

	
  * According to the documentation an outlet can learn more than one code. The same code can be linked to more than one outlet and one outlet can respond to more than one code. I don't know if this is somewhat useful to figure out the pattern but it's cool!

Some captures for a couple of Nero remote signals and enough for now.

{{< lazyimg "images/ON1-codes.jpg" "Outlet 1 ON - 1F000001FFX0" >}}

{{< lazyimg "images/SET2-cropped.jpg" "Outlet 2 SET - 1F000001FFX1" >}}

		

---
author: Xose
comments: true
date: 2013-01-15 23:41:56+00:00
layout: post
slug: smartmeter-pulse-counter-4
title: "Smartmeter pulse counter (4)"
post_id: 155
image: "images/schematic1.png"
categories:
- Code
- Projects
tags:
- arduino
- cosm
- xbee
---

This is going to be the last post for the smart meter pulse counter setup series. I want to wrap up several things like the final hardware, the code and the data visualization.

## Final hardware

This is what the pulse counter sensor looks like, almost. The final version that's already "in production" has a switch to hard-reset the radio from outside the enclosure. Nothing special otherwise. Everything goes in a socket so I could reuse the components, the photocell probe connects to the 3.5mm jack that's on the top of the box and I'm using 3 alkaline AA batteries (not the rechargeable ones in the picture).

{{< lazyimg "images/2012-11-27-02.03.58-small.jpg" >}}

## Code

The code is freely available under GPLv3 license [on github](https://github.com/xoseperez/pulsecounter). The code itself is pretty simple: it uses the [Arduino LowPower library](http://www.rocketscream.com/blog/2011/07/04/lightweight-low-power-arduino-library/) by RocketScream to keep the arduino sleeping for most of the time. It only wakes on an event on any of the two possible interrupt pins:

```
void setup() {

pinMode(LDR_INTERRUPT_PIN, INPUT);
pinMode(XBEE_INTERRUPT_PIN, INPUT);
pinMode(XBEE_SLEEP_PIN, OUTPUT);

Serial.begin(9600);

// Using the ADC against internal 1V1 reference for battery monitoring
analogReference(INTERNAL);

// Send welcome message
sendStatus();

// Allow pulse to trigger interrupt on rising
attachInterrupt(LDR_INTERRUPT, pulse, RISING);

// Enable interrupt on xbee awaking
attachInterrupt(XBEE_INTERRUPT, xbee_awake, FALLING);

}
```

The LDR_INTERRUPT pin is there the photocell based voltage divider is plugged to. When the photocell resistance drops due to a light pulse the pin sees a RISING transition and the Arduino counts the pulse. The XBEE_INTERRUPT pin is connected to the ON_SLEEP pin of the XBee (pin 13). When the XBee is sleeping this pin is pulled high and when it awakes the pin goes low and the Arduino sends the message.

```
void pulse() {
  ++pulses;
}
void xbee_awake() {
  ready_to_send = true;
}
```

On the main loop the arduino sleeps until an event awakes it. If the event has been triggered by the XBee then it calls the message sending methods.

```
void loop() {

// Enter power down state with ADC and BOD module disabled
LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);

// Check if I have to send a report
if (ready_to_send) {
ready_to_send = false;
sendAll();
}

}
```

## Results

The messages are being received by an XBee coordinator radio that's connected to an USB port in my home server. On the server my [xbee2mqtt daemon](http://tinkerman.eldiariblau.net/xbee-to-mqtt-gateway/) is running listening to incoming messages from the radio port. The messages are mapped to MQTT topics (namely /benavent/general/power and /benavent/powermeter/sensor/battery).

The the <del>mqtt2cosm</del> mqtt2cloud daemon (I will write a post about this one soon) pushes the data to Cosm.com or Tempo-db.com.

## First issues

The pulse counter has been running for some days now and the first issue has arised. You may notice in the previous graph that from time to time the sensor stops reporting data for several minutes. I still have to find out what's going wrong but my guess is that there is some issue with the interrupts and the transmissions. I am not disabling interrupts while transmitting because I thought it was not necessary when using the hardware UART, but maybe I was wrong.

The problem doesn't seem to be related to the time of day, the power measure and in the tests I did while testing the XBee sleep cycle it did not happen (the probe was not plugged in so there were no additional interrupts...). The distance to the coordinator radio was one of the problem generation candidates in my first tests but now I am testing another sensor that's just one meter apart from the pulse counter and it reports flawlessly...

Any suggestions?		

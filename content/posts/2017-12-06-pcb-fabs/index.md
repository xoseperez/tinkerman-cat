---
author: Xose
comments: true
date: 2017-12-06 13:05:16+00:00
layout: post
slug: pcb-fabs
title: "PCB fabs"
post_id: 2168
image: "images/20171112_185649s-1200x800.jpg"
categories:
- Analysis
tags:
- assembly
- bga
- eagle
- enig
- fab
- kicad
- osh park
- pcb
- pcba
- seeedstudio
- smart-prototyping
- vinegar
- welpcb
---

A few years ago (not many) I used to burn copper plates using [acetic acid, a.k.a. vinegar](/post/pcb-etching-at-home-using-vinegar/). I was somewhat concerned about using stronger acids so it was OK to use another acid, even if it was soooo sloooow. If you were patient you could get to have decent boards using 50mil traces (or even thinner). But it required keeping a good temperature on the copper bath and regulating the ratio vinegar/hydrogen peroxide continuously, adding a little salt from time to time to speed things up.

{{< lazyimg "images/2014-04-11-21.09.46.jpg" "The vinegar biting the copper" >}}

One day I saw an article about cheap Chinese PCB fabs (I think it was DirtyPCB by Dangerous Prototypes) and I decided to design my own board and send it to fab. I used Eagle (I still use it although I'm learning KICAD) and the learning process was significant. But at the end I managed to get something. I learned to use copper pours, to correctly label the board, to create my own parts, to avoid auto-route, to use design rules and create gerbers,...

{{< lazyimg "images/20171112_185919s.jpg" >}}

And over time I have tested different fabs. I'm not an expert by any means but I wanted to write a bit about them here. The basic order I usually do is a 10 units, under 50x50mm board, 1.6mm thick, 1oz copper and HASL Lead Free due to RoHS rules in the EU. The prices and options below are based on these settings.

One more thing. I reckon these suppliers are good enough in most cases for small batches, testing boards or DIY projects. Some EE are concerned about the quality of the boards and they prefer EU-based (or US-based) fabs. I cannot offer a good reason to use these manufacturers here or to not use them in professional/industrial projects.

<!-- more -->

## Smart Prototyping

[Smart Prototyping](https://www.smart-prototyping.com/) (China) was the first fab I used. It offers affordable prices if you stick to their green PCBs. $9.9 for 10 boards is great. If you want to go red, blue, white... they will cost you $17.90. Uncommon colors like purple could go as high as $45 for 10 units. ENIG means $11 extra and 2oz copper is more than $30 more. Of course this is still pretty cheap.

{{< lazyimg "images/PCB-Prototyping-Smart-Prototyping.png" >}}

I have never had problems with the quality of the boards. And if I had problems with the boards it has always been a design error. They add an internal order number on the boards that can be annoying.

Their green PCBs are somewhat matte. You might want to ensure you have proper space around drills. The milled slots or cut-outs are kind of rough on the edges (but that's not an issue) and the silkscreen looks like melted sugar (does anyone know the name of this technique?).

{{< lazyimg "images/smartprototyping-espurna-h-0.6.20170212_01.jpg" >}} 

{{< lazyimg "images/smartprototyping-espurna-h-0.6.20170212_02.jpg" >}} 

{{< lazyimg "images/smartprototyping-espurna-h-0.6.20170212_03.jpg" >}} 

{{< lazyimg "images/smartprototyping-espurna-h-0.6.20170212_04.jpg" >}} 

{{< lazyimg "images/smartprototyping-espurna-h-0.6.20170212_05.jpg" >}} 

{{< lazyimg "images/smartprototyping-espurna-h-0.6.20170212_06.jpg" >}} 

{{< lazyimg "images/smartprototyping-espurna-h-0.6.20170212_07.jpg" >}} 

{{< lazyimg "images/smartprototyping-espurna-h-0.6.20170212_08.jpg" >}}

Smart Prototyping offers stencils, PCB assembly and parts sourcing as well, but I have not used these products so I can't tell.

## SeeedStudio

[SeeedStudio](https://www.seeedstudio.com/) (China, with offices in Japan and the USA) is very well known for it's bazaar, a place where you can find lots of low and high tech products for your next project. But also has the Fusion service, where they provide affordable PCB prototyping as well as PCBA (Assembly) services.

{{< lazyimg "images/Fusion-PCB-Manufacturing-Prototype-PCB-Assembly-Seeed-Studio.png" >}}

Price is a bit higher than with Smart Prototyping but they do not charge extra for color PCBs, so if you want something different from green Seeed's options is cheaper. ENIG will cost you $15 extra and 2oz copper will cost you more than $30 extra.

A good thing for those of us to whom double or triple checking is never enough is that it has a gerbers viewer where you can see how the board will look like. I have gone back to the CAD a lot of times after checking the visual aspect of the board.

The milling is cleaner than with Smart Prototype service. The silkscreen is dotted and that means characters look crispier but sometimes lines look pixelated or having different widths along the line. Drill holes have a fair margin around without copper, silkscreen or any other layer, just silicon. The overall feeling is really good.

{{< lazyimg "images/seeedstudio-espurna-hlv-0.7.20170914_02.jpg" >}} 

{{< lazyimg "images/seeedstudio-espurna-hlv-0.7.20170914_04.jpg" >}} 

{{< lazyimg "images/seeedstudio-espurna-hlv-0.7.20170914_05.jpg" >}} 

{{< lazyimg "images/seeedstudio-espurna-hlv-0.7.20170914_06.jpg" >}} 

{{< lazyimg "images/seeedstudio-espurna-hlv-0.7.20170914_08.jpg" >}} 

{{< lazyimg "images/seeedstudio-espurna-hlv-0.7.20170914_07.jpg" >}} 

{{< lazyimg "images/seeedstudio-espurna-hlv-0.7.20170914_03.jpg" >}}

SeeedStudio's PCBA service is so easy to use! It doesn't have a fixed starting cost as with Smart Prototype's one and you can get a online quote if you only use parts from their OPL (Open Parts Library) which has 600 common parts. They source the part and assembly it. You can always use a part using it's manufacturer reference but then I think they will quote your order manually. I have used it to populate hard to hand-solder BGA packages and it's worth it. Well basically sometimes it's the difference between being able to fulfill the project or not being able to. Highly recommended.

## WellPCB

[WelPCB](https://www.wellpcb.com) (China and Australia) is a new kid on the block (at least from my point of view). Unless Smart Prototyping or Seeedstudio they focus on the PCB fab service, they do not sell parts, modules, or whatever else.

**Update**: Howie Liu from WellPCB has contected me to remark that even thou they specialize in PCB Prototype services, they can also offer PCB Production, Component Procurement, PCB Assembly. They work with different component suppliers like Digi-key, Mouser and Element14 for small quantity orders. Arrow, Avnet and Future for mass production.

**Promotion**: They are also running a Christmas Promotion until December 24th: only $3.99 for PCB Prototype (≤100m*100m, 1-2layers, 10pieces). Not bad, right?

Price for 10 units (50x50mm, 1.6mm thick, 1oz copper, HASL Lead Free) is slightly higher than Seeed's, but again they let you choose the board color without extra charge so (unless you want it green) it's cheaper than Smart Prototype. One think you can do is choose the silkscreen color between white and black, whilst the others use a fixed color. You can get immersion gold finish for $20 or 2oz copper for just $10 extra. That's a lot cheaper than the other two and worth it if your board uses high current traces.

{{< lazyimg "images/PCB-Online-Quote-WELLPCB.png" >}}

The boards look and feel are very similar to those by Smart Prototypes. Their green color is more vivid, the milling is as good as with Seeedstudio and their silkscreen is thinner (makes the Smart Prototyping silkscreen look like a bold font). I have noticed a few imperfections on the board (look the pics below). I don't know the reason or if it was a defective manufacturing but any way it's just make up, not an issue.

{{< lazyimg "images/wellpcb-espurna-h-0.7.20170913_01.jpg" >}} 

{{< lazyimg "images/wellpcb-espurna-h-0.7.20170913_02.jpg" >}} 

{{< lazyimg "images/wellpcb-espurna-h-0.7.20170913_04.jpg" >}} 

{{< lazyimg "images/wellpcb-espurna-h-0.7.20170913_05.jpg" >}} 

{{< lazyimg "images/wellpcb-espurna-h-0.7.20170913_06.jpg" >}} 

{{< lazyimg "images/wellpcb-espurna-h-0.7.20170913_08.jpg" >}} 

{{< lazyimg "images/wellpcb-espurna-h-0.7.20170913_07.jpg" >}}

WellPCB also offers PCB assembly service but it's not integrated in their web page so you'll need an offline quote.

There was one thing that really make it stood over the others. Along with the PCBs I received a 4-pages quality report (Final Inspection Report, E-Test Report and Microsection Analysis Report) with a lot of interesting info. Just for this it is worth testing their service.

{{< lazyimg "images/wellpcb_report_1s.jpg" >}} 

{{< lazyimg "images/wellpcb_report_2s.jpg" >}} 

{{< lazyimg "images/wellpcb_report_3s.jpg" >}} 

{{< lazyimg "images/wellpcb_report_4s.jpg" >}}

## OSH Park

[OSH Park](https://oshpark.com/) (USA) is the only service in this post that it's not located in China. I don't have fancy pictures to show you. I primary use their service for small boards and very small batches (3 units). Since the final price can be as low as $5.

**Update**: Actually, as some user have reported, there is no minimum-price, it all depends on the board size so you can get a price tag of just $1.5 for 3 units of a 0.5" square board.

You don't have many options. Actually you barely have any option. Their boards are purple (they are very well known for this) and gold plated. Always. You can choose 2oz copper at expense of 0.8mm thickness but for the same price. And "swift service". And that's it.

{{< lazyimg "images/OSH-Park-Cart.png" >}}

They claim to be able to do 6mil traces but I have had problems with that in the past. I'm not saying they have a bad quality. It's just meant for a different user target.

{{< lazyimg "images/20170522_231152s.jpg" >}}

One thing I don't like about OSH Park is that they say they are focused on building a community around OSHW projects. That's great, but the tools they offer are meant to buy them boards designed and shared by other users. You don't have any way to share schematics, layouts, BOM, whatever that would be necessary to publish a truly OS hardware project. You only have a title and a description field.

Overall, it's a useful platform to let other users have one or two or three copies of your board (I have used it myself for this reason) but this is not open sourcing your hardware.

## Conclusions

Smart Prototyping, SeeedStudio and WellPCB offer quality at a good price for your project, either if it's a DIY/maker project or an early stage prototype. Seeedstudio is the one offering a more complete online experience for PCB and PCBA services. WellPCB looks like being the more professional-oriented option. Either one of these two are worth giving them a the chance to become your official PCB fab.

Just go ahead and start designing your PCBs with your logo on them!		

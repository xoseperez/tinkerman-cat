---
title: "Grove AI Hat, edge computing in your hands"
date: 2019-05-14T09:34:00+02:00
post_id: 20190514
author: Xose
drafts: true
slug: grove-ai-hat-edge-computing-in-your-hands
comments: true
layout: post
image: "images/grove-ai-hat.png"
description: "Edge computing is about moving the intelligence closer to your data, an that is starting to make a lot of sense at home."
categories:
- Analysis
tags:
- seeedstudio
- edge computing
- risc-v
- cloud
- ai
- artificial intelligence
- nvidia
- jetson
- sipeed
- maix bit
- raspberry pi
- grove
- hat
- kendryte
- k210
- esp8285
- arduino
- micropython
- ai vision
- voice recognition
---

The industry has been talking about **edge computing** for some time now. The idea is to distribute the data storage and intelligence in locations closer to where they are needed and only use external services when absolutely necessary. Centralized cloud services have a series of drawbacks for the IIoT world, being maybe latency and security the bigger ones. Some (a lot of?) companies do not want their data to go outside their facilities and critical systems cannot rely on Internet connection. 

But this has been an industry only "thing" until recently. The rise of affordable and powerful SBC and AI engines have led to the appearance of interesting edge computing solutions for under USD100, something you can play with at home.

<!-- more -->

> Disclaimer: SeeedStudio has contacted me to review the [Grove AI Hat for Edge Computing](https://www.seeedstudio.com/Grove-AI-HAT-for-Edge-Computing-p-4026.html). This is a preliminary post to warm up engines while waiting for the item to arrive. I will write a hands on as soons as I play a little bit with it. Stay tunned.

## Bringing back my data

We are using cloud-based services more and more. It's mostly OK as long as we are aware of it. As long as we are fully informed on how is our data being used. But that doesn't happen often, right? We don't really read the terms and conditions for the services we actually use, do we? We have heard about the voice surveying policies that Amazon uses with their Alexa service, but some of us have just accepted them for the sake of usefulness.

In a scenario where data is the new oil, we should really care for where our personal data goes. And having the power to actually move in some of this external services is good news.

There is already some interesting hardware in the market. Some are standalone boards loaded with the ability to do the heavy lifting just on the edge of your home network, like the [Nvidia Jetson Nano Dev Kit](https://www.seeedstudio.com/NVIDIA-Jetson-Nano-Development-Kit-p-2916.html) [SeeedStudio] or the [Coral Dev Board](https://www.seeedstudio.com/catalog/product/view/id/2900/s/Coral-Dev-Board-p-2900/category/1234/) [SeeedStudio]. Other are add-ons for popular single board computers like MIC arrays for the Raspberry Pi or computer vision modules.

Hardware is actually evolving so quickly that we can see small footprint modules with amazing capabilities like the [Sipeed MAix BiT](https://www.seeedstudio.com/Artificial-Intelligence-c-1234/Sipeed-MAix-BiT-for-RISC-V-AI-IoT-1-p-2873.html) [SeeedStudio] with a RISC-V MCU inside. And if you are already using a Raspberry Pi for you local data processing (like I used to do until recently) then the Grove AI Hat might be an interesting option...

{{< lazyimg "images/maix-bit-suit.jpg" "Sipeed MAix BiT Suit" >}}

## Grove AI Hat for Edge Computing 

The [Grove AI Hat](https://www.seeedstudio.com/Grove-AI-HAT-for-Edge-Computing-p-4026.html) is a Raspberry Pi Hat that provides powerful AI processing capabilities to the Raspberry Pi ecosystem. It can also work as an independent board (without a Raspberry Pi).

{{< lazyimg "images/grove-ai-hat-front.png" "Grove AI Hat for Edge Computing" >}}

{{< lazyimg "images/grove-ai-hat-pinout.jpg" "Grove AI Hat for Edge Computing" >}}

As you can see the board is meant to interface with the hundreds of Grove sensors that SeeedStudio have on their catalogue. It also has the connectors for an LCD display and a camera module. A USB-C connector for power and flashing, and onboard accelerometer (robot, anyone?) and a MIC for voice recognition are also packed in.

Even thou the Grove AI Hat does not have WiFi connectivity on its own, you can of course use a Raspberry Pi 3 with WiFi and Ethernet along with any of the already existing options you can find for that platform. Check the [SeeedStudio Raspberry Pi section](https://www.seeedstudio.com/category/Raspberry-pi-c-1010.html) for ideas.

### RISC-V processor

The board brains is a **Sipeed MAix M1 AI Module** with **Kendryte K210** processor. The K210 is one of the first RISC-V 64bits processors out there. A 400MHz (and up to 800MHz) dual core with 8Mb high-speed SRAM, a **Neural Network Processor** (KPU), **Audio Processor** (APU), **Field Programmable IO Array** (FPIOA), independent **FFT accelerator**, **hardware AES and SHA256**, DVP and LCD interfaces and a bunch of prepherials (I2C, UART, SPI, I2S, PWM, GPIO,...). The M1 module adds 3 channels DC-DC, flash memory and (some versions, not the one in the grove hat) WiFi by using an **ESP8285** MCU. Everything for under USD9 and 0.3W. 

{{< lazyimg "images/sipeed-m1-risc-v-ai-module.jpg" "Sipeed M1 RISC-V AI Module" >}}

{{< lazyimg "images/sipeed-m1-wifi-pin.jpg" "Sipeed M1 RISC-V AI Module Inside" >}}

You can find the K210 and the M1 module in a bunch of products from Sipeed and on sale on SeeedStudio. Sipeed launched an Indiegogo campaing a year ago with several products at the same time (the MAix BiT, a MAix Go Suite very similar to the Grove Hat, and addon boards) and they are now finishing the delivery phase (not without issues). 

### Arduino? Python?

Do you need it simpler? The K210 is a powerful processor and you can use [Kendryte K210 standalone SDK](https://github.com/kendryte/kendryte-standalone-sdk) to program it. But in addition, SeeedStudio is working on an [Arduino Core port for the K210](https://github.com/Seeed-Studio/ArduinoCore-k210) that will let you use the Arduino framework (and IDE!) to flash the K210. Curiously, Sipeed is also working on a [Arduino port on MAix board](https://github.com/sipeed/Maixduino). Maybe they should try to unify resources... Sipeed is also developing a [MicroPython port for the K210](https://github.com/sipeed/MaixPy).

Looking at the repositories for these projects, my guess is that they are still a work in progress. There are one-two developers working on each of them and you will probably miss some features and stumble on a few bugs yet. The SDK, based on FreeRTOS, looks a bit more mature.

## Conclusions

With the Grove AI Hat there is no reason to not starting investigating on Edge Computing solutions for home or small offices. AI vision can provide an interesting range of options from playing your favourite song when you get home to helping impaired persons using home domotics. Voice recognition in the edge will remove the need to use not really respectful external services.

It's time to play on the edge!
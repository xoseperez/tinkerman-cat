---
author: Xose
comments: true
date: 2016-11-21 00:13:08+00:00
layout: post
slug: emulate-wemo-device-esp8266
title: "Emulate a WeMo device with ESP8266"
post_id: 1329
image: "images/esp8266_inside.png"
categories:
- Code
- Hacking
tags:
- alexa
- amazon dot
- belkin
- emulate
- esp8266
- espasynctcp
- espasyncudp
- espurna
- fauxmo
- mqtt
- platformio
- sonoff
- wemo
---

My daughters love to talk to (or with) my [**Amazon Dot**](http://amzn.to/2fy3DMv) [Amazon US] in their funny English: "Alexa, hello!", "Alexa,  li-on!" (actually "light on"). It's so easy to use it to switch on/off things at home using the [**fauxmo** python script](https://github.com/makermusings/fauxmo) by Maker Musings. In his post about [Amazon Echo and Home Automation](http://www.makermusings.com/2015/07/13/amazon-echo-and-home-automation/) more than a year ago he explains how he reverse-engineered the protocol of the **WeMo** switches that **Alexa** ([Amazon Echo](http://amzn.to/2gn05u7) [Amazon US] or[ Amazon Dot](http://amzn.to/2fy3DMv) [Amazon US]) supports.

I also have a server running the fauxmo script with an **MQTT** handler to control some of the **Sonoffs** I have at home, but this morning I woke up thinking: why should I use an external script to control my devices if I can code it in the firmware?

<!-- more -->

## The fauxmoESP library

You will never be the first. Aruna Tennakoon [had already done it](https://github.com/kakopappa/arduino-esp8266-alexa-wemo-switch) a few months ago. But his approach did not suit me. I wanted something that could be easily embedded into an existing solution (like my [ESPurna firmware](https://github.com/xoseperez/espurna)). So I've coded it into a library I could include in my other projects and have it run in 3 lines of code. Literally.

Lately I have been migrating ESPurna to the state-of-the-art [ESPAsyncWebServer](https://github.com/me-no-dev/ESPAsyncWebServer) by core developer Hristo Gochkov (@me-no-dev). So I decided to go for his suit of asynchronous TCP and UDP libraries for this project (note: finally the ESPAsyncUDP library has been removed and its using the WiFiUdp instead). The result is a fast, sturdy library that is amazingly easy to use.

The result is the **fauxmoESP** library, named after Musings' code.

> The fauxmoESP library for ESP8266 is released as **free open software **and can be checked out at my [**fauxmoESP**](https://bitbucket.org/xoseperez/fauxmoesp)** repository **on Bitbucket.

## Installing and compiling it

**Instructions here have been updated for version 2.0 of the library.**

### Using PlatformIO

As I said, the fauxmoESP library depends on [ESPAsyncTCP](https://github.com/me-no-dev/ESPAsyncTCP) <del>and [ESPAsyncUDP](https://github.com/me-no-dev/ESPAsyncUDP)</del> libraries by Gochkov. You will need those first in order to compile it. <del>You will also need the latest Arduino ESP8266 Core installation, at least from after July 11 2016. This is required to join the multicast group where the WeMo app (or Alexa) broadcast a message when searching for compatible devices.</del>

You will need to install ESPAsyncTCP via the lib_deps option in the platformio.ini file or by typing:
    
```
pio lib install ESPAsyncTCP
```
    
### Using Arduino IDE

Same applies to the Arduino IDE. You will need to use the development version of the ESP8266 Arduino Core. Steps to use the library are:

  1. <del>Install the latest ESP8266 Arduino Core using the instructions here: [https://github.com/esp8266/Arduino#using-git-version](https://github.com/esp8266/Arduino#using-git-version) (remove before the stable version from your Boards Manager if any).</del>
  2. Copy or checkout the [ESPAsyncTCP](https://github.com/me-no-dev/ESPAsyncTCP) <del>and [ESPAsyncUDP](https://github.com/me-no-dev/ESPAsyncUDP)</del> libraries in your arduino/libraries folder, it should be under "My Documents/Arduino/libraries" in Windows or "Documents/Arduino/libraries" in Mac or Linux unless you have placed it somewhere else.
  3. Same for the [fauxmoESP](https://bitbucket.org/xoseperez/fauxmoesp) library, check it out in the arduino/libraries folder.
  4. Restart your Arduino IDE
  5. Look for the fauxmoESP_Basic example under File > Examples > fauxmoESP > ...
  6. Choose your board and compile.

## Using it

Include the library, instantiate an object, <del>set the device name</del> define your devices and the message callback and you are done. It can't be easier. Since all the networking stuff is asynchronous there is no need to manually poll for new messages. If you are using [Amazon Echo](http://amzn.to/2gn05u7) [Amazon US] or [Dot](http://amzn.to/2fy3DMv) [Amazon US], once the device is running click on "Discover devices" from the "Smart Home" tab and your device name will be added to the list, now just say "Alexa, turn on/off" and enjoy!

You can change the device name on the fly and re-scan for new devices, the old one will be replaced.

The library allows you to define more than one device to be discovered and then perform different actions depending on which one was triggered. This was a suggestion by user Dave Myers in the comments below and has meant a change in the library API. Version 2.0 includes some discovery optimizations and has some API changes. The example below is taken from the 2.0.0 examples. As you can see the library requires a minimum setup.

```
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include "fauxmoESP.h"
#include "credentials.h"

#define SERIAL_BAUDRATE                 115200

fauxmoESP fauxmo;

// -----------------------------------------------------------------------------
// Wifi
// -----------------------------------------------------------------------------

void wifiSetup() {

    // Set WIFI module to STA mode
    WiFi.mode(WIFI_STA);

    // Connect
    Serial.printf("[WIFI] Connecting to %s ", WIFI_SSID);
    WiFi.begin(WIFI_SSID, WIFI_PASS);

    // Wait
    while (WiFi.status() != WL_CONNECTED) {
        Serial.print(".");
        delay(100);
    }
    Serial.println();

    // Connected!
    Serial.printf("[WIFI] STATION Mode, SSID: %s, IP address: %s\n", WiFi.SSID().c_str(), WiFi.localIP().toString().c_str());

}

void setup() {

    // Init serial port and clean garbage
    Serial.begin(SERIAL_BAUDRATE);
    Serial.println();
    Serial.println();

    // Wifi
    wifiSetup();

    // Fauxmo v2.0
    fauxmo.addDevice("light one");
    fauxmo.addDevice("light two");
    fauxmo.onMessage([](unsigned char device_id, const char * device_name, bool state) {
        Serial.printf("[MAIN] Device #%d (%s) state: %s\n", device_id, device_name, state ? "ON" : "OFF");
    });

}

void loop() {
    fauxmo.handle();
}
```

## Control your ESPurna device with Alexa

I've also worked on the integration of the fauxmoESP library in ESPurna. <del>The code is there and it works but I have decided to disable support by default. Basically because of the required staging environment for the espressif8266 platform in PlatformIO. Right now to compile it with WeMo emulation support you have to change the platform to "espressif8266_stage" and add "-DENABLE_FAUXMO=1" to the build flags. Check my platformio.ini file above. Once PlatformIO updates the espressif platform I will probably enable it by default.</del> As of version 1.4.1 of ESPurna it includes fauxmoESP 2.0 which does not require any special development environment and is included by default in every ESPurna built.		

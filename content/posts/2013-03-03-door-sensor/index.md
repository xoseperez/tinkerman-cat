---
author: Xose
comments: true
date: 2013-03-03 22:01:31+00:00
layout: post
slug: door-sensor
title: "Door sensor"
post_id: 239
image: "images/2013-01-03-23.13.10.small_.jpg"
categories:
- Learning
tags:
- 74221
- cr2032
- edge detection
- reed switch
- xbee
---

**UPDATE**: check my post about my new [Moteino based door monitor](/post/moteino-door-monitor/).

In the past I've been monitoring my home door with an IP camera and the [motion](http://www.lavrsen.dk/foswiki/bin/view/Motion/WebHome) software in my server. Whenever something was moving in the camera range the server saves a little footage, takes a snapshot and calls a couple of scripts I use to send notifications via [NMA](http://www.notifymyandroid.com/) and email.

The problem with this set up was that it had lots of false positives. Mainly due to shadows or changes in the luminosity when someone opened or closed a door in the corridor that leads to the front door. Playing with the software configuration (sensitivity, zone patterns, trying to remove luminosity bursts,...) made no significant difference. Some days 80% of the alerts were due to this type of events, and then the whole system becomes useless because you stop checking the notifications...

<!-- more -->

## Choosing the sensor

So I decided to replace it with something more trustful. My first plan was to add a PIR sensor but I started having sensitivity problems again. Then I just thought: well, if all I want is to get a notification when someone's at the door why not checking when it opens? And I started playing with a magnetic switch, a [reed switch](http://en.wikipedia.org/wiki/Reed_switch).

The switch is very easy to use but it's veeery delicate. I broke two of them trying to bend the leads to the shape I needed. I've used the [cristal ones](https://www.sparkfun.com/products/8642) but I've also bought some [insulated](https://www.sparkfun.com/products/10601) ones for the future, they don't look as fragile. They are very simple switches: when exposed to a magnetic field they close and when the magnetic field is removed they open. So all you need is one of these reed switches and a magnet and you are good to go.

The set up could have been as simple as that. But I added some requirements: it's got to be small, I didn't want to use a micro-controller for such a simple thing and I wanted it to notify the server whenever the door gets opened OR closed, and every 5 minutes if no events.

Since my [previous project](http://tinkerman.eldiariblau.net/smartmeter-pulse-counter-4/) and the gateway I already had in place was based on an Xbee radio that decision was easy. The radio just has to wake every 5 minutes or based on an event (that cyclic sleep plus pin awake modes, or SM5). Then it just has to report the status of a digital pin and the battery status. The only trick here is to awake the radio when the switch gets closed AND when it gets open...

## Clear edge detection

Edge detection is a topic by itself. There are lots of information and options. Off course I could have used an uC with an interrupt pin but that was out by requirement. Maybe the simpler solution is using a XOR gate with two input lines with the same signal, only slightly delayed in one of them (a series of NOT gates would do). And then there are different solutions based on latches, multivibrators, monostables,...

My final choice was a 74HC221, a dual monostable multivibrator with schmitt trigger input. It features a dual edge detector with configurable output pulse length. Each detector has two input lines A and B for positive and negative edge detection. So you only need one of these chips to detect rising and falling edges of the same signal. Nice.

The circuit includes a RC filter for the input signal (that with the schmidt trigger in the input lines of the 74HC221 will remove bouncing problems) and a couple of 2222 transistors to "merge" the output signals from the two monostables. And that's pretty much it.

{{< lazyimg "images/2012-12-29-23.39.53.small_.jpg" "Door switch prototype" >}}

{{< lazyimg "images/schematics.png" "Door switch schematic" >}}

The resistor (1MOhm) and capacitor (100nF) combination produces a 70ms pulse aprox. which is enough to wake up the radio and send the information. The radio is set to SM=5 mode (cyclic sleep, pin wake) with SP=7E4, SN=6 (aprox 2 minutes sleep cycle) ST=32 (50ms time before sleep) and SO=6 (sleep for the whole 2 minutes). The sleep cycle is slightly above 2 minutes (121.2 seconds) but the internal clock is never accurate and it cannot account for the time it's awake so I always configure the Xbee timings by trial and error. The sample rate is 1 second (IR=3E8) and it is reporting DIO12 and VCC (P2=3 and V+=FFFF). Pull-ups for DIO12 and DTR (Sleep request pin) have been disabled (PR=1BBF).

## Run on batteries for months

The final issue is the power supply. Since I wanted the sensor to be as small as possible I thought about using a CR2032 coin cell. Their capacity is about 230mAh and my tests and calculations resulted in a life time of 415 days. The problem is that I never took into consideration the drain current condition of the battery until I started investigating the continuous drop in the battery voltage the sensor reported during the first hours in production environment.

A good resource about stressed coined cells is "[High pulse drain impact on CR2032 coin cell battery capacity](http://www.eetimes.com/ContentEETimes/Documents/Schweber/C0924/C0924post.pdf)" (PDF) study by Kjartan Furset and Peter Hoffman. There are multiple factors that determine what e authors define as the "corrected battery capacity": the peak current, the length of the pulse, the periodicity or the recovery-time. A battery that was designed to have a drain battery of less than 1mA won't perform as expected with peaks of up to 50mA. A patch to the sensor could be adding a large capacitor across the battery to help it with the current peaks. But I decided to not doing anything just yet and wait and see.

The graph for the first 20 days showed a drop of about 10mV per day average. The Xbee minimum operational voltage is 2100mV so it looked like the sensor won't last longer than maybe 2 more months instead of the original 13 I had calculated. But then the voltage drop ceased and now, after 2 months duty time, it is more or less stabilized around 2650mV. I cannot tell how much longer it will survive but this has become a new experiment. My plan is to let the battery die and then add a 1000uF capacitor and a fresh battery and compare the lifetime of both batteries.

## Forwarding the messages

The sensor works very well. Every time the door opens or closes the server gets the notification through the [Xbee2MQTT gateway](http://tinkerman.eldiariblau.net/xbee-to-mqtt-gateway/). An "alarm" daemon processes the messages, waits a second and sends a NMA notification to my Android phone with a picture from the IP camera. The delay is there to get a reliable image of who is at the door, otherwise I would only get pictures of opening doors when someone gets in. The whole process takes no longer than 5 seconds depending on the lag time to my phone. The only issue I have noticed is that sometimes there is an echo: the door opens and closes and on the next cyclic awake the radio reports again the open-close event... the result is a false positive. I have no explanation for it yet but I can live with it.

Here you have a couple of pictures from the final sensor and how I have placed over the door, using the metal door shield to place the magnet that operates the reed switch.

{{< lazyimg "images/2013-01-03-23.13.10.small_.jpg" "Door switch sensor final" >}}

{{< lazyimg "images/2013-01-03-23.23.35.small_.jpg" "Door switch sensor placement over the door" >}}

		

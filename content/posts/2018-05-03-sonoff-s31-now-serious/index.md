---
author: Xose
comments: true
date: 2018-05-03 19:11:34+00:00
layout: post
slug: sonoff-s31-now-serious
title: "Sonoff S31, a world apart"
post_id: 2324
image: "images/P1240886s-1200x800.jpg"
categories:
- Analysis
- Hacking
tags:
- certifications
- chipsea
- cse7766
- domoticz
- esp8266
- esptool
- espurna
- hlw8012
- home assistant
- hot ground
- influxdb
- iteadstudio
- mqtt
- s20
- s31
- schuko
- sonoff
- thingspeak
- usb2uart
---

It's not that other Sonoff products are not "serious" business, but there are a number of design changes in the [Sonoff S31](https://www.itead.cc/smart-home/sonoff-s31.html) that make this new product a world apart. For the functional point of view it looks like a S20 with POW-powers, but they have redesigned the product completely. The result is very very interesting.

  * Revamped case, more compact and sturdy
  * Redesigned PCB, actually 2 different PCBs for main and control
  * Different power monitor chip: the CSE7766 (same as in the new POW R2) replaces the HLW8012

The only drawback: it's only compatible with plug types A & B, tat is central and north-america and few other countries. I'd love to see a S31-EU schuko version!

You can buy the S31 from Itead (see link above) or via the usual marketplaces. Actually the [S31 is slightly cheaper](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338044841&mpre=https%3A%2F%2Fwww.ebay.com%2Fitm%2FSonoff-S31-US-16A-WIFI-Smart-Switch-Socket-APP-Remote-Ctrl-Timer-Power-Moniter%2F332500362410%3Fhash%3Ditem4d6a90e4aa%3Ag%3AmgQAAOSwDFBaRL1f) [Ebay] on some of them.

<!-- more -->

## Not an unboxing

You know I don't do unboxings so  this is not an unboxing. They just happen to be pictures with the device in a box.

{{< lazyimg "images/P1240882s.jpg" >}}

{{< lazyimg "images/P1240883s.jpg" >}}

{{< lazyimg "images/P1240884s.jpg" >}}

The device itself is kind of pretty, compact and robust. It has some appealing design choices (outside and inside) and the way everything fits together (see more pictures below) is great.

Aside from the male and female type A plugs, you have a ON/OFF button on the dark gray side and two LEDs. The red LED closer to the side shows the relay status and the blue one, closer to the plug is used for notifications.

{{< lazyimg "images/P1240886s-1200x800.jpg" >}}

{{< lazyimg "images/P1240887s.jpg" >}}

{{< lazyimg "images/P1240890s.jpg" >}}

Every time I look at a USA plug I get the same feeling. Ooooh, how is that you are so sad? But this time it was also my face. The slick design of the device gets completely ruined with the two adapters (C to A and A to C).

{{< lazyimg "images/P1240891s.jpg" >}}

{{< lazyimg "images/IMG_20160728_161811x.jpg" >}}

The design is very different from the [S20](/post/s20-smart-socket/). The later is used by several manufacturers for their branded smart plugs, so it's probably someone else's design. I can't tell about the S31 but the level of integration makes me think it's an _adhoc_ design.

## Opening it

Opening the case to hack the device it's not hard. The only tricky step is to measure the strength you have to apply to first remove the dark grey cap where the ON/OFF button is.

{{< lazyimg "images/P1240894s.jpg" >}}

{{< lazyimg "images/P1240896s.jpg" >}}

Then you have to slide the corners to reveal 3 small Philips screws that will let you remove the top completely. The bottom side (the one with the male plugs) seems to be glued to the PCB so it is not easy to remove without risking breaking something. Anyway you don't need to access the bottom side of the PCB where there are only a few passives and a SO8 chip, probably the switching power supply chip.

{{< lazyimg "images/P1240897s.jpg" >}}

{{< lazyimg "images/P1240898s.jpg" >}}

## Compact design

The design is based on **two PCBs**. The bigger one for the AC connections, AC/DC transformer and relay. And the smaller one for the the DC circuitry, including the ESP8266, an SPI flash memory and the CSE7766 power monitoring chip. It sounds like a good idea but it gets somewhat screw because they don't actually isolate AC from DC. AC is in the secondary PCB too since the power monitor chips needs access to mains. Also the distance between AC and DC traces is thinner than what would be desirable.

{{< lazyimg "images/P1240877s.jpg" >}}

{{< lazyimg "images/P1240878s.jpg" >}}

Notice that, unlike the S20 where the PCB is connected to the plug barrels via wires, in the S31 **the connectors are soldered to the PCB**. This saves some space for sure but probably makes the device more sturdy too. The removable cap has separators to ensure the contacts are isolated from other components, including the small cables from the transformer.

{{< lazyimg "images/P1240879s.jpg" >}}

For a complete photo book of the S31 you can check the [FCC database photos](https://fccid.io/2AE2J-S31/Internal-Photos/Internal-Photos-3692513).

## The CSE7766 power monitoring chip

IteadStudio has switched from the **HLW8012** chip to the **CSE7766** for power monitoring with their latests products, the S31 and the POW R2. The reason might be that the new chip by Chinese manufacturer **Chipsea** offers a more stable reading, according to the first tests. The good news are that both chips are pin compatible even thou the protocol is very different.

You might remember from my post about the [HLW8012](/post/hlw8012-ic-new-sonoff-pow/) that this IC uses two pins to output power and current/voltage as variable-frequency square wave. Those pins are number 6 and 7 in the SO8 package. Pin 8 is used in the HLW8012 as an input pin to select whether the output in pin 7 would be proportional to the current or the voltage.

The [CSE7766](http://chipsea.com/UploadFiles/2017/08/11144342F01B5662.pdf) [datasheet, PDF] uses a **serial protocol** instead. At the moment only TX line (pin 6) is enabled. The RX line in pin 8 is flagged as "reserved" in the datasheet. Pin 7 outputs a 50% duty cycle pulse with frequency proportional to the active power, just like the HLW8012.

{{< lazyimg "images/HLW8012-Circuit.png" "HLW8012 suggested application" >}}

{{< lazyimg "images/cse7766_application.jpg" "CSE7766 suggested application with isolation" >}}

So you see that on both chips have the same AC interface (left side of the chip) and a similar interface on the MCU part (right side). Actually pins 6 and 7 are outputs and pin 8 is an input on both chips.

Mind that the CSE7766, just like the HLW8012, ties the logic ground to the mains neutral line. This is commonly known as "hot ground". It's not bad by itself as long as ground is not exposed outside the case.

## But

We all know about Itead products. They have walked a long path since the first Sonoff Basic (or Sonoff WiFi). But still, they do not provide any **electrical safety certifications** (ETS, UL, ETSI) with their products. They have applied for FCC, claim to be CE and comply with RoHS but EE and professional electricians will probably not install these devices. It's true the S31 is a plug-and-play device, no electrician required, but in some sense it's an "AS IS" product. You are a maker, right?

Even thou IteadStudio is improving with every design, **safety distance between AC and DC traces** is not met.

The POW and S31 have the caveat of being **non-isolated** devices. Mains are connected to the logic ground and the power monitor IC output is not isolated from the MCU. Even thou the datasheet suggests an isolated application using an optocoupler I have not seen it in the S31. So please, take all the safety precautions when the PCB is exposed.

## Flashing ESPurna on the S31

> **REMEMBER: do not do this while the device is connected to mains. The logic ground in the device is connected to the mains neutral line.**

Flashing the S31 with custom firmware requires soldering some wires or a header to the pads at the edge of the small PCB. These pads expose (not only) GND, 3V3, RX and TX. Just connect them to your USB2UART programmer. Then press and hold the button (which is tied to GPIO0) and connect the programmer to your computer so it will power the board.

With GPIO0 connected to GND (that's what the button does) the ESP8266 will enter flash mode. You will then be able to flash ESPurna (or any other firmware) to the device via serial from your favorite IDE or from the console using **esptool**.

{{< lazyimg "images/P1240877se.jpg" >}}

**ESPurna supports the Sonoff S31 since version 1.12.6**. It can control the relay, the notifications LED and report current, voltage, active power and energy to Thingspeak, InfluxDB, Home Assistant, Domoticz or any other service via MQTT.

{{< lazyimg "images/espurna-s31.jpg" >}}

That's it. You might have notice I'm not writing as much as I used to a few months ago. I happen to be quite busy lately which is not bad news. But I really miss to be able to spend some more time exploring new devices...

See you soon, hopefully!		

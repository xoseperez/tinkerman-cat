---
author: Xose
comments: true
date: 2012-10-21 20:02:53+00:00
layout: post
slug: electronics-jewelry
title: "Electronics jewelry"
post_id: 14
image: "images/flower.jpg"
categories:
- Jewelry
tags:
- diy
- gifts
- jewelry
---

Funny how this has ended up being my first post...

It has recently been our 12th anniversary with my wife and as it always happens you feel like socially impelled to give her a "material" present, although there is no better present than a night out together or a romantic weekend out of the city. But anyway, since it was supposed to be something personal and since I've been tinkering with electronics for quite some time now, I though about putting something of my hobby into it.

I knew that the blinking-LEDs kind of things wouldn't appeal to her very much, so it should be something more manual and less electronics... and it happens that it's not hard to find that sort of things on the internet. Just google [electronics jewelry](https://www.google.es/search?q=electronics+jewelry&prmd=imvnsz&tbm=isch) or search for the same keywords on [etsy.com](http://www.etsy.com/search?q=electronics%20jewelry) and you will find loads of ideas.

Now, most of those designs are copyrighted so you are not supposed to use them freely. Copyright and intellectual property in jewelry, as in many other fields, is a hot issue but consensus and common sense say that law may only apply, if it does, when someone is taking profit of somebody else's work. And since that's not my case I just borrowed two of the designs I liked for a one-time gift for my wife.

{{< lazyimg "images/horse.jpg" >}}

The first design is an horse/deer-shaped earring made with an IC and a resistor. Initially I thought it was a design by [Saltygal](http://www.flickr.com/photos/saltygal/sets/72157606084739626/) because one of the pictures I found was linked to her profile at Etsy, and since she had closed her shop I wrote her and asked her for permission to borrow her design. Finally it turned out it is not so I don't know who designed it... anyway, even thou it is pretty simple it's also one of the nicest ones. It involves just electronics components to mimic a horse or a deer or whatever four legged animal you like the most (albeit with eight legs...). I had some trouble gluing both components together. I used some jewelry-glue I bought but it took ages for it to dry so I had to use the [third hand](https://www.sparkfun.com/products/9317) I use to hold still the PCBs while soldering and I just left both pieces there for 24 hours before daring to touch them again. If there is a next time I will give epoxy a chance...

{{< lazyimg "images/flower.jpg" >}}

The second one is a flower pendant using 5 axial coils. I think I also took the idea from Etsy but I have not been able to find the product now that I'm writing this post so I don't know whose design it is either. It is also quite simple and it involves some soldering. The tricky part here is trying to find some good-looking inductors... and although I'm happy with the final result I know I could do better.

By the way... my wife liked them a lot. She wore them to a party just the night after I gave her the present and I think she enjoyed being the geekiest girl in the party...		
